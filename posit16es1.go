package posit

import (
	"math"
	"math/bits"
	"strconv"
	"strings"
)

type Posit16es1 struct {
	num uint16
}

func NewPosit16es1FromBits(bits uint16) Posit16es1 {
	return Posit16es1{num: bits}
}

func (a Posit16es1) Float() float64 {
	if a.num == 0 {
		return 0.0
	}
	if a.num == 1<<(16-1) {
		return math.Inf(1)
	}
	aneg := a.num&(1<<(16-1)) != 0
	if aneg {
		a.num = uint16(-int16(a.num))
	}
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum<<1 | 1<<(16-1)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	frac := float64(afracP1) / float64(uint32(0b1<<(16-1)))
	if aneg {
		return -(math.Pow(2.0, float64(ascale)) * frac)
	} else {
		return math.Pow(2.0, float64(ascale)) * frac
	}
}

// String converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit16es1) String() string {
	return a.FormatScientific()
}

// FormatScientific converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit16es1) FormatScientific() string {
	switch a.num {
	case 0:
		return "0"
	case 1 << 15:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	start := i - 1
	end := i - 1

	var s string
	if aneg {
		buf[i-2] = '-'
		start--
	}

	buf[i-1] = buf[i]

	if i+1 != len(buf) {
		last_non0 := 0
		for index, c := range buf[i+1:] {
			if c != '0' {
				last_non0 = index + 2
			}
		}
		buf[i] = '.'
		end = i - 1 + last_non0
	}
	s = string(buf[start : end+1])
	power := int64(ascaleB10) + int64(len(buf[i+1:]))
	if power == 1 {
		s += "*10"
	} else if power != 0 {
		s += "*10^" + strconv.FormatInt(int64(ascaleB10)+int64(len(buf[i+1:])), 10)
	}
	return s
}

// FormatBasic converts the Posit into a string in basic numerical notation.
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit16es1) FormatBasic() string {
	switch a.num {
	case 0:
		return "0"
	case 1 << 15:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	var s string
	if aneg {
		s = "-"
	}
	switch {
	case int32(len(buf))+int32(ascaleB10) > int32(i):
		s += string(buf[i : len(buf)+int(ascaleB10)])
		if ascaleB10 != 0 {
			s += "."
			s += string(buf[len(buf)+int(ascaleB10):])
		}
	case int32(len(buf))+int32(ascaleB10) <= int32(i):
		s += "0."
		for int32(len(buf))+int32(ascaleB10) < int32(i) {
			s += "0"
			ascaleB10++
		}

		s += string(buf[i:])
	}
	s = strings.TrimRight(s, "0")
	s = strings.TrimRight(s, ".")
	return s
}

func (a Posit16es1) getDigits() ([64]byte, int16, int, bool) {
	aneg := a.num&(1<<(16-1)) != 0
	if aneg {
		a.num = uint16(-int16(a.num))
	}
	var reg int8
	r1 := uint16(2)
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		reg = 0
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}

	sftnum <<= 2
	afracP1 := uint32((sftnum << (1 & 0x1f)) | (1 << 15))
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascaleB2 := (int16(reg) << (1 & 0x1f)) + int16(aexp)

	ascaleB10 := ascaleB2 - (16 - 1)
	ascaleB5 := -ascaleB10
	if ascaleB5 < 0 {
		if ascaleB5 < -16 {
			ascaleB5 += 16
			afracP1 <<= 16
		} else {
			afracP1 <<= -ascaleB5
			ascaleB5 = 0
		}
		ascaleB10 = 0
	}

	//we need to have at least 3 bits clean for a non-rounded multiplication by 5
	for ascaleB5 > 0 && afracP1>>(32-3) == 0 {
		afracP1 *= 5
		ascaleB5--
	}

	var buf [64]byte
	i := len(buf)

	for afracP1 >= 100 {
		is := afracP1 % 100 * 2
		afracP1 /= 100
		i -= 2
		buf[i+1] = smallsString[is+1]
		buf[i+0] = smallsString[is+0]
		//we need to have at least 3 bits clean for a non-rounded multiplication by 5
		for ascaleB5 > 0 && afracP1>>(32-3) == 0 {
			afracP1 *= 5
			ascaleB5--
		}
		for ascaleB5 < 0 && afracP1>>(32-1) == 0 {
			afracP1 <<= 1
			ascaleB5++
		}
	}

	// afracP1 < 100
	is := afracP1 * 2
	i--
	buf[i] = smallsString[is+1]
	if afracP1 >= 10 {
		i--
		buf[i] = smallsString[is]
	}
	return buf, ascaleB10, i, aneg
}
func NewPosit16ES1FromFloat(f float64) Posit16es1 {
	if math.IsNaN(f) || math.IsInf(f, 0) {
		return NewPosit16es1FromBits(1 << 15)
	}

	bits := math.Float64bits(math.Abs(f))

	scale := int16((bits & 0x7fffffffffffffff) >> 52)
	combinedFrac := bits & 0x000fffffffffffff
	if scale == 0 {
		if combinedFrac == 0 {
			return Posit16es1{
				num: 0,
			}
		} else {
			combinedFrac >>= 1
			scale++
		}
	}
	scale -= 1023

	combinedFrac >>= 52 - 16
	endreg := scale >> 1
	endexp := uint16(scale - (endreg << 1))

	var outPosit uint16
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 16-2 {
			outPosit = 1
			if math.Signbit(f) {
				outPosit = uint16(-int16(outPosit))
			}
			return Posit16es1{num: outPosit}
		}
		outPosit = (0b1 << (15 - 1)) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 16-2 {
			outPosit = 0xffff >> 1
			if math.Signbit(f) {
				outPosit = uint16(-int16(outPosit))
			}
			return Posit16es1{num: outPosit}
		}
		outPosit = (0xffff >> 1) - ((0xffff >> 1) >> (outn & 0x0f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 15 - 1 - outn - 1

	combinedFrac >>= 1 + outn + 2

	outPosit |= uint16(endexp) << outFracBits
	outPosit |= uint16(combinedFrac)

	if math.Signbit(f) {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

//This returns a Posit that best represents neg*2^scale*(1+frac)
func NewPosit16ES1FromComponents(neg bool, scale int, frac uint16) Posit16es1 {
	endreg := scale >> 1
	endexp := uint16(scale - (endreg << 1))

	var outPosit uint16
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 16-1 {
			return Posit16es1{num: 0}
		}
		outPosit = (0b1 << (15 - 1)) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 16-2 {
			outPosit = 0xffff >> 1
			if neg {
				outPosit = uint16(-int16(outPosit))
			}
			return Posit16es1{num: outPosit}
		}
		outPosit = (0xffff >> 1) - ((0xffff >> 1) >> (outn & 0x0f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 15 - 1 - outn - 1

	frac >>= 1 + outn + 2

	outPosit |= uint16(endexp) << outFracBits
	outPosit |= uint16(frac)

	if neg {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

func NewPosit16ES1FromInt64(x int64) Posit16es1 {
	var a Posit16es1
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint64(x)
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 64 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)
	if endreg > 16-3 {
		a.num = 0xffff >> 1
		if aneg {
			a.num = uint16(-int16(a.num))
		}
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fffffffffffffff) >> (64 - 16 + 1 + 1)) >> endreg)

	mask := uint64((1 << (64 - 16 + 1 + 1)) << endreg)

	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint16(-int16(a.num))
	}
	return a
}

func NewPosit16ES1FromUint64(x uint64) Posit16es1 {
	var a Posit16es1
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := uint64(x)
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 64 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)

	if endreg > 16-3 {
		a.num = 0xffff >> 1
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fffffffffffffff) >> (64 - 16 + 1 + 1)) >> endreg)

	mask := uint64((1 << (64 - 16 + 1 + 1)) << endreg)

	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func NewPosit16ES1FromInt32(x int32) Posit16es1 {
	var a Posit16es1
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint32(x)
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= 32 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)
	if endreg > 16-3 {
		a.num = 0xffff >> 1
		if aneg {
			a.num = uint16(-int16(a.num))
		}
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fffffff) >> (32 - 16 + 1 + 1)) >> endreg)

	mask := uint32((1 << (32 - 16 + 1 + 1)) << endreg)

	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint16(-int16(a.num))
	}
	return a
}

func NewPosit16ES1FromUint32(x uint32) Posit16es1 {
	var a Posit16es1
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := uint32(x)
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= 32 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)

	if endreg > 16-3 {
		a.num = 0xffff >> 1
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fffffff) >> (32 - 16 + 1 + 1)) >> endreg)

	mask := uint32((1 << (32 - 16 + 1 + 1)) << endreg)

	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func NewPosit16ES1FromInt16(x int16) Posit16es1 {
	var a Posit16es1
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint16(x)
	outscale := uint8(bits.Len16(outfrac >> 1))
	outfrac <<= 16 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)
	if endreg > 16-3 {
		a.num = 0xffff >> 1
		if aneg {
			a.num = uint16(-int16(a.num))
		}
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fff) >> (16 - 16 + 1 + 1)) >> endreg)

	mask := uint16((1 << (1 + 1)) << endreg)

	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint16(-int16(a.num))
	}
	return a
}

func NewPosit16ES1FromUint16(x uint16) Posit16es1 {
	var a Posit16es1
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := uint16(x)
	outscale := uint8(bits.Len16(outfrac >> 1))
	outfrac <<= 16 - 1 - outscale

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)

	if endreg > 16-3 {
		a.num = 0xffff >> 1
		return a
	}
	a.num = 0xffff - ((0xffff >> 1) >> endreg)

	h := uint16(0xffff >> 1)
	a.num |= bits.RotateLeft16(endexp, 16-int(endreg)-2-1) & h
	a.num |= uint16(((outfrac & 0x7fff) >> (16 - 16 + 1 + 1)) >> endreg)

	mask := uint16((1 << (1 + 1)) << endreg)

	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func (a Posit16es1) Int64() int64 {
	if a.num == 0 || a.num == 1<<15 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint16(-int16(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	ascale -= 16 - 1
	if ascale < 0 {
		ret := afracP1 >> uint64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -int64(ret)
		}
		return int64(ret)
	} else if ascale >= 16 {
		if aneg {
			return math.MinInt64
		}
		return math.MaxInt64
	}
	ret := uint64(afracP1) << uint64(ascale)
	if aneg {
		return int64(-ret)
	}
	return int64(ret)
}

func (a Posit16es1) Uint64() uint64 {
	if a.num == 0 || a.num == 1<<15 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	ascale -= 16 - 1
	if ascale < 0 {
		ret := afracP1 >> uint64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint64(ret)
	} else if ascale >= 16+1 {
		return math.MaxUint64
	}
	return uint64(afracP1) << uint64(ascale)
}

func (a Posit16es1) Int32() int32 {
	if a.num == 0 || a.num == 1<<15 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint16(-int16(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	ascale -= 16 - 1
	if ascale < 0 {
		ret := afracP1 >> uint32(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint32(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -int32(ret)
		}
		return int32(ret)
	} else if ascale >= 16 {
		if aneg {
			return math.MinInt32
		}
		return math.MaxInt32
	}
	ret := uint32(afracP1) << uint32(ascale)
	if aneg {
		return int32(-ret)
	}
	return int32(ret)
}

func (a Posit16es1) Uint32() uint32 {
	if a.num == 0 || a.num == 1<<15 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	ascale -= 16 - 1
	if ascale < 0 {
		ret := afracP1 >> uint32(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint32(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint32(ret)
	} else if ascale >= 16+1 {
		return math.MaxUint32
	}
	return uint32(afracP1) << uint32(ascale)
}

func (a Posit16es1) Le(b Posit16es1) bool {
	return a.num <= b.num
}

func (a Posit16es1) Lt(b Posit16es1) bool {
	return a.num < b.num
}

func (a Posit16es1) Eq(b Posit16es1) bool {
	return a.num == b.num
}

func (a Posit16es1) Add(b Posit16es1) Posit16es1 {
	aneg := a.num&(1<<(16-1)) != 0
	if aneg {
		if a.num == 1<<15 {
			return NewPosit16es1FromBits(1 << 15)
		}

		a.num = uint16(-int16(a.num))
	} else {
		if a.num == 0 {
			return b
		}
	}

	xneg := aneg
	bneg := b.num&(1<<(16-1)) != 0
	if bneg {
		if b.num == 1<<15 {
			return NewPosit16es1FromBits(1 << 15)
		}

		xneg = !xneg
		b.num = uint16(-int16(b.num))
	} else {
		if b.num == 0 {
			if aneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
	}

	//This allows less branching later on.
	//This potentially leaves bneg in the wrong position, dont use bneg!!
	if a.num < b.num {
		a.num, b.num = b.num, a.num
		aneg = bneg
	} else if a.num == -b.num {
		a.num = 0
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	bexp := sftnum >> (16 - 1)
	bscale := (int16(reg) << 1) + int16(bexp)

	//Calc

	// We are using a trick to not have to double-negate.
	// The simple way to do this would be to negate af and fb
	// on aneg and bneg respectively, and negate the result if the result should be negated.
	// Instead we take advantage that a>b, so we never have to negate a and
	// we can negate b if and only if bneg^aneg.

	combinedFrac := uint32(afracP1) << 15

	tmp := bscale - ascale + 15
	if tmp > 0 {
		bf := uint32(bfracP1) << (tmp & 0x1f)
		if xneg {
			combinedFrac -= bf
		} else {
			combinedFrac += bf
		}
	}

	if xneg {
		if combinedFrac == 0 {
			a.num = 0
			return a
		}
		//This is faster than LeadingZeros
		for (combinedFrac>>(32-2))&1 == 0 {
			ascale--
			combinedFrac <<= 1
		}
	}

	// leave the hidden bit in
	overflow := int16(combinedFrac >> 31)
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> 1
	f := uint16(0xffff)
	endexp := uint16(ascale) &^ (f << 1)

	var outPosit uint16
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 16-2 {
			a.num = 1
			if aneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = (0b1 << 15) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 16-2 {
			a.num = 0xffff >> 1
			if aneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = 0xffff - (0xffff >> (outn & 0x0f))
	}

	combinedShift := (1 + outn) & 0x1f
	combinedFrac >>= combinedShift

	outfrac := uint16(combinedFrac>>15) & ((0xffff >> (1 + 1)) >> (outn & 0x0f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft16(endexp, 15-int(combinedShift)) & (0xffff >> 1)

	if combinedFrac&(0xffff>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if aneg {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

func (a Posit16es1) Sub(b Posit16es1) Posit16es1 {
	b.num = uint16(-int16(b.num))
	return a.Add(b)
}

func (a Posit16es1) Neg() Posit16es1 {
	a.num = uint16(-int16(a.num))
	return a
}

func (a Posit16es1) Mul(b Posit16es1) Posit16es1 {
	//A
	aneg := a.num&(1<<(16-1)) != 0
	if aneg {
		if a.num == 1<<15 {
			return NewPosit16es1FromBits(1 << 15)
		}

		a.num = uint16(-int16(a.num))
	}

	xneg := aneg
	bneg := b.num&(1<<(16-1)) != 0
	if bneg {
		if b.num == 1<<15 {
			return NewPosit16es1FromBits(1 << 15)
		}

		xneg = !xneg
		b.num = uint16(-int16(b.num))
	}

	if a.num == 0 || b.num == 0 {
		return NewPosit16es1FromBits(0)
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}

	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	bexp := sftnum >> (16 - 1)
	ascale += (int16(reg) << 1) + int16(bexp)

	//Calc

	af := int32(afracP1)
	bf := int32(bfracP1)
	combinedFrac := uint32(af * bf)

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 16-2 for the number

	// leave the hidden bit in
	overflow := int16(combinedFrac >> 31)
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> 1
	f := uint16(0xffff)
	endexp := uint16(ascale) &^ (f << 1)

	var outPosit uint16
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 16-2 {
			a.num = 1
			if xneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = (0b1 << 15) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 16-2 {
			a.num = 0xffff >> 1
			if xneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = 0xffff - (0xffff >> (outn & 0x0f))
	}

	combinedShift := (1 + outn) & 0x1f
	combinedFrac >>= combinedShift

	outfrac := uint16(combinedFrac>>15) & ((0xffff >> (1 + 1)) >> (outn & 0x0f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft16(endexp, 15-int(combinedShift)) & (0xffff >> 1)

	if combinedFrac&(0xffff>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

func (a Posit16es1) Div(b Posit16es1) Posit16es1 {
	//A
	aneg := a.num&(1<<(16-1)) != 0
	xneg := aneg
	bneg := b.num&(1<<(16-1)) != 0

	if bneg {
		if b.num == 1<<15 {
			return b
		}

		xneg = !xneg
		b.num = uint16(-int16(b.num))
	} else if b.num == 0 {
		return NewPosit16es1FromBits(1 << 15)
	}

	if aneg {
		if a.num == 1<<15 {
			return a
		}

		a.num = uint16(-int16(a.num))
	} else if a.num == 0 {
		return a
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 1) | (1 << 15)
	sftnum <<= 1
	bexp := sftnum >> (16 - 1)
	ascale -= (int16(reg) << 1) + int16(bexp)

	//Calc

	af := uint32(afracP1)
	bf := uint32(bfracP1)
	combinedFrac := uint32(af<<(15-1)) / bf

	if combinedFrac == 0 {
		a.num = 0
		return a
	}
	rem := uint32(af<<(15-1)) % (uint32(bf))

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 16-2 for the number

	// leave the hidden bit in
	overflow := combinedFrac>>(15-1) == 0
	if overflow {
		ascale -= 1
		combinedFrac <<= 1
	}

	endreg := ascale >> 1
	f := uint16(0xffff)
	endexp := uint16(ascale) &^ (f << 1)

	var outPosit uint16
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 16-2 {
			a.num = 1
			if xneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = (0b1 << 15) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 16-2 {
			a.num = 0xffff >> 1
			if xneg {
				a.num = uint16(-int16(a.num))
			}
			return a
		}
		outPosit = 0xffff - (0xffff >> (outn & 0x0f))
	}

	combinedShift := (1 + outn - 1) & 0x1f

	outfrac := (uint16(combinedFrac) >> (combinedShift & 0x1f)) & ((0xffff >> (1 + 1)) >> (outn & 0x0f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft16(endexp, 15-1-int(outn)) & (0xffff >> 1)

	mask := uint32(1<<(combinedShift&0x3f)) - 1
	if combinedFrac&mask != 0 || rem != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

func (a Posit16es1) Sqrt() Posit16es1 {
	if a.num>>15 != 0 {
		return NewPosit16es1FromBits(1 << 15)
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		if a.num == 0 {
			return a
		}

		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum << 1
	sftnum <<= 1
	aexp := sftnum >> (16 - 1)

	//Calc
	n := uint32(afracP1) << 15
	if aexp&1 != 0 {
		n = (n << 1) | (0b1 << 31)
		n -= 1 << (32 - 2)
	}

	if reg&1 != 0 {
		aexp += 1 << 1
	}
	reg >>= 1
	aexp >>= 1

	//out
	var outPosit uint16
	var outn uint8
	if reg < 0 {
		outn = uint8(-reg)
		outPosit = (0b1 << (15 - 1)) >> (outn & 0x0f)
	} else {
		outn = uint8(1 + reg)
		outPosit = (0xffff >> 1) - ((0xffff >> 1) >> (outn & 0x0f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 15 - 1 - 1 - outn

	//more calc
	//https://stackoverflow.com/a/31120562

	combinedFrac := uint32(1 << (32 - 2 - 2*1 - (outn << 1)))
	one := combinedFrac >> 2
	n >>= ((outn << 1) + 2*1) & 0x1f

	//There are some tricky performance characteristics of modifying this code
	//Allignment changes the benchmark for this loop a lot (jumping to aligned addeesses ...)
	//Dont be allarmed if suddenly this code performs worse with a small change
	for one != 0 {
		a1 := combinedFrac + one
		if n >= a1 {
			n -= a1
			combinedFrac += one << 1
		}
		combinedFrac >>= 1
		one >>= 2
	}
	g := uint8((2 + 1 + outn) & 0x1f)
	outPosit |= uint16(aexp) << outFracBits

	outfrac := uint16(combinedFrac>>1) << g
	outfrac >>= g

	outPosit |= outfrac
	outPosit += uint16(combinedFrac) & 1
	return Posit16es1{num: outPosit}
}

func (a Posit16es1) Ceil() Posit16es1 {
	//A
	decnum := a.num
	aneg := decnum>>15 != 0
	if aneg {
		if a.num == 1<<15 {
			return a
		}

		decnum = uint16(-int16(a.num))
	}
	if a.num == 0 {
		return a
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<(16-2)) == 0 {
		if aneg {
			a.num = 0
		} else {
			a.num = 1 << (16 - 2)
		}
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := uint16(1 << (16 - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= 16 - 1
	ascale := (uint16(m) << 1) + aexp

	//calculate the how many bits are "bad"
	badBits := int8(16-2-1-m-1) - int8(ascale)
	if badBits >= 0 {
		leftover := decnum &^ (0xffff << (badBits & 0x1f))
		decnum &= (0xffff << (badBits & 0x1f))
		if leftover != 0 != aneg {
			decnum += 1 << (badBits & 0x1f)
		}

		if aneg {
			a.num = uint16(-int16(decnum))
		} else {
			a.num = decnum
		}
	}

	return a
}

func (a Posit16es1) Truncate() Posit16es1 {
	//A
	decnum := a.num
	aneg := decnum>>15 != 0
	if aneg {
		if a.num == 1<<15 {
			return a
		}

		decnum = uint16(-int16(a.num))
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<(16-2)) == 0 {
		a.num = 0
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := uint16(1 << (16 - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= 16 - 1

	ascale := (uint16(m) << 1) + aexp

	//Calc
	tmp := int8(16-2-1-m-1) - int8(ascale)
	if tmp >= 0 {
		if aneg {
			a.num = uint16(-int16(decnum & (0xffff << (tmp & 0x1f))))
		} else {
			a.num = a.num & (0xffff << (tmp & 0x1f))
		}
	}

	return a
}

func (a Posit16es1) Round() Posit16es1 {
	//A
	decnum := a.num
	aneg := decnum>>15 != 0

	if aneg {
		if a.num == 1<<15 {
			return NewPosit16es1FromBits(1 << 15)
		}
		decnum = uint16(-int16(decnum))
	}

	if decnum&(1<<(16-2)) == 0 {
		aexp := decnum
		if aexp&(1<<(15-2)) == 0 {
			//the second time we know for sure an>1 aka a<0.5
			a.num = 0
			return a
		}
		aexp <<= 3
		aexp >>= 16 - 1

		if aexp != (1<<1)-1 || decnum<<(3+1) == 0 {
			a.num = 0
			return a
		} else {
			if aneg {
				a.num = 0b11 << (15 - 1)
				return a
			} else {
				a.num = 0b1 << (15 - 1)
				return a
			}
		}
	}

	var an = uint8(1)
	aexp := decnum
	for aexp&(1<<(15-2)) != 0 {
		an++
		aexp <<= 1
	}

	aexp <<= 3
	aexp >>= 16 - 1

	var m = an - 1
	ascale := (uint16(m) << 1) + aexp

	//Calc
	// we need to make sure to tie-break to nearest even, not uneven.
	// To make sure 1.5 also breaks correctly, we | the hidden bit
	tmp := 1 + an + uint8(ascale)
	if an < 15-1 && (((decnum|((0x1<<(16-2-1))>>(an&0x0f)))>>((15-2-tmp)&0x0f))&3 == 3 || (decnum<<((2+tmp)&0x0f)) > (1<<15)) {
		//slowpath
		afracP1 := decnum | (0b1 << ((15 - (1 + an + 1)) & 0x0f))
		afracP1 &= (0xffff >> (1 + 1)) >> (an & 0x0f)
		afracP1 += (0b1 << (15 - 1)) >> tmp
		if afracP1&((0b1<<(15-1))>>(an&0x0f)) != 0 {
			ascale++
			tmp++

			endreg := ascale >> 1
			f := uint16(0xffff)
			endexp := uint16(ascale) &^ (f << 1)

			outn := uint8(1 + endreg)
			a.num = (0xffff >> 1) - ((0xffff >> 1) >> (outn & 0x0f))

			outFracBits := 15 - 1 - 1 - outn
			afracP1 >>= 1
			afracP1 &= ((0xffff >> (2 + 1)) >> (outn & 0x0f))
			a.num |= endexp << (outFracBits & 0x0f)
		} else {
			outFracBits := 15 - 1 - 1 - an
			a.num = decnum & (0xffff << (outFracBits & 0x0f))
			afracP1 &= (0xffff >> (2 + 1)) >> (an & 0x0f)
		}
		if (15 - 1 - int8(tmp)) >= 0 {
			afracP1 = afracP1 & (0xffff << ((15 - 1 - tmp) & 0x0f))
		}
		a.num |= afracP1
		if aneg {
			a.num = uint16(-int16(a.num))
		}
		return a
	} else {
		tmp := int8(15-1-1-an) - int8(ascale)
		if tmp >= 0 {
			if aneg {
				a.num = uint16(-int16(decnum & (0xffff << (tmp & 0x1f))))
			} else {
				a.num = a.num & (0xffff << (tmp & 0x1f))
			}
		}
	}
	return a
}

func (a Posit16es1) Exp() Posit16es1 {
	//TODO: should we have per-size tables and calculations? like a 32 bit table since we wont need more than 16 bits of accuracy anyways

	//A
	aneg := a.num&(1<<(16-1)) != 0
	if aneg {
		a.num = uint16(-int16(a.num))
	}

	// check for things that result in 1
	// Modifies version of normal calculations.
	// scale = -28, but its negated since to avoid negating it at runtime
	// also avoids branches because ist never bigger than 30
	if a.num <= 191 {
		a.num = 1 << (16 - 2)
		return a
	}

	if a.num >= 28848 {
		a.num = 0xffff >> 1
		if aneg {
			a.num = 0
		}
		return a
	}

	var reg int8
	r1 := uint8(1)
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}
	n := 16 - 1 - 1 - r1 - 1
	if n >= 16 {
		n = 0
	}

	afrac := a.num & ((1 << (n)) - 1)
	sftnum <<= 3
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	for afrac&1 == 0 && afrac != 0 {
		afrac >>= 1
		n--
	}

	if afrac == 0 || n > 32 {
		n = 0
	}

	var outscale uint64
	finalscale := uint64(1 << 63)
	var outfrac uint64
	var power uint64
	var powerOfTwo uint32
	var baseindex uint8

	// Instead of taking the base out of the array directly, we store the index.
	// This way we can look up `base*base` instead of calculating. This reduces errors.
	// From 29 on it always results in 1  ( in 32 bits)
	if ascale < 0 {
		// e^((1+a/2^n)*2^b)	=	e^(a/2^n*2^b)*e^(2^b)
		//						=	e^(a/2^n*2^b)*e^(2^b)
		//						=	(e^(1/2^n))^(a*2^b)*e^(1/2^(-b))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee2[-ascale]
		baseindex = n
		power = uint64(afrac >> -uint32(ascale))

		// We have rounded off everything after the binary point in the power.
		// Here we find the biggest fraction of this, so the biggest 1/2^n.
		// Then we add this again later
		powfracBits := uint8(-ascale)

		i := uint8(0)
		if int8(baseindex+powfracBits)+1-32 >= 0 {
			i = baseindex + powfracBits + 1 - 32
		}
		if afrac<<(32-i) != 0 {
			afrac += 1 << i
		}

		// This is prefered over doing it in the main loop,
		// because we dont have to check for overflow.
		for ; i < powfracBits; i++ {
			if afrac&(1<<i) != 0 {
				finalscale, _ = bits.Mul64(finalscale, erootee2[baseindex+powfracBits-i])
				finalscale <<= 1
				finalscale += 1
			}
		}
	} else if ascale < int16(n) {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^(n-b))
		//						=	e^(2^b)*e^a^(1/(2^(n-b)))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = n - uint8(ascale)
		power = uint64(afrac)
		outscale += 1 << (powerOfTwo & 0x3f)
	} else {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a*2^(b-n))
		//						=	e^(2^b)*e^(a*2^(b-n))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = 0
		power = uint64(afrac << (uint32(ascale) - uint32(n)))
		outscale += 1 << powerOfTwo
	}

	for powerOfTwo > 0 {
		powerOfTwo--
		outfrac, _ = bits.Mul64(outfrac, outfrac)

		if outfrac&(1<<63) != 0 {
			outscale += 1 << powerOfTwo
			outfrac >>= 1
		}
		outfrac <<= 1
	}

	if int(baseindex) < len(erootee2) {
		for ; power != 0 && baseindex != 0; power >>= 1 {
			if power&1 == 1 {
				outfrac, _ = bits.Mul64(outfrac, erootee2[baseindex])
				if outfrac&(1<<63) != 0 {
					outscale++
					outfrac >>= 1
				}
				outfrac <<= 1
			}
			baseindex--
		}
	}
	if power != 0 && baseindex == 0 {
		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, erootee0)

			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
		outscale += power
		power >>= 1
	}
	base := erootee0

	for ; power != 0; power >>= 1 {
		base, _ = bits.Mul64(base, base)
		if base&(1<<63) != 0 {
			outscale += power
			base >>= 1
		}
		base <<= 1

		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, base)
			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
	}
	outfrac, low := bits.Mul64(outfrac, finalscale)
	outfrac <<= 1

	endreg := outscale >> 1
	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)

	outn := uint8(1 + endreg)
	outPosit := uint16(0xffff - (0xffff >> (outn & 0x0f)))

	combinedShift := (1 + uint8(outn) - 1) & 0x1f
	mask := uint64((2<<32)<<(combinedShift&0x1f)) - 1

	outfrac2 := outfrac >> (32 + 1 + 32 - 16) // now the frac is the entirity of the output

	outPosit |= (uint16(outfrac2) >> (combinedShift & 0x1f)) & ((0xffff >> (1 + 1)) >> (outn & 0x1f))
	outPosit |= bits.RotateLeft16(endexp, 16-2-int(combinedShift)) & (0xffff >> 1)

	if outPosit&2 == 2 || outfrac&mask != 0 || low != 0 {
		outPosit += 1
	}
	outPosit >>= 1

	p := Posit16es1{
		num: outPosit,
	}

	if aneg {
		p = p.Div(NewPosit16es1FromBits(1 << (16 - 2)))
	}
	return p
}

func (a Posit16es1) Log2() Posit16es1 {
	// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation

	if a.num == 0 || a.num&(1<<(16-1)) != 0 {
		a.num = 1 << (16 - 1)
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(16-2)) == 0 {
		reg = -1
		for sftnum&(1<<(16-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(16-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 1
	afracP1 := uint64(((sftnum << 1) | (1 << (16 - 2))) &^ (1 << (16 - 1))) // shifted one to the right compared to normal afracP1
	afracP1 <<= (32 - 16)
	sftnum <<= 2
	aexp := sftnum >> (16 - 1)
	ascale := (int16(reg) << 1) + int16(aexp)

	n := uint16(ascale)
	if ascale < 0 {
		n = uint16(-int16(n)) - 1
	}

	var outPosit uint16
	var outn uint8
	var series uint16
	var outscale int32
	seg := uint16(1 << (15 - 1))

	// This is a very specialised version of the algorithm to use less bits.
	if ascale < 0 {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			var passedFirst bool
			var zeroCounter bool = true
			for afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				seg >>= 1
				if afracP1&(1<<31) != 0 {
					afracP1 >>= 1
					series -= seg

					if zeroCounter {
						// this means the last bit is 0 again, so we shift by one.
						seg <<= 1
						series <<= 1
						outscale--
						zeroCounter = true
					} else if passedFirst {
						break
					} else {
						passedFirst = true
						outscale--
					}
				} else if zeroCounter {
					zeroCounter = false
				}
			}

			seg <<= 1
			series <<= 1

			series &^= 1 << (15 - 1)

			endreg := int16(outscale >> 1)
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 16-2 {
					x := -1
					a.num = uint16(-int16(x))
					return a
				}
				outPosit = (0b1 << 15) >> (outn & 0x0f)
			} else {
				outn = 1
				outPosit = 0xffff - (0xffff >> outn)
			}
		} else {
			// pre flipping the first bit, trick to be able to more the second loop after the first
			series = 1 << (15 - 1)
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n % 2) << (15 - 1 - 1))
				n >>= 1
				seg >>= 1
			}
			if afracP1 == (1 << 30) {
				outscale++
			}

			endreg := int16(outscale >> 1)
			outn = uint8(1 + endreg)
			if outn > 16-2 {
				x := 0xffff >> 1
				a.num = uint16(-int16(x))
				return a
			}
			outPosit = 0xffff - (0xffff >> (outn & 0x0f))
		}
		series >>= outn - 1
		seg >>= outn
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series -= seg
			}
			seg >>= 1
		}
		series >>= 1
	} else {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			for afracP1&(1<<31) == 0 && afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				outscale--
			}
			if afracP1 == 1<<30 {
				a.num = 0
				return a
			}
			n = 1

			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}

			endreg := int16(outscale >> 1)
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 16-2 {
					a.num = 1
					return a
				}
				outPosit = (0b1 << 15) >> (outn & 0x0f)
			} else {
				outn = 1
				outPosit = 0xffff - (0xffff >> outn)
			}
		} else {
			// Here we first shift the series and seg to the right to limmit the amount of calculations done.
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n % 2) << (15 - 1 - 1))
				seg >>= 1
				n >>= 1
			}

			endreg := int16(outscale >> 1)
			outn = uint8(1 + endreg)
			if outn > 16-2 {
				a.num = 0xffff >> 1
				return a
			}
			outPosit = 0xffff - (0xffff >> (outn & 0x0f))
		}
		series >>= outn
		seg >>= outn + 1
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}
			seg >>= 1
		}
	}

	f := uint16(0xffff)
	endexp := uint16(outscale) &^ (f << 1)

	outfrac := series & ((0xffff >> (1 + 1)) >> (outn & 0x0f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft16(endexp, 15-1-int(outn)) & (0xffff >> 1)

	if afracP1 != (1<<30) || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if ascale < 0 {
		outPosit = uint16(-int16(outPosit))
	}

	return Posit16es1{num: outPosit}
}

func (a Posit16es1) Loge() Posit16es1 {
	log2 := a.Log2()
	// Constant is log2(e)
	return log2.Div(Posit16es1{num: 18197})
}

func (a Posit16es1) Logb(b Posit16es1) Posit16es1 {
	log2 := a.Log2()
	return log2.Div(b.Log2())
}

func (b Posit16es1) Pow(p Posit16es1) Posit16es1 {
	ln := b.Loge()
	e := ln.Mul(p)
	return e.Exp()
}
