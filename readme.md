# Gosit

Gosit is a Posit library for go. There is currently one other go implementation but this one has been archived and wont receive updates.

Posits are a representation of real numbers that tries to compete with floating point numbers. See [the wikipedia page][positwiki] and [the paper][positpaper] if you want to learn more about posits.

## Compatibility guarantees

Exact output is not covered by the compatibility guarantee, but the mathematical function each method represents will stay the same.

## Testing

Gosit is fuzzed against softposit-rs, if the fuzzing finds a case where the bits returned form this library are diferent from softposit-rs, it fails. Currently all functions are fuzzed at least a couple million times. Currently only 32bits and es=2 are fuzzed. other sizes will come later, but only ES values supported by softposit-rs can be fuzzed.

Some bugs have been found in the rust implementation, hence some special cases in the fuzz tests.
These are: truncating 3 (exact) to 2, and rounding `0<n<0.5` to 1 instead of 0.

## Suport

Gosit currently only supports 32 bit posits, this should be enough for most use cases. smaller sizes, are on the roadmap. hoewver 64 bit is unlikely in the near future.
Currently only es=2 is fuzzed as states in the [Testing](#Testing) section.

## How to use.

### Construction of posits

There are are multiple ways to construct new posits, but the easiest to use is to just convert from floats.

```
Newposit{XX}FromBits
Newposit{XX}FromFloat
Newposit{XX}FromComponents
```

If you have pre-computed posits and need the accuracy you can use the `Newposit{XX}FromBits` funcion. This wil take the
raw ES and bits and interpret them as a posit.

If you have an exponent, a fraction, and a sign, in the form `{sign}2^exponent*(1+fraction)`you can use `Newposit{XX}FromComponents`
to construct a posit dynamically. This will give the posit representing this formula.

### math functions

All math methods take one or more posits of the same type and return a posit of the same type.
Functions on the generic ES posits currently only have one variant, the `{X}SameES` variant.
This variant takes one or multiple posits of the same ES, ad returns a posit with this ES.
In the future non-`SameES` may be added that dont have this requirement, thus being able to take
2 diferent values for ES, and may even choose the ES of the return value itself.

### Conversion to string

Conversion to string is a not-so-good version of ryus algorithm without the limiting part.
It should be an accurate representation, but it may not be perfect.

### Examples

generic ES:

```go
p := gosit.Newposit32FromBits(0b0_001_10_11_10000011_01111110_10010111, 2)
q := gosit.Newposit32FromBits(0b0_000001_11_0000001_00000111_10100011, 2)
fmt.Println(p.Float(), "*", q.Float(), "=", p.MulSameES(q).Float)
```

ES=2 specific:

```go
p := gosit.NewPosit32es2FromBits(0b0_001_10_11_10000011_01111110_10010111)
q := gosit.NewPosit32es2FromBits(0b0_000001_11_0000001_00000111_10100011)
fmt.Println(p.Float(), "*", q.Float(), "=", p.Mul(a).Float())
```

## Benchmarks

Currently I only benchmark against the slow goposit, since its the only other go library for posits.

### Goposit

All tests are ran with the exect same bench cases to eliminate favouring one library over another because of coincidence.
These are rotated out every iteration. For sqrt, the absolute value is taken to avoid the fast-path.
All benches use the recomended ES for the bitsize

```
#Turbo boost has been disabled
cset shield --exec -- go test --run=X --bench=. -benchtime 30s -timeout 1h # cset with 2 threads
BenchmarkAddSlow-10                     1000000000              14.33 ns/op
BenchmarkAdd32ESConst-10                1000000000              12.33 ns/op
BenchmarkAdd16ESConst-10                1000000000              11.74 ns/op
BenchmarkAddSlowGoposit-10              11298813              3463 ns/op
BenchmarkMulSlow-10                     1000000000              13.28 ns/op
BenchmarkMul32ESConst-10                1000000000              11.40 ns/op
BenchmarkMul16ESConst-10                1000000000              10.04 ns/op
BenchmarkMulSlowGoposit-10              10038568              3466 ns/op
BenchmarkDivSlow-10                     1000000000              15.19 ns/op
BenchmarkDiv32ESConst-10                1000000000              13.58 ns/op
BenchmarkDiv16ESConst-10                1000000000              11.89 ns/op
BenchmarkDivSlowGoposit-10               9835690              3661 ns/op
BenchmarkSqrtSlow-10                    1000000000              32.94 ns/op
BenchmarkSqrt32ESConst-10               928119480               38.82 ns/op
BenchmarkSqrt16ESConst-10               1000000000              16.54 ns/op
BenchmarkTruncSlow-10                   1000000000               3.747 ns/op
BenchmarkTrunc32ESConst-10              1000000000               3.388 ns/op
BenchmarkTrunc16ESConst-10              1000000000               3.739 ns/op
BenchmarkRoundSlow-10                   1000000000               6.574 ns/op
BenchmarkRound32ESConst-10              1000000000               5.746 ns/op
BenchmarkRound16ESConst-10              1000000000               5.079 ns/op
BenchmarkStringSlow-10                  147758671              251.7 ns/op
BenchmarkExp32Tiny-10                   1000000000               8.718 ns/op
BenchmarkExp32Medium-10                 257134201              140.9 ns/op
BenchmarkExp32Big-10                    1000000000              11.89 ns/op
BenchmarkExp32ConstESTiny-10            1000000000               8.321 ns/op
BenchmarkExp32ConstESMedium-10          262799006              137.3 ns/op
BenchmarkExp32ConstESBig-10             1000000000              11.85 ns/op
BenchmarkExp16ConstESTiny-10            1000000000               7.412 ns/op
BenchmarkExp16ConstESMedium-10          503256609               72.13 ns/op
BenchmarkExp16ConstESBig-10             1000000000               7.098 ns/op
BenchmarkLog232Tiny-10                  1000000000              12.77 ns/op
BenchmarkLog232Medium-10                570807181               63.49 ns/op
BenchmarkLog232Big-10                   589752162               60.92 ns/op
BenchmarkLog232ConstESTiny-10           1000000000              12.04 ns/op
BenchmarkLog232ConstESMedium-10         606771810               59.79 ns/op
BenchmarkLog232ConstESBig-10            630153366               57.70 ns/op
BenchmarkLog216ConstESTiny-10           1000000000               7.370 ns/op
BenchmarkLog216ConstESMedium-10         1000000000              34.29 ns/op
BenchmarkLog216ConstESBig-10            1000000000              20.42 ns/op
BenchmarkFromInt3232Slow-10             1000000000              10.03 ns/op
BenchmarkFromInt3232ConstES-10          1000000000               7.838 ns/op
BenchmarkFromInt3216ConstES-10          1000000000               5.431 ns/op
BenchmarkFromUint3232Slow-10            1000000000               8.697 ns/op
BenchmarkFromUint3232ConstES-10         1000000000               6.613 ns/op
BenchmarkFromUint3216ConstES-10         1000000000               5.427 ns/op
BenchmarkFromInt6432Slow-10             1000000000              10.10 ns/op
BenchmarkFromInt6432ConstES-10          1000000000               8.242 ns/op
BenchmarkFromInt6416ConstES-10          1000000000               5.619 ns/op
BenchmarkFromUint6432Slow-10            1000000000               8.985 ns/op
BenchmarkFromUint6432ConstES-10         1000000000               7.253 ns/op
BenchmarkFromUint6416ConstES-10         1000000000               5.582 ns/op
BenchmarkFromInt1616ConstES-10          1000000000              11.18 ns/op
BenchmarkFromUint1616ConstES-10         1000000000               7.202 ns/op
```

### softposit

These are directly comparible to the go benchmarks as they use the same data.
As you can see, these are quite a bit slower than the rust version.

In the soft-posit readme it is recomended to enable optimisations in their makefile,
however this did not bring any siginificant improvement.
```
BenchmarkCAdd32ES2-10           1000000000              11.15 ns/op
BenchmarkCMul32ES2-10           1000000000              10.89 ns/op
BenchmarkCDiv32ES2-10           1000000000              17.83 ns/op
BenchmarkCSqrt32ES2-10          1000000000              10.57 ns/op
BenchmarkCTrunc32ES2-10         1000000000               7.333 ns/op
```

### softposit-rs

(also using cset and no boost)
32P2:
| Operation | ns/op on my machine | 
|:---------:|:-------------------:|
|    Add    | 8.4ns/op            | 
|    -      | 7.9ns/op            |
|    *      | 7.1ns/op            |
|    /      | 11.1ns/op           |
|    sqrt   | 8.2ns/op            |
|   trunc   | -                   |
|    ⌊x⌉    | 3.8ns/op            |
|  exp(x)   | 607ns/op            |
|   ln(x)   | 600ns/op            |

16P1
| Operation | ns/op on my machine | 
|:---------:|:-------------------:|
|    Add    | 10.1ns/op           | 
|    -      | 8.5ns/op            |
|    *      | 8.2ns/op            |
|    /      | 11.3ns/op           |
|    sqrt   | 9.0ns/op            |
|   trunc   | -                   |
|    ⌊x⌉    | 5.2ns/op            |
|  exp(x)   | 7ns/op              |
|   ln(x)   | 12ns/op             |


## Exp

Exp is inheritly not an exact function, in any implementation. It wil always be an approximation. However that does mean that this is hard to fuzz, as there could be small errors and you would have to manually check every error and mark it as either acceptable or fix them.

Since this is not an exact funciton, I have instead decided to show its correctness via some plots of `exp(x)` in Posit vs float64 implementation:

![](grapher/exp_plot.svg)

As you can see these match pretty well at least up to 10.

Below you can see the erro graph between 0 and 5:

![](grapher/exp_small_error_32_plot.svg)

As you can see 32 bit posits are practically always better than float32.

At the other end of the spectrum: 

![](grapher/exp_big_error_32_plot.svg)

At the higher end of the scale posits dont do so well. Up to about 25 posits to better, but past that float32 is better in most cases.
This is to be expected, since posits get less accurate the bigger you go compared to floats.
The error is still measured in 0.05%, so it should still be good for computations. But if you go much bigger, to the range of 100 the error becomes unreasonably large.
This is due ot the inherit limitations of posits. I checked some numbers that high, and they were all as accurate as possible for posits. There might still be some improvements however.

For 16 bit posits its really not looking great. 

![](grapher/exp_small_error_16_plot.svg)

At the smaller end of the scale the errors get big fast already already, and its even worse on the higher end. This is mostly just due to posit16 having a lot less bits. However it is still very good if data size is important to you.
Keep in mind that this error is compared to a 64 bit float.
At the high end exp just returns the maximum posit16 available for basically the whole range.

![](grapher/exp_big_error_16_plot.svg)


[positwiki]: https://en.wikipedia.org/wiki/Unum_(number_format)#Unum_III
[positpaper]: http://www.johngustafson.net/pdfs/BeatingFloatingPoint.pdf
