package main

import (
	"fmt"
	"io"
	"os"
	"text/template"
)

var options = map[string][2]uint8{
	"32ES2": {32, 2},
	"16ES1": {16, 1},
}

//for 128bit posits: make sure all the types work for this bitcount. int8 wont be enough anymore :(
func main() {
	pack := os.Args[1]
	for _, v := range os.Args[2:] {
		size := options[v][0]
		es := options[v][1]
		file, err := os.Create(fmt.Sprintf("posit%des%d.go", size, es))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		generateHeader(pack, file)
		generateType(size, es, file)
		generateFromBits(size, es, file)
		generateToFloat(size, es, file)
		generateToDecimal(size, es, file)
		generateFromFloat(size, es, file)
		generateFromComponents(size, es, file)

		generateFromInts(size, es, 64, file)
		generateFromInts(size, es, 32, file)
		if size != 32 && size != 64 {
			generateFromInts(size, es, size, file)
		}

		generateToInts(size, es, 64, file)
		generateToInts(size, es, 32, file)

		generateEquality(size, es, file)
		generateAdd(size, es, file)
		generateSub(size, es, file)
		generateNeg(size, es, file)
		generateMul(size, es, file)
		generateDiv(size, es, file)
		generateSqrt(size, es, file)
		generateCeil(size, es, file)
		generateTrunc(size, es, file)
		generateRound(size, es, file)
		generateExp(size, es, file)
		generateLog2(size, es, file)
		generateLoge(size, es, file)
		generateLogb(size, es, file)
		generatePow(size, es, file)
	}
}

func generateHeader(pack string, out io.Writer) {
	executeTemplate(0, 0, pack, headerTemplate, nil, out)
}
func generateType(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", typeTemplate, nil, out)
}
func generateFromBits(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", frombitsTemplate, nil, out)
}
func generateToFloat(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", toFloatTemplate, nil, out)
}
func generateFromFloat(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", FromFloatTemplate, nil, out)
}
func generateFromComponents(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", FromComponentsTemplate, nil, out)
}
func generateFromInts(bits, es, inbits uint8, out io.Writer) {
	executeTemplate(bits, es, "", FromIntegerTemplate, map[string]interface{}{"inbits": inbits}, out)
}
func generateToInts(bits, es, outbits uint8, out io.Writer) {
	executeTemplate(bits, es, "", ToIntegerTemplate, map[string]interface{}{"outbits": outbits}, out)
}
func generateToDecimal(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", ToDecimalTemplate, nil, out)
}
func generateEquality(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", EqualityTemplate, nil, out)
}
func generateAdd(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", AddTemplate, nil, out)
}
func generateSub(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", SubTemplate, nil, out)
}
func generateNeg(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", NegTemplate, nil, out)
}
func generateMul(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", MulTemplate, nil, out)
}
func generateDiv(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", DivTemplate, nil, out)
}
func generateSqrt(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", SqrtTemplate, nil, out)
}
func generateCeil(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", CeilTemplate, nil, out)
}
func generateTrunc(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", TruncateTemplate, nil, out)
}
func generateRound(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", RoundTemplate, nil, out)
}
func generateExp(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", ExpTemplate, nil, out)
}
func generateLog2(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", Log2Template, nil, out)
}
func generateLoge(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", LogeTemplate, nil, out)
}
func generateLogb(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", LogbTemplate, nil, out)
}
func generatePow(bits, es uint8, out io.Writer) {
	executeTemplate(bits, es, "", PowTemplate, nil, out)
}

const headerTemplate = `package {{.package}}

import (
	"math"
	"math/bits"
	"strconv"
	"strings"
)

`
const typeTemplate = `type {{template "typename" .}} struct {
	num {{template "numtyp" .rbits}}
}

`

const frombitsTemplate = `func NewPosit{{.bits}}es{{.es}}FromBits(bits {{template "numtyp" .rbits}}) {{template "typename" .}} {
	return {{template "typename" .}}{num: bits}
}

`

const toFloatTemplate = `func (a {{template "typename" .}}) Float() float64 {
	if a.num == 0 {
		return 0.0
	}
	if a.num == {{.signmask}} {
		return math.Inf(1)
	}
	aneg := a.num&({{.signmask}}) != 0
	if aneg {
		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}
	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum << {{.es}} | {{.signmask}}
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	frac :=  float64(afracP1)/float64(uint32(0b1<<({{.rbits}}-1)))
	if aneg {
		return -(math.Pow(2.0, float64(ascale)) * frac)
	} else {
		return math.Pow(2.0, float64(ascale)) * frac
	}
}

`

const FromFloatTemplate = `func NewPosit{{.bits}}ES{{.es}}FromFloat(f float64) {{template "typename" .}} {
	if math.IsNaN(f) || math.IsInf(f, 0) {
		return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
	}

	bits := math.Float64bits(math.Abs(f))

	scale := int16((bits&0x7fffffffffffffff)>>52)
	combinedFrac := bits & 0x000fffffffffffff
	if scale == 0 {
		if combinedFrac == 0 {
			return {{template "typename" .}}{
				num: 0,
			}
		} else {
			combinedFrac >>= 1
			scale++
		}
	}
	scale -= 1023

	{{if le .bits 52}}combinedFrac >>= 52 - {{.bits}}{{else}}combinedFrac <,= {{.bits}} - 52{{end}}
	endreg := scale >> {{.es}}
	endexp := {{template "numtyp" .rbits}}(scale - (endreg << {{.es}}))

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > {{.bits}}-2 {
			outPosit = 1
			if math.Signbit(f) {
				outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
			}
			return {{template "typename" .}}{num: outPosit}
		}
		outPosit = (0b1 << ({{.maxshift}} - 1)) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + endreg)
		if outn > {{.bits}}-2 {
			outPosit = {{.maxmask}} >> 1
			if math.Signbit(f) {
				outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
			}
			return {{template "typename" .}}{num: outPosit}
		}
		outPosit = ({{.maxmask}} >> 1) - (({{.maxmask}} >> 1) >> (outn & {{template "shiftmask" .rbits}}))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := {{.maxshift}} - {{.es}} - outn - 1

	combinedFrac >>= {{.es}} + outn + 2

	outPosit |= {{template "numtyp" .rbits}}(endexp) << outFracBits
	outPosit |= {{template "numtyp" .rbits}}(combinedFrac)

	if math.Signbit(f) {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}
`

const FromComponentsTemplate = `
//This returns a Posit that best represents neg*2^scale*(1+frac)
func NewPosit{{.bits}}ES{{.es}}FromComponents(neg bool, scale int, frac {{template "numtyp" .rbits}}) {{template "typename" .}} {
	endreg := scale >> {{.es}}
	endexp := {{template "numtyp" .rbits}}(scale - (endreg << {{.es}}))

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > {{.bits}}-1 {
			return {{template "typename" .}}{num: 0}
		}
		outPosit = (0b1 << ({{.maxshift}} - 1)) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + endreg)
		if outn > {{.bits}}-2 {
			outPosit = {{.maxmask}} >> 1
			if neg {
				outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
			}
			return {{template "typename" .}}{num: outPosit}
		}
		outPosit = ({{.maxmask}} >> 1) - (({{.maxmask}} >> 1) >> (outn & {{template "shiftmask" .rbits}}))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := {{.maxshift}} - {{.es}} - outn - 1

	frac >>= {{.es}} + outn + 2

	outPosit |= {{template "numtyp" .rbits}}(endexp) << outFracBits
	outPosit |= {{template "numtyp" .rbits}}(frac)

	if neg {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}
`

const FromIntegerTemplate = `
func NewPosit{{.bits}}ES{{.es}}FromInt{{.inbits}}(x {{template "snumtyp" .inbits}}) {{template "typename" .}} {
	var a {{template "typename" .}}
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := {{template "numtyp" .inbits}}(x)
	outscale := uint8(bits.Len{{.inbits}}(outfrac >> 1))
	outfrac <<= {{.inbits}}-1 - outscale

	endreg := outscale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(outscale) &^ (f << {{.es}})
	if endreg > {{.bits}}-3 {
		a.num = {{.maxmask}} >> 1
		if aneg {
			a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
		}
		return a
	}
	a.num = {{.maxmask}} - (({{.maxmask}}>>1) >> endreg)

	h := {{template "numtyp" .rbits}}({{.maxmask}} >> {{.es}})
	a.num |= bits.RotateLeft{{.rbits}}(endexp, {{.bits}}-int(endreg)-2-{{.es}}) & h
	a.num |= {{template "numtyp" .rbits}}(((outfrac&{{template "nbitmask" .inbits}})>>({{.inbits}}-{{.bits}}+{{.es}}+1)) >> endreg)

{{if eq .inbits .bits}}
	mask := {{template "numtyp" .inbits}}((1<<({{.es}}+1))<<endreg)
{{/*TODO: This could probably be simplifid*/}}
{{else if le .inbits .bits}}
	var mask {{template "numtyp" .inbits}}
	if endreg >= {{.inbits}}-{{.bits}}-{{.es}}-1 {
		mask = {{template "numtyp" .inbits}}(((1<<({{.es}}+1))<<endreg)>>({{.bits}}-{{.inbits}}))
	}else{
		mask = 1
	}
{{else}}
	mask := {{template "numtyp" .inbits}}((1<<({{.inbits}}-{{.bits}}+{{.es}}+1))<<endreg)
{{end}}
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}
	return a
}

func NewPosit{{.bits}}ES{{.es}}FromUint{{.inbits}}(x {{template "numtyp" .inbits}}) {{template "typename" .}} {
	var a {{template "typename" .}}
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := {{template "numtyp" .inbits}}(x)
	outscale := uint8(bits.Len{{.inbits}}(outfrac >> 1))
	outfrac <<= {{.inbits}} - 1 - outscale

	endreg := outscale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(outscale) &^ (f << {{.es}})

	if endreg > {{.bits}}-3 {
		a.num = {{.maxmask}} >> 1
		return a
	}
	a.num = {{.maxmask}} - (({{.maxmask}}>>1) >> endreg)

	h := {{template "numtyp" .rbits}}({{.maxmask}} >> {{.es}})
	a.num |= bits.RotateLeft{{.rbits}}(endexp, {{.bits}}-int(endreg)-2-{{.es}}) & h
	a.num |= {{template "numtyp" .rbits}}(((outfrac&{{template "nbitmask" .inbits}})>>({{.inbits}}-{{.bits}}+{{.es}}+1)) >> endreg)

{{if eq .inbits .bits}}
	mask := {{template "numtyp" .inbits}}((1<<({{.es}}+1))<<endreg)
{{/*TODO: This could probably be simplifid*/}}
{{else if le .inbits .bits}}
	var mask {{template "numtyp" .inbits}}
	if endreg >= {{.inbits}}-{{.bits}}-{{.es}}-1 {
		mask = {{template "numtyp" .inbits}}(((1<<({{.es}}+1))<<endreg)>>({{.bits}}-{{.inbits}}))
	}else{
		mask = 1
	}
{{else}}
	mask := {{template "numtyp" .inbits}}((1<<({{.inbits}}-{{.bits}}+{{.es}}+1))<<endreg)
{{end}}
	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}
`

const ToIntegerTemplate = `
func (a {{template "typename" .}}) Int{{.outbits}}() {{template "snumtyp" .outbits}} {
	if a.num == 0 || a.num == 1<<{{.maxshift}} {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = {{template "numtyp" .bits}}(-{{template "snumtyp" .bits}}(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	ascale -= {{.bits}}-1
	if ascale < 0 {
		ret := afracP1 >> {{template "numtyp" .outbits}}(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<{{template "numtyp" .outbits}}(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -{{template "snumtyp" .outbits}}(ret)
		}
		return {{template "snumtyp" .outbits}}(ret)
	} else if ascale >= {{.bits}} {
		if aneg {
			return math.MinInt{{.outbits}}
		}
		return math.MaxInt{{.outbits}}
	}
	ret := {{template "numtyp" .outbits}}(afracP1) << {{template "numtyp" .outbits}}(ascale)
	if aneg {
		return {{template "snumtyp" .outbits}}(-ret)
	}
	return {{template "snumtyp" .outbits}}(ret)
}

func (a {{template "typename" .}}) Uint{{.outbits}}() {{template "numtyp" .outbits}} {
	if a.num == 0 || a.num == 1<<{{.maxshift}} || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	ascale -= {{.bits}}-1
	if ascale < 0 {
		ret := afracP1 >> {{template "numtyp" .outbits}}(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<{{template "numtyp" .outbits}}(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return {{template "numtyp" .outbits}}(ret)
	} else if ascale >= {{.bits}} + 1 {
		return math.MaxUint{{.outbits}}
	}
	return {{template "numtyp" .outbits}}(afracP1) << {{template "numtyp" .outbits}}(ascale)
}

`

const ToDecimalTemplate = `
// String converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a {{template "typename" .}}) String() string {
	return a.FormatScientific()
}

// FormatScientific converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a {{template "typename" .}}) FormatScientific() string {
	switch a.num {
	case 0:
		return "0"
	case 1<<{{.maxshift}}:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	start := i - 1
	end := i - 1

	var s string
	if aneg {
		buf[i-2] = '-'
		start--
	}

	buf[i-1] = buf[i]

	if i+1 != len(buf) {
		last_non0 := 0
		for index, c := range buf[i+1:] {
			if c != '0' {
				last_non0 = index + 2
			}
		}
		buf[i] = '.'
		end = i - 1 + last_non0
	}
	s = string(buf[start : end+1])
	power := int64(ascaleB10) + int64(len(buf[i+1:]))
	if power == 1 {
		s += "*10"
	} else if power != 0 {
		s += "*10^" + strconv.FormatInt(int64(ascaleB10)+int64(len(buf[i+1:])), 10)
	}
	return s
}

// FormatBasic converts the Posit into a string in basic numerical notation.
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a {{template "typename" .}}) FormatBasic() string {
	switch a.num {
	case 0:
		return "0"
	case 1<<{{.maxshift}}:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	var s string
	if aneg {
		s = "-"
	}
	switch {
	case int32(len(buf))+int32(ascaleB10) > int32(i):
		s += string(buf[i : len(buf)+int(ascaleB10)])
		if ascaleB10 != 0 {
			s += "."
			s += string(buf[len(buf)+int(ascaleB10):])
		}
	case int32(len(buf))+int32(ascaleB10) <= int32(i):
		s += "0."
		for int32(len(buf))+int32(ascaleB10) < int32(i) {
			s += "0"
			ascaleB10++
		}

		s += string(buf[i:])
	}
	s = strings.TrimRight(s, "0")
	s = strings.TrimRight(s, ".")
	return s
}

func (a {{template "typename" .}}) getDigits() ([64]byte, {{template "snumtyp" .rbits}}, int, bool) {
	aneg := a.num&({{.signmask}}) != 0
	if aneg {
		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}
	var reg int8
	r1 := {{template "numtyp" .rbits}}(2)
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		reg = 0
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}

	sftnum <<= 2
	afracP1 := {{template "numtyp" .drbits}}((sftnum << ({{.es}} & 0x1f)) | (1 << {{.maxshift}}))
	sftnum <<= 1
	aexp := sftnum >> ({{.rbits}} - {{.es}})
	ascaleB2 := ({{template "snumtyp" .rbits}}(reg) << ({{.es}} & 0x1f)) + {{template "snumtyp" .rbits}}(aexp)

	
	ascaleB10 := ascaleB2 - ({{.rbits}} - 1)
	ascaleB5 := -ascaleB10
	if ascaleB5 < 0 {
		if ascaleB5 < - {{.rbits}}{
			ascaleB5 += {{.rbits}}
			afracP1 <<= {{.rbits}}
		} else {
			afracP1 <<= -ascaleB5
			ascaleB5 = 0
		}
		ascaleB10 = 0
	}

	//we need to have at least 3 bits clean for a non-rounded multiplication by 5
	for ascaleB5 > 0 && afracP1>>({{.drbits}}-3) == 0 {
		afracP1 *= 5
		ascaleB5--
	}

	var buf [64]byte
	i := len(buf)

	for afracP1 >= 100 {
		is := afracP1 % 100 * 2
		afracP1 /= 100
		i -= 2
		buf[i+1] = smallsString[is+1]
		buf[i+0] = smallsString[is+0]
		//we need to have at least 3 bits clean for a non-rounded multiplication by 5
		for ascaleB5 > 0 && afracP1>>({{.drbits}}-3) == 0 {
			afracP1 *= 5
			ascaleB5--
		}
		for ascaleB5 < 0 && afracP1>>({{.drbits}}-1) == 0 {
			afracP1 <<=1
			ascaleB5++
		}
	}

	// afracP1 < 100
	is := afracP1 * 2
	i--
	buf[i] = smallsString[is+1]
	if afracP1 >= 10 {
		i--
		buf[i] = smallsString[is]
	}
	return buf, ascaleB10, i, aneg
}
`

const EqualityTemplate = `
func (a {{template "typename" .}}) Le(b {{template "typename" .}}) bool {
	return a.num <= b.num
}

func (a {{template "typename" .}}) Lt(b {{template "typename" .}}) bool {
	return a.num < b.num
}

func (a {{template "typename" .}}) Eq(b {{template "typename" .}}) bool {
	return a.num == b.num
}
`

const AddTemplate = `
func (a {{template "typename" .}}) Add(b {{template "typename" .}}) {{template "typename" .}} {
	aneg := a.num&({{.signmask}}) != 0
	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
		}

		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	} else {
		if a.num == 0{
			return b
		}	
	}

	xneg := aneg
	bneg := b.num&({{.signmask}}) != 0
	if bneg {
		if b.num == 1<<{{.maxshift}} {
			return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
		}

		xneg = !xneg
		b.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(b.num))
	} else {
		if b.num == 0{
			if aneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}	
	}

	//This allows less branching later on.
	//This potentially leaves bneg in the wrong position, dont use bneg!!
	if a.num < b.num {
		a.num, b.num = b.num, a.num
		aneg = bneg
	} else if a.num == -b.num {
		a.num = 0
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	//B
	sftnum = b.num
	if b.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	bexp := sftnum >> ({{.bits}} - {{.es}})
	bscale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(bexp)

	//Calc

	// We are using a trick to not have to double-negate.
	// The simple way to do this would be to negate af and fb
	// on aneg and bneg respectively, and negate the result if the result should be negated.
	// Instead we take advantage that a>b, so we never have to negate a and
	// we can negate b if and only if bneg^aneg.

	combinedFrac := {{template "numtyp" .drbits}}(afracP1) << {{.maxshift}}

	tmp := bscale - ascale + {{.maxshift}}
	if tmp > 0 {
		bf := {{template "numtyp" .drbits}}(bfracP1) << (tmp & {{template "dshiftmask" .rbits}})
		if xneg {
			combinedFrac -= bf
		} else {
			combinedFrac += bf
		}
	}

	if xneg {
		if combinedFrac == 0 {
			a.num = 0
			return a
		}
		//This is faster than LeadingZeros
		for (combinedFrac>>({{.drbits}}-2))&1 == 0 {
			ascale--
			combinedFrac <<= 1
		}
	}

	// leave the hidden bit in
	overflow := {{template "snumtyp" .rbits}}(combinedFrac >> {{.dmaxshift}})
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(ascale) &^ (f << {{.es}})

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > {{.bits}}-2 {
			a.num = 1
			if aneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = (0b1 << {{.maxshift}}) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + endreg)
		if outn > {{.bits}}-2 {
			a.num = {{.maxmask}} >> 1
			if aneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = {{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}}))
	}

	combinedShift := ({{.es}} + outn) & {{template "shiftmask" .drbits}}
	combinedFrac >>= combinedShift

	outfrac := {{template "numtyp" .rbits}}(combinedFrac >> {{.maxshift}}) & (({{.maxmask}} >> (1 + {{.es}})) >> (outn & {{template "shiftmask" .rbits}}))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft{{.rbits}}(endexp, {{.maxshift}}-int(combinedShift)) & ({{.maxmask}} >> {{.es}})

	if combinedFrac&({{.maxmask}}>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if aneg {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}

`

const SubTemplate = `func (a {{template "typename" .}}) Sub(b {{template "typename" .}}) {{template "typename" .}} {
	b.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(b.num))
	return a.Add(b)
}

`

const NegTemplate = `func (a {{template "typename" .}}) Neg() {{template "typename" .}} {
	a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	return a
}

`

const MulTemplate = `func (a {{template "typename" .}}) Mul(b {{template "typename" .}}) {{template "typename" .}} {
	//A
	aneg := a.num&({{.signmask}}) != 0
	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
		}

		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}

	xneg := aneg
	bneg := b.num&({{.signmask}}) != 0
	if bneg {
		if b.num == 1<<{{.maxshift}} {
			return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
		}
	
		xneg = !xneg
		b.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(b.num))
	}


	if a.num == 0 || b.num == 0 {
		return NewPosit{{.bits}}es{{.es}}FromBits(0)
	}

	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}

	sftnum <<= 2
	afracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	//B
	sftnum = b.num
	if b.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	bexp := sftnum >> ({{.bits}} - {{.es}})
	ascale += ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(bexp)

	//Calc

	af := {{template "snumtyp" .drbits}}(afracP1)
	bf := {{template "snumtyp" .drbits}}(bfracP1)
	combinedFrac := {{template "numtyp" .drbits}}(af * bf)

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit {{.bits}}-2 for the number

	// leave the hidden bit in
	overflow := {{template "snumtyp" .rbits}}(combinedFrac >> {{.dmaxshift}})
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(ascale) &^ (f << {{.es}})

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > {{.bits}}-2 {
			a.num = 1
			if xneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = (0b1 << {{.maxshift}}) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + endreg)
		if outn > {{.bits}}-2 {
			a.num = {{.maxmask}} >> 1
			if xneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = {{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}}))
	}

	combinedShift := ({{.es}} + outn) & {{template "shiftmask" .drbits}}
	combinedFrac >>= combinedShift

	outfrac := {{template "numtyp" .rbits}}(combinedFrac >> {{.maxshift}}) & (({{.maxmask}} >> (1 + {{.es}})) >> (outn & {{template "shiftmask" .rbits}}))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft{{.rbits}}(endexp, {{.maxshift}}-int(combinedShift)) & ({{.maxmask}} >> {{.es}})

	if combinedFrac&({{.maxmask}}>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}
`

const DivTemplate = `
func (a {{template "typename" .}}) Div(b {{template "typename" .}}) {{template "typename" .}} {
	//A
	aneg := a.num&({{.signmask}}) != 0
	xneg := aneg
	bneg := b.num&({{.signmask}}) != 0
	
	if bneg {
		if b.num == 1<<{{.maxshift}} {
			return b
		}
	
		xneg = !xneg
		b.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(b.num))
	} else if b.num == 0 {
		return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
	}

	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return a
		}
	
		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	} else if a.num == 0 {
		return a
	}

	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	//B
	sftnum = b.num
	if b.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << {{.es}}) | (1 << {{.maxshift}})
	sftnum <<= 1
	bexp := sftnum >> ({{.bits}} - {{.es}})
	ascale -= ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(bexp)

	//Calc

	af := {{template "numtyp" .drbits}}(afracP1)
	bf := {{template "numtyp" .drbits}}(bfracP1)
	combinedFrac := {{template "numtyp" .drbits}}(af<<({{.maxshift}}-1)) / bf

	if combinedFrac == 0 {
		a.num = 0
		return a
	}
	rem := {{template "numtyp" .drbits}}(af<<({{.maxshift}}-1)) % ({{template "numtyp" .drbits}}(bf))

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit {{.bits}}-2 for the number

	// leave the hidden bit in
	overflow := combinedFrac>>({{.maxshift}}-1) == 0
	if overflow {
		ascale -= 1
		combinedFrac <<= 1
	}

	endreg := ascale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(ascale) &^ (f << {{.es}})

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > {{.bits}}-2 {
			a.num = 1
			if xneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = (0b1 << {{.maxshift}}) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + endreg)
		if outn > {{.bits}}-2 {
			a.num = {{.maxmask}} >> 1
			if xneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
			}
			return a
		}
		outPosit = {{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}}))
	}

	combinedShift := ({{.es}} + outn - 1) & {{template "shiftmask" .drbits}}

	outfrac := ({{template "numtyp" .rbits}}(combinedFrac) >> (combinedShift&{{template "shiftmask" .drbits}}) ) & (({{.maxmask}} >> (1 + {{.es}})) >> (outn & {{template "shiftmask" .rbits}}))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft{{.rbits}}(endexp, {{.maxshift}}-{{.es}}-int(outn)) & ({{.maxmask}} >> {{.es}})

	mask := {{template "numtyp" .drbits}}(1 << (combinedShift & 0x3f)) -1
	if combinedFrac&mask != 0 || rem != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}
`
const SqrtTemplate = `
func (a {{template "typename" .}}) Sqrt() {{template "typename" .}} {
	if a.num>>{{.maxshift}} != 0 {
		return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		if a.num == 0 {
			return a
		}
	
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum << {{.es}}
	sftnum <<= 1
	aexp := sftnum >> ({{.bits}} - {{.es}})

	//Calc
	n := {{template "numtyp" .drbits}}(afracP1) << {{.maxshift}}
	if aexp&1 != 0 {
		n = (n << 1) | (0b1 << {{.dmaxshift}})
		n -= 1 << ({{.drbits}} - 2)
	}

	if reg&1 != 0 {
		aexp += 1 << {{.es}}
	}
	reg >>= 1
	aexp >>= 1

	//out
	var outPosit {{template "numtyp" .rbits}}
	var outn uint8
	if reg < 0 {
		outn = uint8(-reg)
		outPosit = (0b1 << ({{.maxshift}} - 1)) >> (outn & {{template "shiftmask" .rbits}})
	} else {
		outn = uint8(1 + reg)
		outPosit = ({{.maxmask}} >> 1) - (({{.maxmask}} >> 1) >> (outn & {{template "shiftmask" .rbits}}))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := {{.maxshift}} - 1 - {{.es}} - outn

	//more calc
	//https://stackoverflow.com/a/31120562

	combinedFrac := {{template "numtyp" .drbits}}(1 << ({{.drbits}} - 2 - 2*{{.es}} - (outn << 1)))
	one := combinedFrac >> 2
	n >>= ((outn << 1) + 2*{{.es}}) & {{template "dshiftmask" .bits}}

	//There are some tricky performance characteristics of modifying this code
	//Allignment changes the benchmark for this loop a lot (jumping to aligned addeesses ...)
	//Dont be allarmed if suddenly this code performs worse with a small change
	for one != 0 {
		a1 := combinedFrac + one
		if n >= a1 {
			n -= a1
			combinedFrac += one << 1
		}
		combinedFrac >>= 1
		one >>= 2
	}
	g := uint8((2 + {{.es}} + outn) & {{template "dshiftmask" .rbits}})
	outPosit |= {{template "numtyp" .rbits}}(aexp) << outFracBits

	outfrac := {{template "numtyp" .rbits}}(combinedFrac>>1) << g
	outfrac >>= g

	outPosit |= outfrac
	outPosit += {{template "numtyp" .rbits}}(combinedFrac) & 1
	return {{template "typename" .}}{num: outPosit}
}
`
const CeilTemplate = `
func (a {{template "typename" .}}) Ceil() {{template "typename" .}} {
	//A
	decnum := a.num
	aneg := decnum>>{{.maxshift}} != 0
	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return a
		}

		decnum = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}
	if a.num == 0 {
		return a
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&({{.firstReg}}) == 0 {
		if aneg {
			a.num = 0
		} else {
			a.num = {{.firstReg}}
		}
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := {{template "numtyp" .rbits}}(1 << ({{.bits}} - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= {{.bits}} - {{.es}}
	ascale := ({{template "numtyp" .rbits}}(m) << {{.es}}) + aexp

	//calculate the how many bits are "bad"
	badBits := int8({{.bits}}-2-{{.es}}-m-1) - int8(ascale)
	if badBits >= 0 {
		leftover := decnum &^ ({{.maxmask}} << (badBits & 0x1f))
		decnum &= ({{.maxmask}} << (badBits & 0x1f))
		if leftover != 0 != aneg {
			decnum += 1 << (badBits & 0x1f)
		}

		if aneg {
			a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(decnum))
		} else {
			a.num = decnum
		}
	}

	return a
}
`
const TruncateTemplate = `
func (a {{template "typename" .}}) Truncate() {{template "typename" .}} {
	//A
	decnum := a.num
	aneg := decnum>>{{.maxshift}} != 0
	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return a
		}

		decnum = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&({{.firstReg}}) == 0 {
		a.num = 0
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := {{template "numtyp" .rbits}}(1 << ({{.bits}} - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= {{.bits}} - {{.es}}

	ascale := ({{template "numtyp" .rbits}}(m) << {{.es}}) + aexp

	//Calc
	tmp := int8({{.bits}}-2-{{.es}}-m-1) - int8(ascale)
	if tmp >= 0 {
		if aneg {
			a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(decnum & ({{.maxmask}} << (tmp & 0x1f))))
		} else {
			a.num = a.num & ({{.maxmask}} << (tmp & 0x1f))
		}
	}

	return a
}
`
const RoundTemplate = `
func (a {{template "typename" .}}) Round() {{template "typename" .}} {
	//A
	decnum := a.num
	aneg := decnum>>{{.maxshift}} != 0

	if aneg {
		if a.num == 1<<{{.maxshift}} {
			return NewPosit{{.bits}}es{{.es}}FromBits(1 << {{.maxshift}})
		}
		decnum = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(decnum))
	}

	if decnum&({{.firstReg}}) == 0 {
		aexp := decnum
		if aexp&(1 << ({{.maxshift}} - 2)) == 0 {
			//the second time we know for sure an>1 aka a<0.5
			a.num = 0
			return a
		}
		aexp <<= 3
		aexp >>= {{.bits}} - {{.es}}

		if aexp != (1<<{{.es}})-1 || decnum<<(3+{{.es}}) == 0 {
			a.num = 0
			return a
		} else {
			if aneg {
				a.num = 0b11 << ({{.maxshift}} - 1)
				return a
			}else{
				a.num = 0b1 << ({{.maxshift}} - 1)
				return a
			}
		}
	}

 	var an = uint8(1)
	aexp := decnum
	for aexp&(1 << ({{.maxshift}} - 2)) != 0 {
		an++
		aexp <<= 1
	}

	aexp <<= 3
	aexp >>= {{.bits}} - {{.es}}

	var m = an - 1
	ascale := ({{template "numtyp" .rbits}}(m) << {{.es}}) + aexp

	//Calc
	// we need to make sure to tie-break to nearest even, not uneven.
	// To make sure 1.5 also breaks correctly, we | the hidden bit
	tmp := {{.es}} + an + uint8(ascale)
	if an < {{.maxshift}}-{{.es}} && (((decnum|((0x1<<({{.bits}}-2-{{.es}}))>>(an&0x0f)))>>(({{.maxshift}}-2-tmp)&{{template "shiftmask" .rbits}}))&3 == 3 || (decnum<<((2+tmp)&{{template "shiftmask" .rbits}})) > (1<<{{.maxshift}})) {
		//slowpath
		afracP1 := decnum | (0b1 << (({{.maxshift}} - ({{.es}} + an + 1)) & {{template "shiftmask" .rbits}}))
		afracP1 &= ({{.maxmask}} >> ({{.es}} + 1)) >> (an & {{template "shiftmask" .rbits}})
		afracP1 += (0b1 << ({{.maxshift}} - 1)) >> tmp
		if afracP1&((0b1<<({{.maxshift}}-{{.es}}))>>(an&{{template "shiftmask" .rbits}})) != 0 {
			ascale++
			tmp++

			endreg := ascale >> {{.es}}
			f := {{template "numtyp" .rbits}}({{.maxmask}})
			endexp := {{template "numtyp" .rbits}}(ascale) &^ (f << {{.es}})
		
			outn := uint8(1 + endreg)
			a.num = ({{.maxmask}} >> 1) - (({{.maxmask}} >> 1) >> (outn & {{template "shiftmask" .rbits}}))

			outFracBits := {{.maxshift}} - 1 - {{.es}} - outn
			afracP1 >>= 1
			afracP1 &= (({{.maxmask}} >> (2 + {{.es}})) >> (outn & {{template "shiftmask" .rbits}}))
			a.num |= endexp << (outFracBits & {{template "shiftmask" .rbits}})
		} else {
			outFracBits := {{.maxshift}} - 1 - {{.es}} - an
			a.num = decnum & ({{.maxmask}} << (outFracBits & {{template "shiftmask" .rbits}}))
			afracP1 &= ({{.maxmask}} >> (2 + {{.es}})) >> (an & {{template "shiftmask" .rbits}})
		}
		if ({{.maxshift}} - 1 - int8(tmp)) >= 0 {
			afracP1 = afracP1 & ({{.maxmask}} << (({{.maxshift}} - 1 - tmp) & {{template "shiftmask" .rbits}}))
		}
		a.num |= afracP1
		if aneg {
			a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
		}
		return a
	} else {
		tmp := int8({{.maxshift}}-1-{{.es}}-an) - int8(ascale)
		if tmp >= 0 {
			if aneg {
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(decnum & ({{.maxmask}} << (tmp & 0x1f))))
			} else {
				a.num = a.num & ({{.maxmask}} << (tmp & 0x1f))
			}
		}
	}
	return a
}
`

const ExpTemplate = `
func (a {{template "typename" .}}) Exp() {{template "typename" .}} {
	//TODO: should we have per-size tables and calculations? like a 32 bit table since we wont need more than 16 bits of accuracy anyways

	//A
	aneg := a.num&({{.signmask}}) != 0
	if aneg {
		a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(a.num))
	}

	// check for things that result in 1
	// Modifies version of normal calculations.
	// scale = -28, but its negated since to avoid negating it at runtime
	// also avoids branches because ist never bigger than 30
	if a.num <= {{.expmaxone}} {
		a.num = {{.firstReg}}
		return a
	}

	if a.num >= {{.expminoverflow}} {
		a.num = {{.maxmask}} >> 1
		if aneg {
			a.num = 0
		}
		return a
	}

	var reg int8
	r1 := uint8(1)
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}
	n := {{.bits}} - 1 - {{.es}} - r1 - 1
	if n >= {{.bits}} {
		n = 0
	}

	afrac := a.num & ((1 << (n)) - 1)
	sftnum <<= 3
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg) << {{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	for afrac&1 == 0 && afrac != 0 {
		afrac >>= 1
		n--
	}

	if afrac == 0 || n > 32 {
		n = 0
	}

	var outscale uint64
	finalscale := uint64(1 << 63)
	var outfrac uint64
	var power uint64
	var powerOfTwo uint32
	var baseindex uint8

	// Instead of taking the base out of the array directly, we store the index.
	// This way we can look up ` + "`" + `base*base` + "`" + ` instead of calculating. This reduces errors.
	// From 29 on it always results in 1  ( in 32 bits)
	if ascale < 0 {
		// e^((1+a/2^n)*2^b)	=	e^(a/2^n*2^b)*e^(2^b)
		//						=	e^(a/2^n*2^b)*e^(2^b)
		//						=	(e^(1/2^n))^(a*2^b)*e^(1/2^(-b))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee2[-ascale]
		baseindex = n
		power = uint64(afrac >> -uint32(ascale))

		// We have rounded off everything after the binary point in the power.
		// Here we find the biggest fraction of this, so the biggest 1/2^n.
		// Then we add this again later
		powfracBits := uint8(-ascale)

		i := uint8(0)
		if int8(baseindex+powfracBits)+1-32 >= 0 {
			i = baseindex + powfracBits + 1 - 32
		}
		if afrac<<(32-i) != 0 {
			afrac += 1 << i
		}

		// This is prefered over doing it in the main loop,
		// because we dont have to check for overflow.
		for ; i < powfracBits; i++ {
			if afrac&(1<<i) != 0 {
				finalscale, _ = bits.Mul64(finalscale, erootee2[baseindex+powfracBits-i])
				finalscale <<= 1
				finalscale += 1
			}
		}
	} else if ascale < {{template "snumtyp" .rbits}}(n) {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^(n-b))
		//						=	e^(2^b)*e^a^(1/(2^(n-b)))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = n - uint8(ascale)
		power = uint64(afrac)
		outscale += 1 << (powerOfTwo & {{template "shiftmask" 64}})
	} else {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a*2^(b-n))
		//						=	e^(2^b)*e^(a*2^(b-n))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = 0
		power = uint64(afrac << (uint32(ascale) - uint32(n)))
		outscale += 1 << powerOfTwo
	}

	for powerOfTwo > 0 {
		powerOfTwo--
		outfrac, _ = bits.Mul64(outfrac, outfrac)

		if outfrac&(1<<63) != 0 {
			outscale += 1 << powerOfTwo
			outfrac >>= 1
		}
		outfrac <<= 1
	}

	if int(baseindex) < len(erootee2) {
		for ; power != 0 && baseindex != 0; power >>= 1 {
			if power&1 == 1 {
				outfrac, _ = bits.Mul64(outfrac, erootee2[baseindex])
				if outfrac&(1<<63) != 0 {
					outscale++
					outfrac >>= 1
				}
				outfrac <<= 1
			}
			baseindex--
		}
	}
	if power != 0 && baseindex == 0 {
		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, erootee0)

			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
		outscale += power
		power >>= 1
	}
	base := erootee0

	for ; power != 0; power >>= 1 {
		base, _ = bits.Mul64(base, base)
		if base&(1<<63) != 0 {
			outscale += power
			base >>= 1
		}
		base <<= 1

		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, base)
			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
	}
	outfrac, low := bits.Mul64(outfrac, finalscale)
	outfrac <<= 1

	endreg := outscale >> {{.es}}
	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(outscale) &^ (f << {{.es}})

	outn := uint8(1 + endreg)
	outPosit := {{template "numtyp" .rbits}}({{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}})))

	combinedShift := ({{.es}} + uint8(outn) - 1) & {{template "shiftmask" .drbits}}
	mask := uint64((2<<32)<<(combinedShift&{{template "shiftmask" .drbits}})) -1

	outfrac2 := outfrac >> (32 + 1+32-{{.bits}}) // now the frac is the entirity of the output

	outPosit |= ({{template "numtyp" .rbits}}(outfrac2) >> (combinedShift & {{template "shiftmask" .drbits}})) & (({{.maxmask}} >> ({{.es}}+1)) >> (outn&0x1f))
	outPosit |= bits.RotateLeft{{.rbits}}(endexp, {{.bits}}-2-int(combinedShift)) & ({{.maxmask}} >> {{.es}})

	if outPosit&2 == 2 || outfrac&mask != 0 || low != 0 {
		outPosit += 1
	}
	outPosit >>= 1

	p := {{template "typename" .}}{
		num: outPosit,
	}

	if aneg {
		p = p.Div(NewPosit{{.bits}}es{{.es}}FromBits({{.firstReg}}))
	}
	return p
}
`

const Log2Template = `
func (a {{template "typename" .}}) Log2() {{template "typename" .}} {
	// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation

	if a.num == 0 || a.num&({{.signmask}}) != 0{
		a.num = {{.signmask}}
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&({{.firstReg}}) == 0 {
		reg = -1
		for sftnum&({{.secondreg}}) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&({{.secondreg}}) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 1
	afracP1 := uint64(((sftnum << {{.es}}) | ({{.firstReg}})) &^ ({{.signmask}})) // shifted one to the right compared to normal afracP1
	afracP1 <<= (32-{{.bits}})
	sftnum <<= 2
	aexp := sftnum >> ({{.bits}} - {{.es}})
	ascale := ({{template "snumtyp" .rbits}}(reg)<<{{.es}}) + {{template "snumtyp" .rbits}}(aexp)

	n := {{template "numtyp" .rbits}}(ascale)
	if ascale < 0 {
		n = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(n)) -1
	}

	var outPosit {{template "numtyp" .rbits}}
	var outn uint8
	var series {{template "numtyp" .rbits}}
	var outscale int32
	seg := {{template "numtyp" .rbits}}(1 << ({{.maxshift}} - {{.es}}))

	// This is a very specialised version of the algorithm to use less bits.
	if ascale < 0 {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			var passedFirst bool
			var zeroCounter bool = true
			for afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				seg >>= 1
				if afracP1&(1<<31) != 0 {
					afracP1 >>= 1
					series -= seg

					if zeroCounter {
						// this means the last bit is 0 again, so we shift by one.
						seg <<= 1
						series <<= 1
						outscale--
						zeroCounter = true
					} else if passedFirst {
						break
					} else {
						passedFirst = true
						outscale--
					}
				} else if zeroCounter {
					zeroCounter = false
				}
			}

			seg <<= 1
			series <<= 1

			series &^= 1 << ({{.maxshift}} - {{.es}})

			endreg := int16(outscale>>{{.es}})
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > {{.bits}}-2 {
					x := -1
					a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(x))
					return a
				}
				outPosit = (0b1 << {{.maxshift}}) >> (outn & {{template "shiftmask" .rbits}})
			} else {
				outn = 1
				outPosit = {{.maxmask}} - ({{.maxmask}} >> outn)
			}
		} else {
			// pre flipping the first bit, trick to be able to more the second loop after the first
			series = 1 << ({{.maxshift}} - {{.es}})
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n%2) << ({{.maxshift}}-1-{{.es}}))
				n >>= 1
				seg >>= 1
			}
			if afracP1 == (1 << 30) {
				outscale++
			}

			endreg := int16(outscale>>{{.es}})
			outn = uint8(1 + endreg)
			if outn > {{.bits}}-2 {
				x := {{.maxmask}} >> 1
				a.num = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(x))
				return a
			}
			outPosit = {{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}}))
		}
		series >>= outn - 1
		seg >>= outn 
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series -= seg
			}
			seg >>= 1
		}
		series >>= 1
	} else {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			for afracP1&(1<<31) == 0 && afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				outscale--
			}
			if afracP1 == 1<<30 {
				a.num = 0
				return a
			}
			n = 1

			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}

			endreg := int16(outscale>>{{.es}})
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > {{.bits}}-2 {
					a.num = 1
					return a
				}
				outPosit = (0b1 << {{.maxshift}}) >> (outn & {{template "shiftmask" .rbits}})
			} else {
				outn = 1
				outPosit = {{.maxmask}} - ({{.maxmask}} >> outn)
			}
		} else {
			// Here we first shift the series and seg to the right to limmit the amount of calculations done.
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n%2) << ({{.maxshift}}-1-{{.es}}))
				seg >>= 1
				n >>= 1
			}

			endreg := int16(outscale>>{{.es}})
			outn = uint8(1 + endreg)
			if outn > {{.bits}}-2 {
				a.num = {{.maxmask}} >> 1
				return a
			}
			outPosit = {{.maxmask}} - ({{.maxmask}} >> (outn & {{template "shiftmask" .rbits}}))		
		}
		series >>= outn
		seg >>= outn + 1
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}
			seg >>= 1
		}
	}

	f := {{template "numtyp" .rbits}}({{.maxmask}})
	endexp := {{template "numtyp" .rbits}}(outscale) &^ (f << {{.es}})

	outfrac := series & (({{.maxmask}} >> (1 + {{.es}})) >> (outn & {{template "shiftmask" .rbits}}))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft{{.rbits}}(endexp, {{.maxshift}}-{{.es}}-int(outn)) & ({{.maxmask}} >> {{.es}})

	if afracP1 != (1<<30) || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if ascale < 0 {
		outPosit = {{template "numtyp" .rbits}}(-{{template "snumtyp" .rbits}}(outPosit))
	}

	return {{template "typename" .}}{num: outPosit}
}
`

const LogeTemplate = `
func (a {{template "typename" .}}) Loge() {{template "typename" .}} {
	log2 := a.Log2()
	// Constant is log2(e)
	return log2.Div({{template "typename" .}}{num: {{.log2_e}}})
}
`

const LogbTemplate = `
func (a {{template "typename" .}}) Logb(b {{template "typename" .}}) {{template "typename" .}} {
	log2 := a.Log2()
	return log2.Div(b.Log2())
}
`

const PowTemplate = `
func (b {{template "typename" .}}) Pow(p {{template "typename" .}}) {{template "typename" .}} {
	ln := b.Loge()
	e := ln.Mul(p)
	return e.Exp()
}
`

func executeTemplate(bits, es uint8, pack, templs string, extras map[string]interface{}, out io.Writer) {
	vars := map[string]interface{}{
		"package":        pack,
		"bits":           bits,
		"rbits":          getrBits(bits),
		"drbits":         getdrBits(bits),
		"es":             es,
		"maxes":          bits - 2,
		"signmask":       getSignMask(bits),
		"firstReg":       getFirstReg(bits),
		"secondreg":      getSecondReg(bits),
		"maxshift":       bits - 1,
		"dmaxshift":      bits*2 - 1,
		"maxmask":        maxmask(bits),
		"log2_e":         getLog2_e(bits),
		"expminoverflow": getExpMinOverflow(bits),
		"expmaxone":      getExpMaxOne(bits),
	}
	for k, v := range extras {
		vars[k] = v
	}
	funs := template.FuncMap{}
	templ := template.New("main")
	templ.Funcs(funs)
	_, err := templ.Parse(templs)
	if err != nil {
		panic(err)
	}
	getDefaultTemplate(templ, vars)
	err = templ.ExecuteTemplate(out, "main", vars)
	if err != nil {
		panic(err)
	}
}

func getDefaultTemplate(templ *template.Template, vars interface{}) {
	const typeTemplate = `Posit{{.bits}}es{{.es}}`
	templ2 := templ.New("typename")
	templ2.Parse(typeTemplate)
	const signedBitsTemplate = `int{{.}}`
	templ2 = templ.New("snumtyp")
	templ2.Parse(signedBitsTemplate)
	const bitsTypeTemplate = `u{{template "snumtyp" .}}`
	templ2 = templ.New("numtyp")
	templ2.Parse(bitsTypeTemplate)
	const dubleBitsMask = `{{with $bits := .}}{{if le $bits 8}}0xf{{else if le $bits 16}}0x1f{{else if le $bits 32}}0x3f{{end}}{{end}}`
	templ2 = templ.New("dshiftmask")
	templ2.Parse(dubleBitsMask)
	const bitsMask = `{{with $bits := .}}{{if le $bits 8}}0x7{{else if le $bits 16}}0x0f{{else if le $bits 32}}0x1f{{else if le $bits 64}}0x3f{{end}}{{end}}`
	templ2 = templ.New("shiftmask")
	templ2.Parse(bitsMask)
	const numbits = `{{with $bits := .}}{{if le $bits 8}}3{{else if le $bits 16}}4{{else if le $bits 32}}5{{else if le $bits 64}}5{{end}}{{end}}`
	templ2 = templ.New("numbits")
	templ2.Parse(numbits)
	// Make this a function? so that we can truely have non-rounded n bits
	const nbitmask = `{{with $bits := .}}{{if le $bits 8}}0x7f{{else if le $bits 16}}0x7fff{{else if le $bits 32}}0x7fffffff{{else if le $bits 64}}0x7fffffffffffffff{{end}}{{end}}`
	templ2 = templ.New("nbitmask")
	templ2.Parse(nbitmask)

}

func getSignMask(bits uint8) string {
	return fmt.Sprintf("1<<(%d-1)", bits)
}

func getFirstReg(bits uint8) string {
	return fmt.Sprintf("1<<(%d-2)", bits)
}
func getSecondReg(bits uint8) string {
	return fmt.Sprintf("1<<(%d-3)", bits)
}

func maxmask(bits uint8) string {
	size := "0x"
	switch {
	case bits <= 8:
		size += "ff"
	case bits <= 16:
		size += "ffff"
	case bits <= 32:
		size += "ffffffff"
	default:
		panic("bit size is not suported")
	}
	return size
}

func getrBits(bits uint8) uint8 {
	switch {
	case bits <= 8:
		return 8
	case bits <= 16:
		return 16
	case bits <= 32:
		return 32
	default:
		panic("bit size is not suported")
	}
}

func getdrBits(bits uint8) uint8 {
	switch {
	case bits <= 8:
		return 16
	case bits <= 16:
		return 32
	case bits <= 32:
		return 64
	default:
		panic("bit size is not suported")
	}
}

//TODO: These should be generated based on the ES as well!!!
func getLog2_e(bits uint8) uint64 {
	switch {
	case bits <= 8:
		return 0b01001110
	case bits <= 16:
		return 0b0100011100010101
	case bits <= 32:
		return 0b01000011100010101010001110110011
	default:
		panic("bit size is not suported")
	}
}

//TODO: These should be generated based on the ES as well!!!
func getExpMaxOne(bits uint8) uint64 {
	switch {
	case bits <= 8:
		return 0b00000000
	case bits <= 16:
		return 0b0000000010111111
	case bits <= 32:
		return 0b00000000100000000000000000000000
	default:
		panic("bit size is not suported")
	}
}

//TODO: These should be generated based on the ES as well!!!
func getExpMinOverflow(bits uint8) uint64 {
	switch {
	case bits <= 8:
		return 0b01101111
	case bits <= 16:
		return 0b0111000010110000
	case bits <= 32:
		return 0b01101001001000000000000000000000
	default:
		panic("bit size is not suported")
	}
}
