//go:build cgo_bench
// +build cgo_bench

package posit

import (
	"testing"
)

func TestAddFuz(t *testing.T) {
	t.Parallel()
	testAddFuz(t)
}

func TestMulFuz(t *testing.T) {
	t.Parallel()
	testMulFuz(t)
}
func TestDivFuz(t *testing.T) {
	t.Parallel()
	testDivFuz(t)
}
func TestSqrtFuz(t *testing.T) {
	t.Parallel()
	testSqrtFuz(t)
}

// func TestTruncFuz(t *testing.T) {
// 	t.Parallel()
// 	testTruncFuz(t)
// }
func TestRoundFuz(t *testing.T) {
	t.Parallel()
	testRoundFuz(t)
}

func TestFromIntFuz(t *testing.T) {
	t.Parallel()
	testFromIntFuz(t)
}

func TestToIntFuz(t *testing.T) {
	t.Parallel()
	testToIntFuz(t)
}

func BenchmarkCAdd32ES2(b *testing.B) {
	benchmarkCAdd32ES2(b)
}
func BenchmarkCMul32ES2(b *testing.B) {
	benchmarkCMul32ES2(b)
}
func BenchmarkCDiv32ES2(b *testing.B) {
	benchmarkCDiv32ES2(b)
}
func BenchmarkCSqrt32ES2(b *testing.B) {
	benchmarkCSqrt32ES2(b)
}
func BenchmarkCTrunc32ES2(b *testing.B) {
	benchmarkCTrunc32ES2(b)
}

// From int benchmarks
func BenchmarkCFomInt6432ES2(b *testing.B) {
	benchmarkCFomInt6432ES2(b)
}

func BenchmarkCFomInt3232ES2(b *testing.B) {
	benchmarkCFomInt3232ES2(b)
}

func BenchmarkCFomUint6432ES2(b *testing.B) {
	benchmarkCFomUint6432ES2(b)
}

func BenchmarkCFomUint3232ES2(b *testing.B) {
	benchmarkCFomUint3232ES2(b)
}

func BenchmarkCFomInt6416ES1(b *testing.B) {
	benchmarkCFomInt6416ES1(b)
}

func BenchmarkCFomInt3216ES1(b *testing.B) {
	benchmarkCFomInt3216ES1(b)
}

func BenchmarkCFomUint6416ES1(b *testing.B) {
	benchmarkCFomUint6416ES1(b)
}

func BenchmarkCFomUint3216ES1(b *testing.B) {
	benchmarkCFomUint3216ES1(b)
}
