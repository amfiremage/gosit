//go:build cgo_bench
// +build cgo_bench

package posit

//#cgo CFLAGS: -I./softposit-c -O3
//#cgo LDFLAGS: -L. -l:positc.a
//#include "./softposit-c/source/include/softposit.h"
//#include <stdint.h>
//// acases must be the same length as bcases
//void bench_add_32es2(posit32_t acases[], posit32_t bcases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_add(acases[i%len], bcases[i%len]);
//	}
//}
//// acases must be the same length as bcases
//void bench_mul_32es2(posit32_t acases[], posit32_t bcases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_mul(acases[i%len], bcases[i%len]);
//	}
//}
//// acases must be the same length as bcases
//void bench_div_32es2(posit32_t acases[], posit32_t bcases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_div(acases[i%len], bcases[i%len]);
//	}
//}
//void bench_sqrt_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_sqrt(acases[i%len]);
//	}
//}
//void bench_round_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_roundToInt(acases[i%len]);
//	}
//}
//void bench_from_int64_32es2(int64_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		i64_to_p32(acases[i%len]);
//	}
//}
//void bench_from_int32_32es2(int32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		i32_to_p32(acases[i%len]);
//	}
//}
//void bench_from_uint64_32es2(uint64_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		ui64_to_p32(acases[i%len]);
//	}
//}
//void bench_from_uint32_32es2(uint32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		ui32_to_p32(acases[i%len]);
//	}
//}
//void bench_from_int64_16es1(int64_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		i64_to_p16(acases[i%len]);
//	}
//}
//void bench_from_int32_16es1(int32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		i32_to_p16(acases[i%len]);
//	}
//}
//void bench_from_uint64_16es1(uint64_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		ui64_to_p16(acases[i%len]);
//	}
//}
//void bench_from_uint32_16es1(uint32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		ui32_to_p16(acases[i%len]);
//	}
//}
//void bench_to_int64_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_to_i64(acases[i%len]);
//	}
//}
//void bench_to_uint64_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_to_ui64(acases[i%len]);
//	}
//}
//void bench_to_int32_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_to_i32(acases[i%len]);
//	}
//}
//void bench_to_uint32_32es2(posit32_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p32_to_ui32(acases[i%len]);
//	}
//}
//void bench_to_int64_16es1(posit16_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p16_to_i64(acases[i%len]);
//	}
//}
//void bench_to_uint64_16es1(posit16_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p16_to_ui64(acases[i%len]);
//	}
//}
//void bench_to_int32_16es1(posit16_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p16_to_i32(acases[i%len]);
//	}
//}
//void bench_to_uint32_16es1(posit16_t acases[], uint64_t len, uint64_t n){
//	for(uint64_t i = 0; i < n; i++){
//		p16_to_ui32(acases[i%len]);
//	}
//}
import "C"
import (
	"fmt"
	fuzz "github.com/google/gofuzz"
	"testing"
)

const datasize = 100
const locfreq = 1000000000

func newC32(in uint32) C.posit32_t {
	return C.posit32_t{C.uint(in)}
}

func newC16(in uint16) C.posit16_t {
	return C.posit16_t{C.ushort(in)}
}

func testAddFuz(t *testing.T) {
	src := fuzz.New()
	var a32 uint32
	var b32 uint32
	var a16 uint16
	var b16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("add:%d\n", i)
		}
		src.Fuzz(&a32)
		src.Fuzz(&b32)

		exp := Posit{
			num: uint32(C.p32_add(newC32(a32), newC32(b32)).v),
			es:  2,
		}
		pa := Posit{
			num: a32,
			es:  2,
		}
		pb := Posit{
			num: b32,
			es:  2,
		}
		res := pa.AddSameES(pb)
		if res != exp {
			t.Logf("a:%#032b", a32)
			t.Logf("b:%#032b", b32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		a_32 := NewPosit32es2FromBits(a32)
		b_32 := NewPosit32es2FromBits(b32)
		exp2_32 := NewPosit32es2FromBits(exp.num)
		res2_32 := a_32.Add(b_32)
		if exp2_32 != exp2_32 {
			t.Logf("a:%#032b", a_32)
			t.Logf("b:%#032b", b_32)
			t.Logf("exp:%#032b", exp2_32.num)
			t.Logf("resf:%#032b", res2_32.num)
			t.Fatal("got the wrong number:", res2_32.Float(), "expected:", exp2_32.Float())
		}
		a16 = uint16(a32)
		b16 = uint16(b32)
		exp16 := uint16(C.p16_add(newC16(a16), newC16(b16)).v)
		a_16 := NewPosit16es1FromBits(a16)
		b_16 := NewPosit16es1FromBits(b16)
		exp2_16 := NewPosit16es1FromBits(exp16)
		res2_16 := a_16.Add(b_16)
		if res2_16 != exp2_16 {
			t.Logf("a:%#016b", a_16)
			t.Logf("b:%#016b", b_16)
			t.Logf("exp:%#016b", exp2_16.num)
			t.Logf("resf:%#016b", res2_16.num)
			t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		}
	}
}

func testMulFuz(t *testing.T) {
	src := fuzz.New()
	var a32 uint32
	var b32 uint32
	var a16 uint16
	var b16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("mul:%d\n", i)
		}
		src.Fuzz(&a32)
		src.Fuzz(&b32)
		exp := Posit{
			num: uint32(C.p32_mul(newC32(a32), newC32(b32)).v),
			es:  2,
		}
		pa := Posit{
			num: a32,
			es:  2,
		}
		pb := Posit{
			num: b32,
			es:  2,
		}
		res := pa.MulSameES(pb)
		if res != exp {
			t.Logf("a:%#032b", a32)
			t.Logf("b:%#032b", b32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		a_32 := NewPosit32es2FromBits(a32)
		b_32 := NewPosit32es2FromBits(b32)
		exp2_32 := NewPosit32es2FromBits(exp.num)
		res2_32 := a_32.Mul(b_32)
		if exp2_32 != exp2_32 {
			t.Logf("a:%#032b", a_32)
			t.Logf("b:%#032b", b_32)
			t.Logf("exp:%#032b", exp2_32.num)
			t.Logf("resf:%#032b", res2_32.num)
			t.Fatal("got the wrong number:", res2_32.Float(), "expected:", exp2_32.Float())
		}
		a16 = uint16(a32)
		b16 = uint16(b32)
		exp16 := uint16(C.p16_mul(newC16(a16), newC16(b16)).v)
		a_16 := NewPosit16es1FromBits(a16)
		b_16 := NewPosit16es1FromBits(b16)
		exp2_16 := NewPosit16es1FromBits(exp16)
		res2_16 := a_16.Mul(b_16)
		if res2_16 != exp2_16 {
			t.Logf("a:%#016b", a_16)
			t.Logf("b:%#016b", b_16)
			t.Logf("exp:%#016b", exp2_16.num)
			t.Logf("resf:%#016b", res2_16.num)
			t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		}
	}
}

func testDivFuz(t *testing.T) {
	src := fuzz.New()

	var a32 uint32
	var b32 uint32
	var a16 uint16
	var b16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("div:%d\n", i)
		}
		src.Fuzz(&a32)
		src.Fuzz(&b32)
		exp := Posit{
			num: uint32(C.p32_div(newC32(a32), newC32(b32)).v),
			es:  2,
		}
		pa := Posit{
			num: a32,
			es:  2,
		}
		pb := Posit{
			num: b32,
			es:  2,
		}
		res := pa.DivSameES(pb)
		if res != exp {
			t.Logf("a:%#032b", a32)
			t.Logf("b:%#032b", b32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		a_32 := NewPosit32es2FromBits(a32)
		b_32 := NewPosit32es2FromBits(b32)
		exp2 := NewPosit32es2FromBits(exp.num)
		res2 := a_32.Div(b_32)
		if res2 != exp2 {
			t.Logf("a:%#032b", a_32)
			t.Logf("b:%#032b", b_32)
			t.Logf("exp:%#032b", exp2.num)
			t.Logf("resf:%#032b", res2.num)
			t.Fatal("got the wrong number:", res2.Float(), "expected:", exp2.Float())
		}
		a16 = uint16(a32)
		b16 = uint16(b32)
		exp16 := uint16(C.p16_div(newC16(a16), newC16(b16)).v)
		a_16 := NewPosit16es1FromBits(a16)
		b_16 := NewPosit16es1FromBits(b16)
		exp2_16 := NewPosit16es1FromBits(exp16)
		res2_16 := a_16.Div(b_16)
		if res2_16 != exp2_16 {
			t.Logf("a:%#016b", a_16)
			t.Logf("b:%#016b", b_16)
			t.Logf("exp:%#016b", exp2_16.num)
			t.Logf("resf:%#016b", res2_16.num)
			t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		}
	}
}

func testSqrtFuz(t *testing.T) {
	src := fuzz.New()
	var a32 uint32
	var a16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("sqrt:%d\n", i)
		}
		src.Fuzz(&a32)
		exp := Posit{
			num: uint32(C.p32_sqrt(newC32(a32)).v),
			es:  2,
		}
		pa := Posit{
			num: a32,
			es:  2,
		}
		res := pa.SqrtSameES()
		if res != exp && (a32&(1<<31)) == 0 {
			t.Logf("a:%#032b", a32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		a_32 := NewPosit32es2FromBits(a32)
		exp2 := NewPosit32es2FromBits(exp.num)
		res2 := a_32.Sqrt()
		if res2 != exp2 {
			t.Logf("a:%#032b", a_32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("resf:%#032b", res2.num)
			t.Fatal("got the wrong number:", res2.Float(), "expected:", exp2.Float())
		}
		a16 = uint16(a32)
		exp16 := uint16(C.p16_sqrt(newC16(a16)).v)
		a_16 := NewPosit16es1FromBits(a16)
		exp2_16 := NewPosit16es1FromBits(exp16)
		res2_16 := a_16.Sqrt()
		if res2_16 != exp2_16 {
			t.Logf("a:%#016b", a_16)
			t.Logf("exp:%#016b", exp2_16.num)
			t.Logf("resf:%#016b", res2_16.num)
			t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		}
	}
}

// func testTruncFuz(t *testing.T) {
// data := make([]byte, datasize)
// var a32 uint32
// var a16 uint16
// for i := 0; true; i++ {
// 	if i%locfreq == 0 {
// 		fmt.Printf("trunc:%d\n", i)
// 	}
// 	rand.Read(data)
// 	fuzz.NewFromGoFuzz(data).Fuzz(&a32)
// 	exp := Posit{
// 		num: uint32(C.posittrunc32(C.uint(a32))),
// 		es:  2,
// 	}
// 	pa := Posit{
// 		num: a32,
// 		es:  2,
// 	}
// 	if a32&(1<<31) == 0 && a32 < 0b01000000000000000000000000000000 {
// 		exp.num = 0
// 	}
// 	if a32&(1<<31) != 0 && a32 == 0b10110100000000000000000000000000 {
// 		exp.num = 0b10110100000000000000000000000000
// 	} else if a32&(1<<31) == 0 && a32 == 0b01001100000000000000000000000000 {
// 		exp.num = 0b01001100000000000000000000000000
// 	}
// 	res := TruncateSameES(pa)
// 	if res != exp {
// 		t.Logf("a:%#032b", a32)
// 		t.Logf("exp:%#032b", exp.num)
// 		t.Logf("got:%#032b", res.num)
// 		t.Fatal("got the wrong number:", GetfloatFromPosit32(res), "expected:", GetfloatFromPosit32(exp))
// 	}
// 	a_32 := NewPosit32es2FromBits(a32)
// 	exp2 := NewPosit32es2FromBits(exp.num)
// 	res2 := a_32.Truncate()
// 	if res2 != exp2 {
// 		t.Logf("a:%#032b", a_32)
// 		t.Logf("exp:%#032b", exp.num)
// 		t.Logf("resf:%#032b", res2.num)
// 		t.Fatal("got the wrong number:", res2.Float(), "expected:", exp2.Float())
// 	}
// 	a16 = uint16(a32)
// 	exp16 := uint16(C.posittrunc16(C.ushort(a16)))
// 	a_16 := NewPosit16es1FromBits(a16)
// 	exp2_16 := NewPosit16es1FromBits(exp16)

// 	if a16&(1<<15) == 0 && a16 < 0b0100000000000000 {
// 		exp.num = 0
// 	}
// 	if a32&(1<<31) != 0 && a32 == 0b1011010000000000 {
// 		exp.num = 0b1011010000000000
// 	} else if a32&(1<<31) == 0 && a32 == 0b0100110000000000 {
// 		exp.num = 0b0100110000000000
// 	}

// 	res2_16 := a_16.Truncate()
// 	if res2_16 != exp2_16 {
// 		t.Logf("a:%#016b", a_16)
// 		t.Logf("exp:%#016b", exp2_16.num)
// 		t.Logf("resf:%#016b", res2_16.num)
// 		t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
// 	}
// }
// }

func testRoundFuz(t *testing.T) {
	src := fuzz.New()
	var a32 uint32
	var a16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("round:%d\n", i)
		}
		src.Fuzz(&a32)
		exp := Posit{
			num: uint32(C.p32_roundToInt(newC32(a32)).v),
			es:  2,
		}
		pa := Posit{
			num: a32,
			es:  2,
		}
		res := pa.RoundSameES()
		if res != exp {
			t.Logf("a:%#032b", a32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		a_32 := NewPosit32es2FromBits(a32)
		exp2 := NewPosit32es2FromBits(exp.num)
		res2 := a_32.Round()
		if res2 != exp2 {
			t.Logf("a:%#032b", a_32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("resf:%#032b", res2.num)
			t.Fatal("got the wrong number:", res2.Float(), "expected:", exp2.Float())
		}
		a16 = uint16(a32)
		exp16 := uint16(C.p16_roundToInt(newC16(a16)).v)
		a_16 := NewPosit16es1FromBits(a16)
		exp2_16 := NewPosit16es1FromBits(exp16)
		res2_16 := a_16.Round()
		if res2_16 != exp2_16 {
			t.Logf("a:%#016b", a_16)
			t.Logf("exp:%#016b", exp2_16.num)
			t.Logf("resf:%#016b", res2_16.num)
			t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		}
	}
}

func testFromIntFuz(t *testing.T) {
	src := fuzz.New()
	var i64 int64
	var u64 uint64
	var i32 int32
	var u32 uint32
	// var i16 int16
	// var u16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("fromInt:%d\n", i)
		}
		src.Fuzz(&i64)
		u64 = uint64(i64)
		i32 = int32(i64)
		u32 = uint32(i64)
		// i16 = int16(i64)
		// u16 = uint16(i64)

		// Posit32

		// 64 bits input
		exp := Posit{
			num: uint32(C.i64_to_p32(C.long(i64)).v),
			es:  2,
		}
		res := NewPosit32FromInt64(i64, 2)
		if res != exp {
			t.Logf("i:%#064b", i64)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		exp = Posit{
			num: uint32(C.ui64_to_p32(C.ulong(u64)).v),
			es:  2,
		}
		res = NewPosit32FromUint64(u64, 2)
		if res != exp {
			t.Logf("u:%#064b", u64)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		// 32 bits input
		exp = Posit{
			num: uint32(C.i32_to_p32(C.int(i32)).v),
			es:  2,
		}
		res = NewPosit32FromInt32(i32, 2)
		if res != exp {
			t.Logf("i:%#032b", i32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		exp = Posit{
			num: uint32(C.ui32_to_p32(C.uint(u32)).v),
			es:  2,
		}
		res = NewPosit32FromUint32(u32, 2)
		if res != exp {
			t.Logf("u:%#032b", u32)
			t.Logf("exp:%#032b", exp.num)
			t.Logf("got:%#032b", res.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}
		// Posit32ES2

		// 64 bits input
		exp32_2 := Posit32es2{
			num: uint32(C.i64_to_p32(C.long(i64)).v),
		}
		res32_2 := NewPosit32ES2FromInt64(i64)
		if res32_2 != exp32_2 {
			t.Logf("i32:%#064b", i64)
			t.Logf("exp:%#032b", exp32_2.num)
			t.Logf("got:%#032b", res32_2.num)
			t.Fatal("got the wrong number:", res32_2.Float(), "expected:", exp32_2.Float())
		}
		exp32_2 = Posit32es2{
			num: uint32(C.ui64_to_p32(C.ulong(u64)).v),
		}
		res32_2 = NewPosit32ES2FromUint64(u64)
		if res32_2 != exp32_2 {
			t.Logf("u32:%#064b", u64)
			t.Logf("exp:%#032b", exp32_2.num)
			t.Logf("got:%#032b", res32_2.num)
			t.Fatal("got the wrong number:", res32_2.Float(), "expected:", exp32_2.Float())
		}

		// 32 bits input
		exp32_2 = Posit32es2{
			num: uint32(C.i32_to_p32(C.int(i32)).v),
		}
		res32_2 = NewPosit32ES2FromInt32(i32)
		if res32_2 != exp32_2 {
			t.Logf("i32:%#032b", i32)
			t.Logf("exp:%#032b", exp32_2.num)
			t.Logf("got:%#032b", res32_2.num)
			t.Fatal("got the wrong number:", res32_2.Float(), "expected:", exp32_2.Float())
		}
		exp32_2 = Posit32es2{
			num: uint32(C.ui32_to_p32(C.uint(u32)).v),
		}
		res32_2 = NewPosit32ES2FromUint32(u32)
		if res32_2 != exp32_2 {
			t.Logf("u32:%#032b", u32)
			t.Logf("exp:%#032b", exp32_2.num)
			t.Logf("got:%#032b", res32_2.num)
			t.Fatal("got the wrong number:", res.Float(), "expected:", exp.Float())
		}

		// Posit16ES1

		// 64 bits input
		exp16_1 := Posit16es1{
			num: uint16(C.i64_to_p16(C.long(i64)).v),
		}
		res16_1 := NewPosit16ES1FromInt64(i64)
		if res16_1 != exp16_1 {
			t.Logf("i32:%#064b", i64)
			t.Logf("exp:%#032b", exp16_1.num)
			t.Logf("got:%#032b", res16_1.num)
			t.Fatal("got the wrong number:", res16_1.Float(), "expected:", exp16_1.Float())
		}
		exp16_1 = Posit16es1{
			num: uint16(C.ui64_to_p16(C.ulong(u64)).v),
		}
		res16_1 = NewPosit16ES1FromUint64(u64)
		if res16_1 != exp16_1 {
			t.Logf("u32:%#064b", u64)
			t.Logf("exp:%#016b", exp16_1.num)
			t.Logf("got:%#016b", res16_1.num)
			t.Fatal("got the wrong number:", res16_1.Float(), "expected:", exp16_1.Float())
		}

		// 32 bits input
		exp16_1 = Posit16es1{
			num: uint16(C.i32_to_p16(C.int(i32)).v),
		}
		res16_1 = NewPosit16ES1FromInt32(i32)
		if res16_1 != exp16_1 {
			t.Logf("i32:%#032b", i32)
			t.Logf("exp:%#016b", exp16_1.num)
			t.Logf("got:%#016b", res16_1.num)
			t.Fatal("got the wrong number:", res16_1.Float(), "expected:", exp16_1.Float())
		}
		exp16_1 = Posit16es1{
			num: uint16(C.ui32_to_p16(C.uint(u32)).v),
		}
		res16_1 = NewPosit16ES1FromUint32(u32)
		if res16_1 != exp16_1 {
			t.Logf("u32:%#032b", u32)
			t.Logf("exp:%#016b", exp16_1.num)
			t.Logf("got:%#016b", res16_1.num)
			t.Fatal("got the wrong number:", res16_1.Float(), "expected:", exp16_1.Float())
		}
	}
}

func testToIntFuz(t *testing.T) {
	src := fuzz.New()
	var a32 uint32
	// var a16 uint16
	for i := 0; true; i++ {
		if i%locfreq == 0 {
			fmt.Printf("toInt:%d\n", i)
		}
		src.Fuzz(&a32)
		// 64 bit output
		exp := uint64(C.p32_to_i64(newC32(a32)))
		pa := Posit{
			num: a32,
			es:  2,
		}
		res := uint64(pa.Int64())
		if res != exp {
			t.Logf("ai_64:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#064b", exp)
			t.Logf("got:%#064b", res)
			t.Fatal("got the wrong number:", int64(res), "expected:", int64(exp))
		}
		pa_32 := Posit32es2{
			num: a32,
		}
		res = uint64(pa_32.Int64())
		if res != exp {
			t.Logf("ai2_64:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#064b", exp)
			t.Logf("got:%#064b", res)
			t.Fatal("got the wrong number:", int64(res), "expected:", int64(exp))
		}
		exp = uint64(C.p32_to_ui64(newC32(a32)))
		res = pa.Uint64()
		if res != exp {
			t.Logf("a_64:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#064b", exp)
			t.Logf("got:%#064b", res)
			t.Fatal("got the wrong number:", res, "expected:", exp)
		}
		res = pa_32.Uint64()
		if res != exp {
			t.Logf("a2_64:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#064b", exp)
			t.Logf("got:%#064b", res)
			t.Fatal("got the wrong number:", int64(res), "expected:", int64(exp))
		}

		// 32 bit output
		exp_32 := uint32(C.p32_to_i32(newC32(a32)))
		res_32 := uint32(pa.Int32())
		if res_32 != exp_32 {
			t.Logf("ai_32:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#032b", exp_32)
			t.Logf("got:%#032b", res_32)
			t.Fatal("got the wrong number:", int32(res_32), "expected:", int32(exp_32))
		}
		exp_32 = uint32(C.p32_to_ui32(newC32(a32)))
		res_32 = pa.Uint32()
		if res_32 != exp_32 {
			t.Logf("a_32:%#032b", a32)
			t.Logf("pa:%v", pa)
			t.Logf("exp:%#032b", exp_32)
			t.Logf("got:%#032b", res_32)
			t.Fatal("got the wrong number:", res_32, "expected:", exp_32)
		}
		// a16 = uint16(a32)
		// exp16 := uint16(C.p16_roundToInt(newC16(a16)).v)
		// a_16 := NewPosit16es1FromBits(a16)
		// exp2_16 := NewPosit16es1FromBits(exp16)
		// res2_16 := a_16.Round()
		// if res2_16 != exp2_16 {
		// 	t.Logf("a:%#016b", a_16)
		// 	t.Logf("exp:%#016b", exp2_16.num)
		// 	t.Logf("resf:%#016b", res2_16.num)
		// 	t.Fatal("got the wrong number:", res2_16.Float(), "expected:", exp2_16.Float())
		// }
	}
}

func benchCaseTo32C(benches []opBenchCase) ([]C.posit32_t, []C.posit32_t) {
	outa := make([]C.posit32_t, 0, len(benches))
	outb := make([]C.posit32_t, 0, len(benches))
	for _, bench := range benches {
		outa = append(outa, newC32(bench.ac32.num))
		outb = append(outa, newC32(bench.bc32.num))
	}
	return outa, outb
}

func benchCaseToUint64(benches []opBenchCase) []uint64 {
	outa := make([]uint64, 0, len(benches))
	for _, bench := range benches {
		outa = append(outa, uint64(bench.ac32.num)<<32+uint64(bench.bc32.num))
	}
	return outa
}
func benchCaseToInt64(benches []opBenchCase) []int64 {
	outa := make([]int64, 0, len(benches))
	for _, bench := range benches {
		outa = append(outa, int64(uint64(bench.ac32.num)<<32+uint64(bench.bc32.num)))
	}
	return outa
}
func benchCaseToUint32(benches []opBenchCase) []uint32 {
	outa := make([]uint32, 0, len(benches))
	for _, bench := range benches {
		outa = append(outa, bench.bc32.num)
	}
	return outa
}
func benchCaseToInt32(benches []opBenchCase) []int32 {
	outa := make([]int32, 0, len(benches))
	for _, bench := range benches {
		outa = append(outa, int32(bench.bc32.num))
	}
	return outa
}

func benchmarkCAdd32ES2(b *testing.B) {
	bencha, benchb := benchCaseTo32C(slowBenchcases[:])
	b.ResetTimer()
	C.bench_add_32es2((*C.posit32_t)(&bencha[0]), (*C.posit32_t)(&benchb[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCMul32ES2(b *testing.B) {
	bencha, benchb := benchCaseTo32C(slowBenchcases[:])
	b.ResetTimer()
	C.bench_mul_32es2((*C.posit32_t)(&bencha[0]), (*C.posit32_t)(&benchb[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCDiv32ES2(b *testing.B) {
	bencha, benchb := benchCaseTo32C(slowBenchcases[:])
	b.ResetTimer()
	C.bench_div_32es2((*C.posit32_t)(&bencha[0]), (*C.posit32_t)(&benchb[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCSqrt32ES2(b *testing.B) {
	bencha, _ := benchCaseTo32C(slowBenchcases[:])
	b.ResetTimer()
	C.bench_sqrt_32es2((*C.posit32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCTrunc32ES2(b *testing.B) {
	bencha, _ := benchCaseTo32C(slowBenchcases[:])
	b.ResetTimer()
	C.bench_round_32es2((*C.posit32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

// From int benchmarks
func benchmarkCFomInt6432ES2(b *testing.B) {
	bencha := benchCaseToInt64(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_int64_32es2((*C.int64_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomInt3232ES2(b *testing.B) {
	bencha := benchCaseToInt32(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_int32_32es2((*C.int32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomUint6432ES2(b *testing.B) {
	bencha := benchCaseToUint64(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_uint64_32es2((*C.uint64_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomUint3232ES2(b *testing.B) {
	bencha := benchCaseToUint32(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_uint32_32es2((*C.uint32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomInt6416ES1(b *testing.B) {
	bencha := benchCaseToInt64(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_int64_16es1((*C.int64_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomInt3216ES1(b *testing.B) {
	bencha := benchCaseToInt32(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_int32_16es1((*C.int32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomUint6416ES1(b *testing.B) {
	bencha := benchCaseToUint64(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_uint64_16es1((*C.uint64_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}

func benchmarkCFomUint3216ES1(b *testing.B) {
	bencha := benchCaseToUint32(slowBenchcases[:])
	b.ResetTimer()
	C.bench_from_uint32_16es1((*C.uint32_t)(&bencha[0]), C.uint64_t(len(slowBenchcases)), C.uint64_t(b.N))
}
