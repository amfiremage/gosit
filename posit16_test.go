package posit

import (
	"math"
	"testing"
)

type floatConv16Test struct {
	f    float64
	p    Posit16es1
	altp Posit16es1
}

var floatconv16tests = map[string]floatConv16Test{
	"1": {
		f:    -2.9482420075,
		p:    NewPosit16es1FromBits(0b1010100001101010),
		altp: NewPosit16es1FromBits(0b1010100001101011),
	},
}

func TestConvFloat16(t *testing.T) {
	for name, test := range floatconv16tests {
		t.Run(name, func(t *testing.T) {
			res := test.p.Float()
			t.Logf("exp:%v", test.f)
			t.Logf("res:%v", res)
			if math.Abs(res-test.f) > 0.000001 {
				t.Error("got the wrong float:", res, "expected:", test.f)
			}
			resp := NewPosit16ES1FromFloat(test.f)
			t.Logf("exp:%#016b", test.p.num)
			t.Logf("res:%#016b", resp.num)
			if resp != test.p && resp != test.altp {
				t.Error("got the wrong posit:", resp, "expected:", test.p, ". Or", resp.Float(), "and", test.f)
			}
		})
	}
}

type stringConv16Test struct {
	s string
	b string
	p Posit16es1
}

var stringconv16tests = map[string]stringConv16Test{
	"1": {
		s: "-1.804443250255",
		b: "-1.804443250255",
		p: NewPosit16es1FromBits(0b1011001100100001),
	},
	"2": {
		s: "-1.6777216*10^7",
		b: "-16777216",
		p: NewPosit16es1FromBits(0b1000000000000100),
	},
	"3": {
		s: "-2.9482420075",
		b: "-2.9482420075",
		p: NewPosit16es1FromBits(0b1010100001101010),
	},
	"4": {
		s: "8.912353025005*10^-1",
		b: "0.8912353025005",
		p: NewPosit16es1FromBits(0b0011110010000101),
	},
	"5": {
		s: "-2.68435456*10^8",
		b: "-268435456",
		p: NewPosit16es1FromBits(0b1000000000000001),
	},
}

func TestConvString16(t *testing.T) {
	for name, test := range stringconv16tests {
		t.Run(name, func(t *testing.T) {
			res := test.p.FormatBasic()
			t.Logf("real:%v", test.p.Float())
			t.Logf("exp:%v", test.b)
			t.Logf("res:%v", res)
			if res != test.b {
				t.Error("got the wrong string:", res, "expected:", test.b)
			}
			res = test.p.FormatScientific()
			t.Logf("exp:%v", test.s)
			t.Logf("res:%v", res)
			if res != test.s {
				t.Error("got the wrong string:", res, "expected:", test.s)
			}
		})
	}
}

type opTest16 struct {
	a   Posit16es1
	b   Posit16es1
	exp Posit16es1
}

var add16tests = map[string]opTest16{
	"1:toadd": {
		a:   NewPosit16es1FromBits(0b1011001100100001),
		b:   NewPosit16es1FromBits(0b0111111111111110),
		exp: NewPosit16es1FromBits(0b0111111111111110),
	},
	"2:toadd1": {
		a:   NewPosit16es1FromBits(0b1010100001101010),
		b:   NewPosit16es1FromBits(0b1000000000000010),
		exp: NewPosit16es1FromBits(0b1000000000000010),
	},
	"3": {
		a:   NewPosit16es1FromBits(0b1000000000000100),
		b:   NewPosit16es1FromBits(0b1010000101000001),
		exp: NewPosit16es1FromBits(0b1000000000000100),
	},
	"4": {
		a:   NewPosit16es1FromBits(0b0011110010000101),
		b:   NewPosit16es1FromBits(0b0111111111111111),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"5:toadd2": {
		a:   NewPosit16es1FromBits(0b1000000000000001),
		b:   NewPosit16es1FromBits(0b0111111110000001),
		exp: NewPosit16es1FromBits(0b1000000000000001),
	},
	"6": {
		a:   NewPosit16es1FromBits(0b1000000000000010),
		b:   NewPosit16es1FromBits(0b1000000000100010),
		exp: NewPosit16es1FromBits(0b1000000000000010),
	},
	"7:todadd3": {
		a:   NewPosit16es1FromBits(0b0010110011101000),
		b:   NewPosit16es1FromBits(0b1110101000001101),
		exp: NewPosit16es1FromBits(0b0010010111101110),
	},
}

func TestAdd16(t *testing.T) {
	for name, test := range add16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Add(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var mul16tests = map[string]opTest16{
	"1:exp": {
		a:   NewPosit16es1FromBits(0b1111000010110011),
		b:   NewPosit16es1FromBits(0b1000000000000001),
		exp: NewPosit16es1FromBits(0b0111111111111100),
	},
	"2:todadd": {
		a:   NewPosit16es1FromBits(0b0011000101001101),
		b:   NewPosit16es1FromBits(0b0000000000000101),
		exp: NewPosit16es1FromBits(0b0000000000000100),
	},
	"3": {
		a:   NewPosit16es1FromBits(0b0011000100101001),
		b:   NewPosit16es1FromBits(0b0000000000000001),
		exp: NewPosit16es1FromBits(0b0000000000000001),
	},
	"4:scaleOverflow": {
		a:   NewPosit16es1FromBits(0b1111111111000001),
		b:   NewPosit16es1FromBits(0b1111111001011011),
		exp: NewPosit16es1FromBits(0b0000000000000010),
	},
	"5": {
		a:   NewPosit16es1FromBits(0b0111111111001001),
		b:   NewPosit16es1FromBits(0b0111111100111110),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"6:nanmul": {
		a:   NewPosit16es1FromBits(0b0000000000000000),
		b:   NewPosit16es1FromBits(0b1000000000000000),
		exp: NewPosit16es1FromBits(0b1000000000000000),
	},
	"7:toadd": {
		a:   NewPosit16es1FromBits(0b0111110101111000),
		b:   NewPosit16es1FromBits(0b0000011000011100),
		exp: NewPosit16es1FromBits(0b0110010001100100),
	},
}

func TestMul16(t *testing.T) {
	for name, test := range mul16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Mul(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var div16tests = map[string]opTest16{
	"1:bitshift": {
		a:   NewPosit16es1FromBits(0b0001110001100010),
		b:   NewPosit16es1FromBits(0b1000100001100111),
		exp: NewPosit16es1FromBits(0b1111110001011111),
	},
	"2:toadd": {
		a:   NewPosit16es1FromBits(0b0111100111000000),
		b:   NewPosit16es1FromBits(0b0000000000001110),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"3:toadd1": {
		a:   NewPosit16es1FromBits(0b0111000110101010),
		b:   NewPosit16es1FromBits(0b1111110011000000),
		exp: NewPosit16es1FromBits(0b1000000010111000),
	},
	"4:toadd2": {
		a:   NewPosit16es1FromBits(0b0111010011000111),
		b:   NewPosit16es1FromBits(0b0000000000000100),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"5:maxoutn": {
		a:   NewPosit16es1FromBits(0b1111111111111111),
		b:   NewPosit16es1FromBits(0b1010011101001101),
		exp: NewPosit16es1FromBits(0b0000000000000001),
	},
	"6:toadd3": {
		a:   NewPosit16es1FromBits(0b0101111100100101),
		b:   NewPosit16es1FromBits(0b0000010110000000),
		exp: NewPosit16es1FromBits(0b0111110100011101),
	},
	"7:toadd4": {
		a:   NewPosit16es1FromBits(0b0101110100101001),
		b:   NewPosit16es1FromBits(0b1111111111111110),
		exp: NewPosit16es1FromBits(0b1000000000000001),
	},
	"8:nandiv": {
		a:   NewPosit16es1FromBits(0b0000000000000000),
		b:   NewPosit16es1FromBits(0b1000000000000000),
		exp: NewPosit16es1FromBits(0b1000000000000000),
	},
}

func TestDiv16(t *testing.T) {
	for name, test := range div16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Div(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var sqrt16tests = map[string]opTest16{
	"1:bitshift": {
		a:   NewPosit16es1FromBits(0b0011100011110000),
		exp: NewPosit16es1FromBits(0b0011110001000000),
	},
}

func TestSqrt16(t *testing.T) {
	for name, test := range sqrt16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Sqrt()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var ceil16tests = map[string]opTest16{
	"1:bitshift": {
		a:   NewPosit16es1FromBits(0b0000110101100111),
		exp: NewPosit16es1FromBits(0b0100000000000000),
	},
	"2:": {
		a:   NewPosit16es1FromBits(0b1111010101001100),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
}

func TestCeil16(t *testing.T) {
	for name, test := range ceil16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Ceil()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var trunc16tests = map[string]opTest16{
	"1:bitshift": {
		a:   NewPosit16es1FromBits(0b0000110101100111),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
	"2:": {
		a:   NewPosit16es1FromBits(0b1111010101001100),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
}

func TestTrunc16(t *testing.T) {
	for name, test := range trunc16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Truncate()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var round16tests = map[string]opTest16{
	"1": {
		a:   NewPosit16es1FromBits(0b1111010101001100),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
	"2:-1.5": {
		a:   NewPosit16es1FromBits(0b1011100000000000),
		exp: NewPosit16es1FromBits(0b1011000000000000),
	},
	"3:140.5": {
		a:   NewPosit16es1FromBits(0b0111101000110010),
		exp: NewPosit16es1FromBits(0b0111101000110000),
	},
	"3:139.5": {
		a:   NewPosit16es1FromBits(0b0111101000101110),
		exp: NewPosit16es1FromBits(0b0111101000110000),
	},
	"4:1.5": {
		a:   NewPosit16es1FromBits(0b0100100000000000),
		exp: NewPosit16es1FromBits(0b0101000000000000),
	},
}

func TestRound16(t *testing.T) {
	for name, test := range round16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Round()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var exp16tests = map[string]opTest16{
	"1": {
		a:   NewPosit16es1FromBits(0b0100000000000000),
		exp: NewPosit16es1FromBits(0b0101010110111111),
	},
	"2": {
		a:   NewPosit16es1FromBits(0b0011000000000000),
		exp: NewPosit16es1FromBits(0b0100101001100001),
	},
	"3": {
		a:   NewPosit16es1FromBits(0b0101000000000000),
		exp: NewPosit16es1FromBits(0b0110011011000111),
	},
	"4": {
		a:   NewPosit16es1FromBits(0b0101001001100110),
		exp: NewPosit16es1FromBits(0b0110100111111001),
	},
	"5:BIG": {
		a:   NewPosit16es1FromBits(0b0111111110010000),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"6": {
		a:   NewPosit16es1FromBits(0b0111010110011000),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"7:veryBig": {
		a:   NewPosit16es1FromBits(0b0111111111110001),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"8:nearZero": {
		a:   NewPosit16es1FromBits(0b1000000000001111),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
	"9:big-1": {
		a:   NewPosit16es1FromBits(0b0111100001001110),
		exp: NewPosit16es1FromBits(0b0111111111111111),
	},
	"10:5": {
		a:   NewPosit16es1FromBits(0b0110001000000000),
		exp: NewPosit16es1FromBits(0b0111101001010010),
	},
	"11:0": {
		a:   NewPosit16es1FromBits(0b0000000000000000),
		exp: NewPosit16es1FromBits(0b0100000000000000),
	},
	"12:small": {
		a:   NewPosit16es1FromBits(0b0010000000000000),
		exp: NewPosit16es1FromBits(0b0100010010001011),
	},
}

func TestExp16(t *testing.T) {
	for name, test := range exp16tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Exp()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

var log216tests = map[string]opTest16{
	"1:1": {
		a:   NewPosit16es1FromBits(0b0100000000000000),
		exp: NewPosit16es1FromBits(0b0000000000000000),
	},
	"2:2": {
		a:   NewPosit16es1FromBits(0b0101000000000000),
		exp: NewPosit16es1FromBits(0b0100000000000000),
	},
	"3:.5": {
		a:   NewPosit16es1FromBits(0b0011000000000000),
		exp: NewPosit16es1FromBits(0b1100000000000000),
	},
	"4:5": {
		a:   NewPosit16es1FromBits(0b0110001000000000),
		exp: NewPosit16es1FromBits(0b0101001010010011),
	},
	"6:.7": {
		a:   NewPosit16es1FromBits(0b0011011001100110),
		exp: NewPosit16es1FromBits(0b1100111110001000),
	},
	"7:.2": {
		a:   NewPosit16es1FromBits(0b0001110011001101),
		exp: NewPosit16es1FromBits(0b1010110101101101),
	},
	"7:.25": {
		a:   NewPosit16es1FromBits(0b0010000000000000),
		exp: NewPosit16es1FromBits(0b1011000000000000),
	},
	"8:0.1": {
		a:   NewPosit16es1FromBits(0b0001010011001101),
		exp: NewPosit16es1FromBits(0b1010010101101101),
	},
	"9:0.01": {
		a:   NewPosit16es1FromBits(0b0000011010001111),
		exp: NewPosit16es1FromBits(0b1001101010110110),
	},
}

func TestLog216(t *testing.T) {
	for name, test := range log216tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Log2()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printOpTest16(test, res, false, t)
		})
	}
}

type componentTest16 struct {
	neg   bool
	scale int
	frac  uint16
	exp   Posit16es1
}

var component16tests = map[string]componentTest16{
	"1": {
		neg:   false,
		scale: -500,
		frac:  0,
		exp: Posit16es1{
			num: 0b0000000000000000,
		},
	},
	"2": {
		neg:   false,
		scale: 1,
		frac:  0,
		exp: Posit16es1{
			num: 0b0101000000000000,
		},
	},
	"3": {
		neg:   false,
		scale: 0,
		frac:  0,
		exp: Posit16es1{
			num: 0b0100000000000000,
		},
	},
	"4": {
		neg:   false,
		scale: 0,
		frac:  0b1010101010010101,
		exp: Posit16es1{
			num: 0b0100101010101001,
		},
	},
}

func TestFromComponents16(t *testing.T) {
	for name, test := range component16tests {
		t.Run(name, func(t *testing.T) {
			res := NewPosit16ES1FromComponents(test.neg, test.scale, test.frac)
			t.Logf("exp:%#016b", test.exp.num)
			t.Logf("res:%#016b", res.num)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
		})
	}
}

type intTest16 struct {
	int uint64
	exp Posit16es1
}

var fromint32tests16 = map[string]intTest16{
	"1": {
		int: 0b00001110011111001010111010000000,
		exp: Posit16es1{
			num: 0b0111111111111111,
		},
	},
	"2": {
		int: 0b00000111011101100111001000101111,
		exp: Posit16es1{
			num: 0b0111111111111110,
		},
	},
	"3": {
		int: 0b11111000000000000000000000000000,
		exp: Posit16es1{
			num: 0b1000000000000010,
		},
	},
}

func TestFromint3216(t *testing.T) {
	for name, test := range fromint32tests16 {
		t.Run(name, func(t *testing.T) {
			res := NewPosit16ES1FromInt32(int32(test.int))
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			printFromIntTest16(test, res, t)
		})
	}
}

func printOpTest16(test opTest16, res Posit16es1, useb bool, t *testing.T) {
	t.Logf("srca:%#032b/%f", test.a.num, test.a.Float())
	if useb {
		t.Logf("srcb:%#032b/%f", test.b.num, test.b.Float())
	}
	t.Logf("exp:%#032b/%f", test.exp.num, test.exp.Float())
	t.Logf("res:%#032b/%f", res.num, res.Float())
}

func printFromIntTest16(test intTest16, res Posit16es1, t *testing.T) {
	t.Logf("src:%d", test.int)
	t.Logf("exp:%#032b/%f", test.exp.num, test.exp.Float())
	t.Logf("res:%#032b/%f", res.num, res.Float())
}

func printToIntTest16(test intTest16, res uint64, signed bool, t *testing.T) {
	t.Logf("src:%#032b/%f", test.exp.num, test.exp.Float())
	if signed {
		t.Logf("exp:%d", int64(test.int))
		t.Logf("res:%d", int64(res))
	} else {
		t.Logf("exp:%d", test.int)
		t.Logf("res:%d", res)
	}
}
