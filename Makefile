ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

positc.: $(wildcard softposit-c/**/*.rs)
	cd softposit-c/build/Linux-x86_64-GCC; make all
	cp softposit-c/build/Linux-x86_64-GCC/softposit.a ./positc.a

rustfuzzing: *.go softposit-c/source/include/softposit.h  ./positc.
	go test -tags cgo_bench -ldflags="-r $(ROOT_DIR)positc" --run=Fuz -parallel 7 -timeout 24h

CBenching: *.go softposit-c/source/include/softposit.h  ./positc.
	go test -tags cgo_bench -ldflags="-r $(ROOT_DIR)positc" --run=XX --bench=BenchmarkC -benchtime 30s -timeout 24h

graphs: *.go
	cd grapher && go run *.go