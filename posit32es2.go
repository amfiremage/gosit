package posit

import (
	"math"
	"math/bits"
	"strconv"
	"strings"
)

type Posit32es2 struct {
	num uint32
}

func NewPosit32es2FromBits(bits uint32) Posit32es2 {
	return Posit32es2{num: bits}
}

func (a Posit32es2) Float() float64 {
	if a.num == 0 {
		return 0.0
	}
	if a.num == 1<<(32-1) {
		return math.Inf(1)
	}
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum << 2 | 1<<(32-1)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	frac :=  float64(afracP1)/float64(uint32(0b1<<(32-1)))
	if aneg {
		return -(math.Pow(2.0, float64(ascale)) * frac)
	} else {
		return math.Pow(2.0, float64(ascale)) * frac
	}
}


// String converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit32es2) String() string {
	return a.FormatScientific()
}

// FormatScientific converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit32es2) FormatScientific() string {
	switch a.num {
	case 0:
		return "0"
	case 1<<31:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	start := i - 1
	end := i - 1

	var s string
	if aneg {
		buf[i-2] = '-'
		start--
	}

	buf[i-1] = buf[i]

	if i+1 != len(buf) {
		last_non0 := 0
		for index, c := range buf[i+1:] {
			if c != '0' {
				last_non0 = index + 2
			}
		}
		buf[i] = '.'
		end = i - 1 + last_non0
	}
	s = string(buf[start : end+1])
	power := int64(ascaleB10) + int64(len(buf[i+1:]))
	if power == 1 {
		s += "*10"
	} else if power != 0 {
		s += "*10^" + strconv.FormatInt(int64(ascaleB10)+int64(len(buf[i+1:])), 10)
	}
	return s
}

// FormatBasic converts the Posit into a string in basic numerical notation.
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit32es2) FormatBasic() string {
	switch a.num {
	case 0:
		return "0"
	case 1<<31:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	var s string
	if aneg {
		s = "-"
	}
	switch {
	case int32(len(buf))+int32(ascaleB10) > int32(i):
		s += string(buf[i : len(buf)+int(ascaleB10)])
		if ascaleB10 != 0 {
			s += "."
			s += string(buf[len(buf)+int(ascaleB10):])
		}
	case int32(len(buf))+int32(ascaleB10) <= int32(i):
		s += "0."
		for int32(len(buf))+int32(ascaleB10) < int32(i) {
			s += "0"
			ascaleB10++
		}

		s += string(buf[i:])
	}
	s = strings.TrimRight(s, "0")
	s = strings.TrimRight(s, ".")
	return s
}

func (a Posit32es2) getDigits() ([64]byte, int32, int, bool) {
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	var reg int8
	r1 := uint32(2)
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		reg = 0
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}

	sftnum <<= 2
	afracP1 := uint64((sftnum << (2 & 0x1f)) | (1 << 31))
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascaleB2 := (int32(reg) << (2 & 0x1f)) + int32(aexp)

	
	ascaleB10 := ascaleB2 - (32 - 1)
	ascaleB5 := -ascaleB10
	if ascaleB5 < 0 {
		if ascaleB5 < - 32{
			ascaleB5 += 32
			afracP1 <<= 32
		} else {
			afracP1 <<= -ascaleB5
			ascaleB5 = 0
		}
		ascaleB10 = 0
	}

	//we need to have at least 3 bits clean for a non-rounded multiplication by 5
	for ascaleB5 > 0 && afracP1>>(64-3) == 0 {
		afracP1 *= 5
		ascaleB5--
	}

	var buf [64]byte
	i := len(buf)

	for afracP1 >= 100 {
		is := afracP1 % 100 * 2
		afracP1 /= 100
		i -= 2
		buf[i+1] = smallsString[is+1]
		buf[i+0] = smallsString[is+0]
		//we need to have at least 3 bits clean for a non-rounded multiplication by 5
		for ascaleB5 > 0 && afracP1>>(64-3) == 0 {
			afracP1 *= 5
			ascaleB5--
		}
		for ascaleB5 < 0 && afracP1>>(64-1) == 0 {
			afracP1 <<=1
			ascaleB5++
		}
	}

	// afracP1 < 100
	is := afracP1 * 2
	i--
	buf[i] = smallsString[is+1]
	if afracP1 >= 10 {
		i--
		buf[i] = smallsString[is]
	}
	return buf, ascaleB10, i, aneg
}
func NewPosit32ES2FromFloat(f float64) Posit32es2 {
	if math.IsNaN(f) || math.IsInf(f, 0) {
		return NewPosit32es2FromBits(1 << 31)
	}

	bits := math.Float64bits(math.Abs(f))

	scale := int16((bits&0x7fffffffffffffff)>>52)
	combinedFrac := bits & 0x000fffffffffffff
	if scale == 0 {
		if combinedFrac == 0 {
			return Posit32es2{
				num: 0,
			}
		} else {
			combinedFrac >>= 1
			scale++
		}
	}
	scale -= 1023

	combinedFrac >>= 52 - 32
	endreg := scale >> 2
	endexp := uint32(scale - (endreg << 2))

	var outPosit uint32
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 32-2 {
			outPosit = 1
			if math.Signbit(f) {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit32es2{num: outPosit}
		}
		outPosit = (0b1 << (31 - 1)) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 32-2 {
			outPosit = 0xffffffff >> 1
			if math.Signbit(f) {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit32es2{num: outPosit}
		}
		outPosit = (0xffffffff >> 1) - ((0xffffffff >> 1) >> (outn & 0x1f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 31 - 2 - outn - 1

	combinedFrac >>= 2 + outn + 2

	outPosit |= uint32(endexp) << outFracBits
	outPosit |= uint32(combinedFrac)

	if math.Signbit(f) {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

//This returns a Posit that best represents neg*2^scale*(1+frac)
func NewPosit32ES2FromComponents(neg bool, scale int, frac uint32) Posit32es2 {
	endreg := scale >> 2
	endexp := uint32(scale - (endreg << 2))

	var outPosit uint32
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 32-1 {
			return Posit32es2{num: 0}
		}
		outPosit = (0b1 << (31 - 1)) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 32-2 {
			outPosit = 0xffffffff >> 1
			if neg {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit32es2{num: outPosit}
		}
		outPosit = (0xffffffff >> 1) - ((0xffffffff >> 1) >> (outn & 0x1f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 31 - 2 - outn - 1

	frac >>= 2 + outn + 2

	outPosit |= uint32(endexp) << outFracBits
	outPosit |= uint32(frac)

	if neg {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

func NewPosit32ES2FromInt64(x int64) Posit32es2 {
	var a Posit32es2
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint64(x)
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 64-1 - outscale

	endreg := outscale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)
	if endreg > 32-3 {
		a.num = 0xffffffff >> 1
		if aneg {
			a.num = uint32(-int32(a.num))
		}
		return a
	}
	a.num = 0xffffffff - ((0xffffffff>>1) >> endreg)

	h := uint32(0xffffffff >> 2)
	a.num |= bits.RotateLeft32(endexp, 32-int(endreg)-2-2) & h
	a.num |= uint32(((outfrac&0x7fffffffffffffff)>>(64-32+2+1)) >> endreg)


	mask := uint64((1<<(64-32+2+1))<<endreg)

	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	return a
}

func NewPosit32ES2FromUint64(x uint64) Posit32es2 {
	var a Posit32es2
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := uint64(x)
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 64 - 1 - outscale

	endreg := outscale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)

	if endreg > 32-3 {
		a.num = 0xffffffff >> 1
		return a
	}
	a.num = 0xffffffff - ((0xffffffff>>1) >> endreg)

	h := uint32(0xffffffff >> 2)
	a.num |= bits.RotateLeft32(endexp, 32-int(endreg)-2-2) & h
	a.num |= uint32(((outfrac&0x7fffffffffffffff)>>(64-32+2+1)) >> endreg)


	mask := uint64((1<<(64-32+2+1))<<endreg)

	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func NewPosit32ES2FromInt32(x int32) Posit32es2 {
	var a Posit32es2
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint32(x)
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= 32-1 - outscale

	endreg := outscale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)
	if endreg > 32-3 {
		a.num = 0xffffffff >> 1
		if aneg {
			a.num = uint32(-int32(a.num))
		}
		return a
	}
	a.num = 0xffffffff - ((0xffffffff>>1) >> endreg)

	h := uint32(0xffffffff >> 2)
	a.num |= bits.RotateLeft32(endexp, 32-int(endreg)-2-2) & h
	a.num |= uint32(((outfrac&0x7fffffff)>>(32-32+2+1)) >> endreg)


	mask := uint32((1<<(2+1))<<endreg)


	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	return a
}

func NewPosit32ES2FromUint32(x uint32) Posit32es2 {
	var a Posit32es2
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := uint32(x)
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= 32 - 1 - outscale

	endreg := outscale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)

	if endreg > 32-3 {
		a.num = 0xffffffff >> 1
		return a
	}
	a.num = 0xffffffff - ((0xffffffff>>1) >> endreg)

	h := uint32(0xffffffff >> 2)
	a.num |= bits.RotateLeft32(endexp, 32-int(endreg)-2-2) & h
	a.num |= uint32(((outfrac&0x7fffffff)>>(32-32+2+1)) >> endreg)


	mask := uint32((1<<(2+1))<<endreg)


	//TODO: Maybe use this masking method on all operations?
	if outfrac&(mask-1) != 0 || a.num&2 != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func (a Posit32es2) Int64() int64 {
	if a.num == 0 || a.num == 1<<31 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	ascale -= 32-1
	if ascale < 0 {
		ret := afracP1 >> uint64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -int64(ret)
		}
		return int64(ret)
	} else if ascale >= 32 {
		if aneg {
			return math.MinInt64
		}
		return math.MaxInt64
	}
	ret := uint64(afracP1) << uint64(ascale)
	if aneg {
		return int64(-ret)
	}
	return int64(ret)
}

func (a Posit32es2) Uint64() uint64 {
	if a.num == 0 || a.num == 1<<31 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	ascale -= 32-1
	if ascale < 0 {
		ret := afracP1 >> uint64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint64(ret)
	} else if ascale >= 32 + 1 {
		return math.MaxUint64
	}
	return uint64(afracP1) << uint64(ascale)
}


func (a Posit32es2) Int32() int32 {
	if a.num == 0 || a.num == 1<<31 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	ascale -= 32-1
	if ascale < 0 {
		ret := afracP1 >> uint32(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint32(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -int32(ret)
		}
		return int32(ret)
	} else if ascale >= 32 {
		if aneg {
			return math.MinInt32
		}
		return math.MaxInt32
	}
	ret := uint32(afracP1) << uint32(ascale)
	if aneg {
		return int32(-ret)
	}
	return int32(ret)
}

func (a Posit32es2) Uint32() uint32 {
	if a.num == 0 || a.num == 1<<31 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	ascale -= 32-1
	if ascale < 0 {
		ret := afracP1 >> uint32(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<uint32(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint32(ret)
	} else if ascale >= 32 + 1 {
		return math.MaxUint32
	}
	return uint32(afracP1) << uint32(ascale)
}


func (a Posit32es2) Le(b Posit32es2) bool {
	return a.num <= b.num
}

func (a Posit32es2) Lt(b Posit32es2) bool {
	return a.num < b.num
}

func (a Posit32es2) Eq(b Posit32es2) bool {
	return a.num == b.num
}

func (a Posit32es2) Add(b Posit32es2) Posit32es2 {
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		if a.num == 1<<31 {
			return NewPosit32es2FromBits(1 << 31)
		}

		a.num = uint32(-int32(a.num))
	} else {
		if a.num == 0{
			return b
		}	
	}

	xneg := aneg
	bneg := b.num&(1<<(32-1)) != 0
	if bneg {
		if b.num == 1<<31 {
			return NewPosit32es2FromBits(1 << 31)
		}

		xneg = !xneg
		b.num = uint32(-int32(b.num))
	} else {
		if b.num == 0{
			if aneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}	
	}

	//This allows less branching later on.
	//This potentially leaves bneg in the wrong position, dont use bneg!!
	if a.num < b.num {
		a.num, b.num = b.num, a.num
		aneg = bneg
	} else if a.num == -b.num {
		a.num = 0
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - 2)
	bscale := (int32(reg) << 2) + int32(bexp)

	//Calc

	// We are using a trick to not have to double-negate.
	// The simple way to do this would be to negate af and fb
	// on aneg and bneg respectively, and negate the result if the result should be negated.
	// Instead we take advantage that a>b, so we never have to negate a and
	// we can negate b if and only if bneg^aneg.

	combinedFrac := uint64(afracP1) << 31

	tmp := bscale - ascale + 31
	if tmp > 0 {
		bf := uint64(bfracP1) << (tmp & 0x3f)
		if xneg {
			combinedFrac -= bf
		} else {
			combinedFrac += bf
		}
	}

	if xneg {
		if combinedFrac == 0 {
			a.num = 0
			return a
		}
		//This is faster than LeadingZeros
		for (combinedFrac>>(64-2))&1 == 0 {
			ascale--
			combinedFrac <<= 1
		}
	}

	// leave the hidden bit in
	overflow := int32(combinedFrac >> 63)
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(ascale) &^ (f << 2)

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 32-2 {
			a.num = 1
			if aneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 32-2 {
			a.num = 0xffffffff >> 1
			if aneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
	}

	combinedShift := (2 + outn) & 0x3f
	combinedFrac >>= combinedShift

	outfrac := uint32(combinedFrac >> 31) & ((0xffffffff >> (1 + 2)) >> (outn & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-int(combinedShift)) & (0xffffffff >> 2)

	if combinedFrac&(0xffffffff>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if aneg {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

func (a Posit32es2) Sub(b Posit32es2) Posit32es2 {
	b.num = uint32(-int32(b.num))
	return a.Add(b)
}

func (a Posit32es2) Neg() Posit32es2 {
	a.num = uint32(-int32(a.num))
	return a
}

func (a Posit32es2) Mul(b Posit32es2) Posit32es2 {
	//A
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		if a.num == 1<<31 {
			return NewPosit32es2FromBits(1 << 31)
		}

		a.num = uint32(-int32(a.num))
	}

	xneg := aneg
	bneg := b.num&(1<<(32-1)) != 0
	if bneg {
		if b.num == 1<<31 {
			return NewPosit32es2FromBits(1 << 31)
		}
	
		xneg = !xneg
		b.num = uint32(-int32(b.num))
	}


	if a.num == 0 || b.num == 0 {
		return NewPosit32es2FromBits(0)
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}

	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - 2)
	ascale += (int32(reg) << 2) + int32(bexp)

	//Calc

	af := int64(afracP1)
	bf := int64(bfracP1)
	combinedFrac := uint64(af * bf)

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 32-2 for the number

	// leave the hidden bit in
	overflow := int32(combinedFrac >> 63)
	ascale += overflow
	combinedFrac >>= overflow & 0b1

	endreg := ascale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(ascale) &^ (f << 2)

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 32-2 {
			a.num = 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 32-2 {
			a.num = 0xffffffff >> 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
	}

	combinedShift := (2 + outn) & 0x3f
	combinedFrac >>= combinedShift

	outfrac := uint32(combinedFrac >> 31) & ((0xffffffff >> (1 + 2)) >> (outn & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-int(combinedShift)) & (0xffffffff >> 2)

	if combinedFrac&(0xffffffff>>1) != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

func (a Posit32es2) Div(b Posit32es2) Posit32es2 {
	//A
	aneg := a.num&(1<<(32-1)) != 0
	xneg := aneg
	bneg := b.num&(1<<(32-1)) != 0
	
	if bneg {
		if b.num == 1<<31 {
			return b
		}
	
		xneg = !xneg
		b.num = uint32(-int32(b.num))
	} else if b.num == 0 {
		return NewPosit32es2FromBits(1 << 31)
	}

	if aneg {
		if a.num == 1<<31 {
			return a
		}
	
		a.num = uint32(-int32(a.num))
	} else if a.num == 0 {
		return a
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << 2) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - 2)
	ascale -= (int32(reg) << 2) + int32(bexp)

	//Calc

	af := uint64(afracP1)
	bf := uint64(bfracP1)
	combinedFrac := uint64(af<<(31-1)) / bf

	if combinedFrac == 0 {
		a.num = 0
		return a
	}
	rem := uint64(af<<(31-1)) % (uint64(bf))

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 32-2 for the number

	// leave the hidden bit in
	overflow := combinedFrac>>(31-1) == 0
	if overflow {
		ascale -= 1
		combinedFrac <<= 1
	}

	endreg := ascale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(ascale) &^ (f << 2)

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 32-2 {
			a.num = 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 32-2 {
			a.num = 0xffffffff >> 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
	}

	combinedShift := (2 + outn - 1) & 0x3f

	outfrac := (uint32(combinedFrac) >> (combinedShift&0x3f) ) & ((0xffffffff >> (1 + 2)) >> (outn & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-2-int(outn)) & (0xffffffff >> 2)

	mask := uint64(1 << (combinedShift & 0x3f)) -1
	if combinedFrac&mask != 0 || rem != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

func (a Posit32es2) Sqrt() Posit32es2 {
	if a.num>>31 != 0 {
		return NewPosit32es2FromBits(1 << 31)
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		if a.num == 0 {
			return a
		}
	
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := sftnum << 2
	sftnum <<= 1
	aexp := sftnum >> (32 - 2)

	//Calc
	n := uint64(afracP1) << 31
	if aexp&1 != 0 {
		n = (n << 1) | (0b1 << 63)
		n -= 1 << (64 - 2)
	}

	if reg&1 != 0 {
		aexp += 1 << 2
	}
	reg >>= 1
	aexp >>= 1

	//out
	var outPosit uint32
	var outn uint8
	if reg < 0 {
		outn = uint8(-reg)
		outPosit = (0b1 << (31 - 1)) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + reg)
		outPosit = (0xffffffff >> 1) - ((0xffffffff >> 1) >> (outn & 0x1f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 31 - 1 - 2 - outn

	//more calc
	//https://stackoverflow.com/a/31120562

	combinedFrac := uint64(1 << (64 - 2 - 2*2 - (outn << 1)))
	one := combinedFrac >> 2
	n >>= ((outn << 1) + 2*2) & 0x3f

	//There are some tricky performance characteristics of modifying this code
	//Allignment changes the benchmark for this loop a lot (jumping to aligned addeesses ...)
	//Dont be allarmed if suddenly this code performs worse with a small change
	for one != 0 {
		a1 := combinedFrac + one
		if n >= a1 {
			n -= a1
			combinedFrac += one << 1
		}
		combinedFrac >>= 1
		one >>= 2
	}
	g := uint8((2 + 2 + outn) & 0x3f)
	outPosit |= uint32(aexp) << outFracBits

	outfrac := uint32(combinedFrac>>1) << g
	outfrac >>= g

	outPosit |= outfrac
	outPosit += uint32(combinedFrac) & 1
	return Posit32es2{num: outPosit}
}

func (a Posit32es2) Ceil() Posit32es2 {
	//A
	decnum := a.num
	aneg := decnum>>31 != 0
	if aneg {
		if a.num == 1<<31 {
			return a
		}

		decnum = uint32(-int32(a.num))
	}
	if a.num == 0 {
		return a
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<(32-2)) == 0 {
		if aneg {
			a.num = 0
		} else {
			a.num = 1<<(32-2)
		}
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := uint32(1 << (32 - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= 32 - 2
	ascale := (uint32(m) << 2) + aexp

	//calculate the how many bits are "bad"
	badBits := int8(32-2-2-m-1) - int8(ascale)
	if badBits >= 0 {
		leftover := decnum &^ (0xffffffff << (badBits & 0x1f))
		decnum &= (0xffffffff << (badBits & 0x1f))
		if leftover != 0 != aneg {
			decnum += 1 << (badBits & 0x1f)
		}

		if aneg {
			a.num = uint32(-int32(decnum))
		} else {
			a.num = decnum
		}
	}

	return a
}

func (a Posit32es2) Truncate() Posit32es2 {
	//A
	decnum := a.num
	aneg := decnum>>31 != 0
	if aneg {
		if a.num == 1<<31 {
			return a
		}

		decnum = uint32(-int32(a.num))
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<(32-2)) == 0 {
		a.num = 0
		return a
	}

	var m uint8
	aexp := (decnum << 3)
	one := uint32(1 << (32 - 3))
	for decnum&one != 0 {
		m++
		one >>= 1
		aexp <<= 1
	}
	aexp >>= 32 - 2

	ascale := (uint32(m) << 2) + aexp

	//Calc
	tmp := int8(32-2-2-m-1) - int8(ascale)
	if tmp >= 0 {
		if aneg {
			a.num = uint32(-int32(decnum & (0xffffffff << (tmp & 0x1f))))
		} else {
			a.num = a.num & (0xffffffff << (tmp & 0x1f))
		}
	}

	return a
}

func (a Posit32es2) Round() Posit32es2 {
	//A
	decnum := a.num
	aneg := decnum>>31 != 0

	if aneg {
		if a.num == 1<<31 {
			return NewPosit32es2FromBits(1 << 31)
		}
		decnum = uint32(-int32(decnum))
	}

	if decnum&(1<<(32-2)) == 0 {
		aexp := decnum
		if aexp&(1 << (31 - 2)) == 0 {
			//the second time we know for sure an>1 aka a<0.5
			a.num = 0
			return a
		}
		aexp <<= 3
		aexp >>= 32 - 2

		if aexp != (1<<2)-1 || decnum<<(3+2) == 0 {
			a.num = 0
			return a
		} else {
			if aneg {
				a.num = 0b11 << (31 - 1)
				return a
			}else{
				a.num = 0b1 << (31 - 1)
				return a
			}
		}
	}

 	var an = uint8(1)
	aexp := decnum
	for aexp&(1 << (31 - 2)) != 0 {
		an++
		aexp <<= 1
	}

	aexp <<= 3
	aexp >>= 32 - 2

	var m = an - 1
	ascale := (uint32(m) << 2) + aexp

	//Calc
	// we need to make sure to tie-break to nearest even, not uneven.
	// To make sure 1.5 also breaks correctly, we | the hidden bit
	tmp := 2 + an + uint8(ascale)
	if an < 31-2 && (((decnum|((0x1<<(32-2-2))>>(an&0x0f)))>>((31-2-tmp)&0x1f))&3 == 3 || (decnum<<((2+tmp)&0x1f)) > (1<<31)) {
		//slowpath
		afracP1 := decnum | (0b1 << ((31 - (2 + an + 1)) & 0x1f))
		afracP1 &= (0xffffffff >> (2 + 1)) >> (an & 0x1f)
		afracP1 += (0b1 << (31 - 1)) >> tmp
		if afracP1&((0b1<<(31-2))>>(an&0x1f)) != 0 {
			ascale++
			tmp++

			endreg := ascale >> 2
			f := uint32(0xffffffff)
			endexp := uint32(ascale) &^ (f << 2)
		
			outn := uint8(1 + endreg)
			a.num = (0xffffffff >> 1) - ((0xffffffff >> 1) >> (outn & 0x1f))

			outFracBits := 31 - 1 - 2 - outn
			afracP1 >>= 1
			afracP1 &= ((0xffffffff >> (2 + 2)) >> (outn & 0x1f))
			a.num |= endexp << (outFracBits & 0x1f)
		} else {
			outFracBits := 31 - 1 - 2 - an
			a.num = decnum & (0xffffffff << (outFracBits & 0x1f))
			afracP1 &= (0xffffffff >> (2 + 2)) >> (an & 0x1f)
		}
		if (31 - 1 - int8(tmp)) >= 0 {
			afracP1 = afracP1 & (0xffffffff << ((31 - 1 - tmp) & 0x1f))
		}
		a.num |= afracP1
		if aneg {
			a.num = uint32(-int32(a.num))
		}
		return a
	} else {
		tmp := int8(31-1-2-an) - int8(ascale)
		if tmp >= 0 {
			if aneg {
				a.num = uint32(-int32(decnum & (0xffffffff << (tmp & 0x1f))))
			} else {
				a.num = a.num & (0xffffffff << (tmp & 0x1f))
			}
		}
	}
	return a
}

func (a Posit32es2) Exp() Posit32es2 {
	//TODO: should we have per-size tables and calculations? like a 32 bit table since we wont need more than 16 bits of accuracy anyways

	//A
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	// check for things that result in 1
	// Modifies version of normal calculations.
	// scale = -28, but its negated since to avoid negating it at runtime
	// also avoids branches because ist never bigger than 30
	if a.num <= 8388608 {
		a.num = 1<<(32-2)
		return a
	}

	if a.num >= 1763704832 {
		a.num = 0xffffffff >> 1
		if aneg {
			a.num = 0
		}
		return a
	}

	var reg int8
	r1 := uint8(1)
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}
	n := 32 - 1 - 2 - r1 - 1
	if n >= 32 {
		n = 0
	}

	afrac := a.num & ((1 << (n)) - 1)
	sftnum <<= 3
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg) << 2) + int32(aexp)

	for afrac&1 == 0 && afrac != 0 {
		afrac >>= 1
		n--
	}

	if afrac == 0 || n > 32 {
		n = 0
	}

	var outscale uint64
	finalscale := uint64(1 << 63)
	var outfrac uint64
	var power uint64
	var powerOfTwo uint32
	var baseindex uint8

	// Instead of taking the base out of the array directly, we store the index.
	// This way we can look up `base*base` instead of calculating. This reduces errors.
	// From 29 on it always results in 1  ( in 32 bits)
	if ascale < 0 {
		// e^((1+a/2^n)*2^b)	=	e^(a/2^n*2^b)*e^(2^b)
		//						=	e^(a/2^n*2^b)*e^(2^b)
		//						=	(e^(1/2^n))^(a*2^b)*e^(1/2^(-b))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee2[-ascale]
		baseindex = n
		power = uint64(afrac >> -uint32(ascale))

		// We have rounded off everything after the binary point in the power.
		// Here we find the biggest fraction of this, so the biggest 1/2^n.
		// Then we add this again later
		powfracBits := uint8(-ascale)

		i := uint8(0)
		if int8(baseindex+powfracBits)+1-32 >= 0 {
			i = baseindex + powfracBits + 1 - 32
		}
		if afrac<<(32-i) != 0 {
			afrac += 1 << i
		}

		// This is prefered over doing it in the main loop,
		// because we dont have to check for overflow.
		for ; i < powfracBits; i++ {
			if afrac&(1<<i) != 0 {
				finalscale, _ = bits.Mul64(finalscale, erootee2[baseindex+powfracBits-i])
				finalscale <<= 1
				finalscale += 1
			}
		}
	} else if ascale < int32(n) {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^(n-b))
		//						=	e^(2^b)*e^a^(1/(2^(n-b)))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = n - uint8(ascale)
		power = uint64(afrac)
		outscale += 1 << (powerOfTwo & 0x3f)
	} else {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a*2^(b-n))
		//						=	e^(2^b)*e^(a*2^(b-n))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = 0
		power = uint64(afrac << (uint32(ascale) - uint32(n)))
		outscale += 1 << powerOfTwo
	}

	for powerOfTwo > 0 {
		powerOfTwo--
		outfrac, _ = bits.Mul64(outfrac, outfrac)

		if outfrac&(1<<63) != 0 {
			outscale += 1 << powerOfTwo
			outfrac >>= 1
		}
		outfrac <<= 1
	}

	if int(baseindex) < len(erootee2) {
		for ; power != 0 && baseindex != 0; power >>= 1 {
			if power&1 == 1 {
				outfrac, _ = bits.Mul64(outfrac, erootee2[baseindex])
				if outfrac&(1<<63) != 0 {
					outscale++
					outfrac >>= 1
				}
				outfrac <<= 1
			}
			baseindex--
		}
	}
	if power != 0 && baseindex == 0 {
		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, erootee0)

			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
		outscale += power
		power >>= 1
	}
	base := erootee0

	for ; power != 0; power >>= 1 {
		base, _ = bits.Mul64(base, base)
		if base&(1<<63) != 0 {
			outscale += power
			base >>= 1
		}
		base <<= 1

		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, base)
			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
	}
	outfrac, low := bits.Mul64(outfrac, finalscale)
	outfrac <<= 1

	endreg := outscale >> 2
	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)

	outn := uint8(1 + endreg)
	outPosit := uint32(0xffffffff - (0xffffffff >> (outn & 0x1f)))

	combinedShift := (2 + uint8(outn) - 1) & 0x3f
	mask := uint64((2<<32)<<(combinedShift&0x3f)) -1

	outfrac2 := outfrac >> (32 + 1+32-32) // now the frac is the entirity of the output

	outPosit |= (uint32(outfrac2) >> (combinedShift & 0x3f)) & ((0xffffffff >> (2+1)) >> (outn&0x1f))
	outPosit |= bits.RotateLeft32(endexp, 32-2-int(combinedShift)) & (0xffffffff >> 2)

	if outPosit&2 == 2 || outfrac&mask != 0 || low != 0 {
		outPosit += 1
	}
	outPosit >>= 1

	p := Posit32es2{
		num: outPosit,
	}

	if aneg {
		p = p.Div(NewPosit32es2FromBits(1<<(32-2)))
	}
	return p
}

func (a Posit32es2) Log2() Posit32es2 {
	// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation

	if a.num == 0 || a.num&(1<<(32-1)) != 0{
		a.num = 1<<(32-1)
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 1
	afracP1 := uint64(((sftnum << 2) | (1<<(32-2))) &^ (1<<(32-1))) // shifted one to the right compared to normal afracP1
	afracP1 <<= (32-32)
	sftnum <<= 2
	aexp := sftnum >> (32 - 2)
	ascale := (int32(reg)<<2) + int32(aexp)

	n := uint32(ascale)
	if ascale < 0 {
		n = uint32(-int32(n)) -1
	}

	var outPosit uint32
	var outn uint8
	var series uint32
	var outscale int32
	seg := uint32(1 << (31 - 2))

	// This is a very specialised version of the algorithm to use less bits.
	if ascale < 0 {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			var passedFirst bool
			var zeroCounter bool = true
			for afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				seg >>= 1
				if afracP1&(1<<31) != 0 {
					afracP1 >>= 1
					series -= seg

					if zeroCounter {
						// this means the last bit is 0 again, so we shift by one.
						seg <<= 1
						series <<= 1
						outscale--
						zeroCounter = true
					} else if passedFirst {
						break
					} else {
						passedFirst = true
						outscale--
					}
				} else if zeroCounter {
					zeroCounter = false
				}
			}

			seg <<= 1
			series <<= 1

			series &^= 1 << (31 - 2)

			endreg := int16(outscale>>2)
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 32-2 {
					x := -1
					a.num = uint32(-int32(x))
					return a
				}
				outPosit = (0b1 << 31) >> (outn & 0x1f)
			} else {
				outn = 1
				outPosit = 0xffffffff - (0xffffffff >> outn)
			}
		} else {
			// pre flipping the first bit, trick to be able to more the second loop after the first
			series = 1 << (31 - 2)
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n%2) << (31-1-2))
				n >>= 1
				seg >>= 1
			}
			if afracP1 == (1 << 30) {
				outscale++
			}

			endreg := int16(outscale>>2)
			outn = uint8(1 + endreg)
			if outn > 32-2 {
				x := 0xffffffff >> 1
				a.num = uint32(-int32(x))
				return a
			}
			outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
		}
		series >>= outn - 1
		seg >>= outn 
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series -= seg
			}
			seg >>= 1
		}
		series >>= 1
	} else {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			for afracP1&(1<<31) == 0 && afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				outscale--
			}
			if afracP1 == 1<<30 {
				a.num = 0
				return a
			}
			n = 1

			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}

			endreg := int16(outscale>>2)
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 32-2 {
					a.num = 1
					return a
				}
				outPosit = (0b1 << 31) >> (outn & 0x1f)
			} else {
				outn = 1
				outPosit = 0xffffffff - (0xffffffff >> outn)
			}
		} else {
			// Here we first shift the series and seg to the right to limmit the amount of calculations done.
			for n > 1 {
				outscale += 1
				series = series>>1 + ((n%2) << (31-1-2))
				seg >>= 1
				n >>= 1
			}

			endreg := int16(outscale>>2)
			outn = uint8(1 + endreg)
			if outn > 32-2 {
				a.num = 0xffffffff >> 1
				return a
			}
			outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))		
		}
		series >>= outn
		seg >>= outn + 1
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}
			seg >>= 1
		}
	}

	f := uint32(0xffffffff)
	endexp := uint32(outscale) &^ (f << 2)

	outfrac := series & ((0xffffffff >> (1 + 2)) >> (outn & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-2-int(outn)) & (0xffffffff >> 2)

	if afracP1 != (1<<30) || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if ascale < 0 {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit32es2{num: outPosit}
}

func (a Posit32es2) Loge() Posit32es2 {
	log2 := a.Log2()
	// Constant is log2(e)
	return log2.Div(Posit32es2{num: 1133159347})
}

func (a Posit32es2) Logb(b Posit32es2) Posit32es2 {
	log2 := a.Log2()
	return log2.Div(b.Log2())
}

func (b Posit32es2) Pow(p Posit32es2) Posit32es2 {
	ln := b.Loge()
	e := ln.Mul(p)
	return e.Exp()
}
