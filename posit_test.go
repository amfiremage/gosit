package posit

import (
	"math"
	"testing"
)

type floatConvTest struct {
	f float64
	p Posit
}

var floatconvtests = map[string]floatConvTest{
	"1": {
		f: 3.5539270428763873e-06,
		p: Posit{
			num: 0b0_0001_101_11011101_00000000_00000000,
			es:  3,
		},
	},
	"2": {
		f: -9312,
		p: Posit{
			num: 0b1_00001_10_11011101_00000000_00000000,
			es:  2,
		},
	},
	"3": {
		f: 0.00021076202921221953,
		p: Posit{
			num: 0b0_00001_11_101110100000000000000000,
			es:  2,
		},
	},
	"4:0": {
		f: 0.0,
		p: Posit{
			num: 0b0000000000000000000000000000000000,
			es:  2,
		},
	},
}

func TestConvFloat(t *testing.T) {
	for name, test := range floatconvtests {
		t.Run(name, func(t *testing.T) {
			res := test.p.Float()
			t.Logf("exp:%v", test.f)
			t.Logf("res:%v", res)
			if math.Abs(res-test.f) > 0.000001 {
				t.Error("got the wrong float:", res, "expected:", test.f)
			}

			resp := NewPosit32FromFloat(test.f, test.p.es)
			t.Logf("exp:%#032b", test.p.num)
			t.Logf("res:%#032b", resp.num)
			if resp != test.p {
				t.Error("got the wrong posit:", resp, "expected:", test.p, ". Or", resp.Float(), "and", test.f)
			}
			switch test.p.es {
			case 2:
				p := NewPosit32es2FromBits(test.p.num)
				res := p.Float()
				t.Logf("exp:%v", test.f)
				t.Logf("res:%v", res)
				if math.Abs(res-test.f) > 0.000001 {
					t.Error("got the wrong float:", res, "expected:", test.f)
				}

				resp := NewPosit32ES2FromFloat(test.f)
				t.Logf("exp:%#032b", p.num)
				t.Logf("res:%#032b", resp.num)
				if resp != p {
					t.Error("got the wrong posit:", resp, "expected:", p, ". Or", resp.Float(), "and", test.f)
				}
			}
		})
	}
}

type stringConvTest struct {
	s string
	b string
	p Posit
}

var stringconvtests = map[string]stringConvTest{
	"1": {
		s: "3.553926944732666000025*10^-6",
		b: "0.000003553926944732666000025",
		p: Posit{
			num: 0b0_0001_101_11011101_00000000_00000000,
			es:  3,
		},
	},
	"2": {
		s: "-9.312*10^3",
		b: "-9312",
		p: Posit{
			num: 0b1_00001_10_11011101_00000000_00000000,
			es:  2,
		},
	},
	"3": {
		s: "2.1076202392578125*10^-4",
		b: "0.00021076202392578125",
		p: Posit{
			num: 0b0_00001_11_101110100000000000000000,
			es:  2,
		},
	},
	"4": {
		s: "5*10^-1",
		b: "0.5",
		p: Posit{
			num: 0b00111000000000000000000000000000,
			es:  2,
		},
	},
	"5": {
		s: "-3.67348359375*10^4",
		b: "-36734.8359375",
		p: Posit{
			num: 0b10000100111000010000001001010100,
			es:  2,
		},
	},
}

func TestConvString(t *testing.T) {
	for name, test := range stringconvtests {
		t.Run(name, func(t *testing.T) {
			res := test.p.FormatBasic()
			t.Logf("exp:%v", test.b)
			t.Logf("res:%v", res)
			if res != test.b {
				t.Error("got the wrong string:", res, "expected:", test.b)
			}
			res = test.p.FormatScientific()
			t.Logf("exp:%v", test.s)
			t.Logf("res:%v", res)
			if res != test.s {
				t.Error("got the wrong string:", res, "expected:", test.s)
			}
			switch test.p.es {
			case 2:
				p := NewPosit32es2FromBits(test.p.num)
				res := p.FormatBasic()
				t.Logf("exp:%v", test.b)
				t.Logf("res:%v", res)
				if res != test.b {
					t.Error("got the wrong string:", res, "expected:", test.b)
				}
				res = p.FormatScientific()
				t.Logf("exp:%v", test.s)
				t.Logf("res:%v", res)
				if res != test.s {
					t.Error("got the wrong string:", res, "expected:", test.s)
				}
			}
		})
	}
}

type boolTest struct {
	a   Posit
	b   Posit
	exp bool
}

var eqtests = map[string]boolTest{
	"1": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		exp: true,
	},
	"2": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: false,
	},
}

func TestEq(t *testing.T) {
	for name, test := range eqtests {
		t.Run(name, func(t *testing.T) {
			res := test.a.EqSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res, "expected:", test.exp)
			}
			res = test.b.EqSameES(test.a)
			if res != test.exp {
				t.Error("got the wrong float:", res, "expected:", test.exp)
			}
			var res2 bool
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				res2 = a.Eq(b)
				if res2 != test.exp {
					t.Error("got the wrong number:", res2, "expected:", test.exp)
				}
			}
			printBoolTest32(test, res, res2, t)
		})
	}
}

var letests = map[string]boolTest{
	"1": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		exp: true,
	},
	"2": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: true,
	},
}

func TestLe(t *testing.T) {
	for name, test := range letests {
		t.Run(name, func(t *testing.T) {
			res := test.a.LeSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res, "expected:", test.exp)
			}
			var res2 bool
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				res2 = a.Le(b)
				if res2 != test.exp {
					t.Error("got the wrong number:", res2, "expected:", test.exp)
				}
			}
			printBoolTest32(test, res, res2, t)
		})
	}
}

var lttests = map[string]boolTest{
	"1": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		exp: false,
	},
	"2": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: true,
	},
}

func TestLt(t *testing.T) {
	for name, test := range lttests {
		t.Run(name, func(t *testing.T) {
			res := test.a.LtSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res, "expected:", test.exp)
			}
			var res2 bool
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				res2 = a.Lt(b)
				if res2 != test.exp {
					t.Error("got the wrong number:", res2, "expected:", test.exp)
				}
			}
			printBoolTest32(test, res, res2, t)
		})
	}
}

type opTest struct {
	a   Posit
	b   Posit
	exp Posit
}

var addtests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b0_00001_11_11011101_00000000_00000000,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b0_00001_11_11101101_00000000_00000000,
			es:  2,
		},
	},
	"3": {
		a: Posit{
			num: 0b1_11110_10_00100000_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b0_00001_10_00001101_00000000_00000000,
			es:  2,
		},
	},
	"4:add1-0": {
		a: Posit{
			num: 0b01000101100011011010110001101011,
			es:  2,
		},
		b: Posit{
			num: 0b10000100111000010000001001010100,
			es:  2,
		},
		exp: Posit{
			num: 0b10000100111000010000010110110111,
			es:  2,
		},
	},
	"5:add1-1": {
		a: Posit{
			num: 0b11001010100000010111011101000100,
			es:  2,
		},
		b: Posit{
			num: 0b11100010010101101111000001110010,
			es:  2,
		},
		exp: Posit{
			num: 0b11001001000101110011001101100000,
			es:  2,
		},
	},
	"6:add1-2": {
		a: Posit{
			num: 0b10110001010101010011110101100011,
			es:  2,
		},
		b: Posit{
			num: 0b01000000010010000100010011001001,
			es:  2,
		},
		exp: Posit{
			num: 0b10110101011110010101111111001000,
			es:  2,
		},
	},
	"7:add1-3": {
		a: Posit{
			num: 0b00001010100110011011111111110101,
			es:  2,
		},
		b: Posit{
			num: 0b10011000110111111000101011101110,
			es:  2,
		},
		exp: Posit{
			num: 0b10011000110111111001000000100001,
			es:  2,
		},
	},
	"8": {
		a: Posit{
			num: 0b01011111100111100110110010101001,
			es:  2,
		},
		b: Posit{
			num: 0b01010101011001000101111011111011,
			es:  2,
		},
		exp: Posit{
			num: 0b01100001100101000010011100001010,
			es:  2,
		},
	},
	"9:add-4": {
		a: Posit{
			num: 0b11001110011111001011111010111100,
			es:  2,
		},
		b: Posit{
			num: 0b01110110011100110110110110010110,
			es:  2,
		},
		exp: Posit{
			num: 0b01110110011100110101101010001111,
			es:  2,
		},
	},
	"10:negativeShift-0": {
		a: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
		b: Posit{
			num: 0b01100001110010010010111000000100,
			es:  2,
		},
		exp: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
	},
	"11": {
		a: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
		b: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
	},
	"12": {
		a: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
		b: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
	},
	"13:restrictiveMask-0": {
		a: Posit{
			num: 0b10000000000000000110111001011100,
			es:  2,
		},
		b: Posit{
			num: 0b11111111111100101111000101010110,
			es:  2,
		},
		exp: Posit{
			num: 0b10000000000000000110111001011100,
			es:  2,
		},
	},
	"14:addNaN": {
		a: Posit{
			num: 0b00000000000000000000000000000001,
			es:  2,
		},
		b: Posit{
			num: 0b00000000000000000000000000000001,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000001,
			es:  2,
		},
	},
}

func TestAdd(t *testing.T) {
	for name, test := range addtests {
		t.Run(name, func(t *testing.T) {
			res := test.a.AddSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			res = test.b.AddSameES(test.a)
			if res != test.exp {
				t.Error("got the wrong float:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Add(b)
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, true, t)
		})
	}
}

var subtests = map[string]opTest{
	"1:zeroRes": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b0_0000000_00000000_00000000_00000000,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b1_111110_01_0000000_00000000_00000000,
			es:  2,
		},
	},
	"3": {
		a: Posit{
			num: 0b1_11110_10_00100000_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		exp: Posit{
			num: 0b1_11110_00_10001001_10000000_00000000,
			es:  2,
		},
	},
}

func TestSub(t *testing.T) {
	for name, test := range subtests {
		t.Run(name, func(t *testing.T) {
			res := test.a.SubSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Sub(b)
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, true, t)
		})
	}
}

var multests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b00011011100000110111111010010111,
			es:  2,
		},
		b: Posit{
			num: 0b00000011100000010000011110100011,
			es:  2,
		},
		exp: Posit{
			num: 0b00000001011110010010111110000101,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b10100110111101110111011101100101,
			es:  2,
		},
		b: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		exp: Posit{
			num: 0b10001111111001011110010001011011,
			es:  2,
		},
	},
	"3:mul0": {
		a: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		b: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"4:mulInf": {
		a: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		b: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"5": {
		a: Posit{
			num: 0b00111000000000000000000000000000,
			es:  2,
		},
		b: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b00111000000000000000000000000000,
			es:  2,
		},
	},
	"6": {
		a: Posit{
			num: 0b11111111110101111100010111100100,
			es:  2,
		},
		b: Posit{
			num: 0b00000000000000000000001001000000,
			es:  2,
		},
		exp: Posit{
			num: 0b11111111111111111111111111111111,
			es:  2,
		},
	},
	"7:toadd": {
		a: Posit{
			num: 0b11111111111111111010110101100110,
			es:  2,
		},
		b: Posit{
			num: 0b11111111111111110000100000101001,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000001,
			es:  2,
		},
	},
	"8": {
		a: Posit{
			num: 0b01111111111111111111110010000011,
			es:  2,
		},
		b: Posit{
			num: 0b01111111111010011101111010011110,
			es:  2,
		},
		exp: Posit{
			num: 0b01111111111111111111111111111111,
			es:  2,
		},
	},
}

func TestMul(t *testing.T) {
	for name, test := range multests {
		t.Run(name, func(t *testing.T) {
			res := test.a.MulSameES(test.b)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			res = test.b.MulSameES(test.a)
			if res != test.exp {
				t.Error("got the wrong float:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Mul(b)
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, true, t)
		})
	}
}

var divtests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b00010001101100111010100101100001,
			es:  2,
		},
		b: Posit{
			num: 0b01100100000010101011101101110000,
			es:  2,
		},
		exp: Posit{
			num: 0b00000111011010010010000101101010,
			es:  2,
		},
	},
	"2:add1-0": {
		a: Posit{
			num: 0b01111011001001100110000001010001,
			es:  2,
		},
		b: Posit{
			num: 0b10000101010101010110010110101001,
			es:  2,
		},
		exp: Posit{
			num: 0b10111100111101011001000011001110,
			es:  2,
		},
	},
	"3:div0": {
		a: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		b: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"3:divbyInf": {
		a: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		b: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
	},
	"4:divby0": {
		a: Posit{
			num: 0b01100011011100100011011001101100,
			es:  2,
		},
		b: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
	},
	"5:0sizedfrac": {
		a: Posit{
			num: 0b00000000000000000000000001010011,
			es:  2,
		},
		b: Posit{
			num: 0b01111010110110111000010110011101,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000110,
			es:  2,
		},
	},
	"6:add1-1": {
		a: Posit{
			num: 0b10000110111101001110111111000101,
			es:  2,
		},
		b: Posit{
			num: 0b01111111110100000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b11111110001111010011101111110001,
			es:  2,
		},
	},
	"7:add1-2": {
		a: Posit{
			num: 0b10110001100000110000001100101011,
			es:  2,
		},
		b: Posit{
			num: 0b00011100000000000000000000000000,
			es:  2,
		},
		exp: Posit{
			num: 0b10010100110000011000000110010110,
			es:  2,
		},
	},
	"8:neg": {
		a: Posit{
			num: 0b00000000000000000000000001100100,
			es:  2,
		},
		b: Posit{
			num: 0b10000000101011001000111101110111,
			es:  2,
		},
		exp: Posit{
			num: 0b11111111111111111111111111111111,
			es:  2,
		},
	},
	"9:add1-3": {
		a: Posit{
			num: 0b11111111111111010101110000011110,
			es:  2,
		},
		b: Posit{
			num: 0b10000000000000000110100101011101,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000100,
			es:  2,
		},
	},
	"10:nearZero": {
		a: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
		b: Posit{
			num: 0b01111111111111111111111111111111,
			es:  2,
		},
		exp: Posit{
			num: 0b00000000000000000000000000000001,
			es:  2,
		},
	},
}

func TestDiv(t *testing.T) {
	for name, test := range divtests {
		t.Run(name, func(t *testing.T) {
			res := test.a.DivSameES(test.b)
			t.Logf("exp:%#032b", test.exp.num)
			t.Logf("res:%#032b", res.num)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				b := NewPosit32es2FromBits(test.b.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res := a.Div(b)
				t.Logf("res:%#032b", res.num)
				if res != exp {
					t.Error("got the wrong number:", res.Float(), "expected:", exp.Float())
				}
			}
		})
	}
}

var sqrttests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000011010100000100111100110011,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b01010000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
	},
	"3:add-0": {
		a: Posit{
			num: 0b01110110100100110101001011011101,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01100110011010110101100011011110,
			es:  2,
		},
	},
	"4:negative": {
		a: Posit{
			num: 0b11001011100010001011101001110010,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10000000000000000000000000000000,
			es:  2,
		},
	},
	"5": {
		a: Posit{
			num: 0b01110111001101011101101000111001,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01100111001010101100000000000000,
			es:  2,
		},
	},
	"6:eaxpvsm": {
		a: Posit{
			num: 0b01101110010011110001011000111101,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01011110001101010110101001011111,
			es:  2,
		},
	},
	"7:wrongshift": {
		a: Posit{
			num: 0b01011001011001100010111010100000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001100010000110110000111100110,
			es:  2,
		},
	},
	"8:large": {
		a: Posit{
			num: 0b00000000000000000000000000001001,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000011000111001100010,
			es:  2,
		},
	},
}

func TestSqrt(t *testing.T) {
	for name, test := range sqrttests {
		t.Run(name, func(t *testing.T) {
			res := test.a.SqrtSameES()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Sqrt()
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, false, t)
		})
	}
}

var ceiltests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b01111110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111110110101011100000110110110,
			es:  2,
		},
	},
	"2:roundup": {
		a: Posit{
			num: 0b01011110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01011111000000000000000000000000,
			es:  2,
		},
	},
	"3:roundup2": {
		a: Posit{
			num: 0b00111110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
	},
	"4:negative": {
		a: Posit{
			num: 0b11011110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"5:negative": {
		a: Posit{
			num: 0b10011110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10011111000000000000000000000000,
			es:  2,
		},
	},
}

func TestCeil(t *testing.T) {
	for name, test := range ceiltests {
		t.Run(name, func(t *testing.T) {
			res := test.a.CeilSameES()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Ceil()
				if res2 != exp {
					t.Error("got the wrong number:", res.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, false, t)
		})
	}
}

var trunctests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b01111110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111110110101011100000110110110,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b00111110110101011100000110110110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
}

func TestTrunc(t *testing.T) {
	for name, test := range trunctests {
		t.Run(name, func(t *testing.T) {
			res := test.a.TruncateSameES()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 := a.Truncate()
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, false, t)
		})
	}
}

var roundtests = map[string]opTest{
	"1:mask": {
		a: Posit{
			num: 0b01100011101001001011011111100010,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01100011110000000000000000000000,
			es:  2,
		},
	},
	"2:roundeven": {
		a: Posit{
			num: 0b01111101011010011100000011010000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111101011010011100000011000000,
			es:  2,
		},
	},
	"3:scale": {
		a: Posit{
			num: 0b01111110110110011100111101011111,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111110110110011100111101011111,
			es:  2,
		},
	},
	"4:roundeven1": {
		a: Posit{
			num: 0b01111110010111001110111011111111,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111110010111001110111100000000,
			es:  2,
		},
	},
	"5:roundeven2": {
		a: Posit{
			num: 0b01111110010110111110101000101101,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111110010110111110101000101100,
			es:  2,
		},
	},
	"6:roundeven3": {
		a: Posit{
			num: 0b01000101010010010011000011101100,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
	},
	"7:round0": {
		a: Posit{
			num: 0b11001000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"8": {
		a: Posit{
			num: 0b10000000000000000000000000000010,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10000000000000000000000000000010,
			es:  2,
		},
	},
	"9": {
		a: Posit{
			num: 0b01111111111111111111111111111100,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111111111111111111111111111100,
			es:  2,
		},
	},
	"10:-1.5": {
		a: Posit{
			num: 0b10111100000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10111000000000000000000000000000,
			es:  2,
		},
	},
}

func TestRound(t *testing.T) {
	for name, test := range roundtests {
		t.Run(name, func(t *testing.T) {
			res := test.a.RoundSameES()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 := a.Round()
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, false, t)
		})
	}
}

var exptests = map[string]opTest{
	"1": {
		a: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001010110111111000010101000110,
			es:  2,
		},
	},
	"2": {
		a: Posit{
			num: 0b00111000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000101001100001001010011000111,
			es:  2,
		},
	},
	"3": {
		a: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01010110110001110011001001011100,
			es:  2,
		},
	},
	"4": {
		a: Posit{
			num: 0b01001001001100110011001100110011,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01011001111110010110010000000101,
			es:  2,
		},
	},
	"5:BIG": {
		a: Posit{
			num: 0b01111010100000100110001111111011,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111111111111111111111111111111,
			es:  2,
		},
	},
	"6": {
		a: Posit{
			num: 0b01100101100110000110000100010011,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111111111111111100001111110001,
			es:  2,
		},
	},
	"7:veryBig": {
		a: Posit{
			num: 0b01111110000010101001100011101000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111111111111111111111111111111,
			es:  2,
		},
	},
	"8:nearZero": {
		a: Posit{
			num: 0b10000001111101010110011100011000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"9:big-1": {
		a: Posit{
			num: 0b01101000100110110010010001000110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01111111111111111111111111110100,
			es:  2,
		},
	},
	"10:5": {
		a: Posit{
			num: 0b01010010000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01101100101000110100111000100110,
			es:  2,
		},
	},
	"11:0": {
		a: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
	},
	"12:small": {
		a: Posit{
			num: 0b00110000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000010010001011010111100011110,
			es:  2,
		},
	},
	"13:tiny-error": {
		a: Posit{
			num: 0b00011110100010101100111100010011,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000011010110110010110010010,
			es:  2,
		},
	},
	"14:tiny-error2": {
		a: Posit{
			num: 0b00110000000001001011001100000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000010010001110011000101100110,
			es:  2,
		},
	},
	"15:round0": {
		a: Posit{
			num: 0b00010111101011100100011011010001,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000111101111010001100110,
			es:  2,
		},
	},
	"16:exp0": {
		a: Posit{
			num: 0b01001001011000110101001111110111,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01011010011101000100001111010101,
			es:  2,
		},
	},
	"17:exp1": {
		a: Posit{
			num: 0b01001001011010111000010100011110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01011010100010011100001011001110,
			es:  2,
		},
	},
	"18:exp2": {
		a: Posit{
			num: 0b00000100101101000010111111111110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000000000000110110100010,
			es:  2,
		},
	},
	"19:exp3": {
		a: Posit{
			num: 0b00000100101101000010111010110101,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000000000000110110100010,
			es:  2,
		},
	},
	"20:exp4": {
		a: Posit{
			num: 0b01010000000000000000000001111110,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01100110110100110010010110111001,
			es:  2,
		},
	},
	"21:exp5": {
		a: Posit{
			num: 0b00100100001001010010110111010000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000110010111101011101101011,
			es:  2,
		},
	},
}

func TestExp(t *testing.T) {
	for name, test := range exptests {
		t.Run(name, func(t *testing.T) {
			res := test.a.ExpSameES()
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = a.Exp()
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printOpTest32(test, res, res2, false, t)
		})
	}
}

var log2tests = map[string]opTest{
	"1:1": {
		a: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"2:2": {
		a: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
	},
	"3:0.5": {
		a: Posit{
			num: 0b00111000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b11000000000000000000000000000000,
			es:  2,
		},
	},
	"4:5": {
		a: Posit{
			num: 0b01010010000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001001010010011010011110000101,
			es:  2,
		},
	},
	"5:10": {
		a: Posit{
			num: 0b01011010000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b01001101010010011010011110000101,
			es:  2,
		},
	},
	"6:0.7": {
		a: Posit{
			num: 0b00111011001100110011001100110011,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b11000111110001000100111011101011,
			es:  2,
		},
	},
	"7:0.2": {
		a: Posit{
			num: 0b00101100110011001100110011001101,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10110110101101100101100001111011,
			es:  2,
		},
	},
	"7:0.25": {
		a: Posit{
			num: 0b110000000000000000000000000000,
			es:  2,
		},
		b: Posit{},
		exp: Posit{
			num: 0b10111000000000000000000000000000,
			es:  2,
		},
	},
}

func TestLog2(t *testing.T) {
	for name, test := range log2tests {
		t.Run(name, func(t *testing.T) {
			res := test.a.Log2SameES()
			t.Logf("exp:%#032b", test.exp.num)
			t.Logf("res:%#032b", res.num)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			switch test.a.es {
			case 2:
				a := NewPosit32es2FromBits(test.a.num)
				exp := NewPosit32es2FromBits(test.exp.num)
				res := a.Log2()
				t.Logf("res32:%#032b", res.num)
				if res != exp {
					t.Error("got the wrong number:", res.Float(), "expected:", exp.Float())
				}
			}
		})
	}
}

type componentTest struct {
	neg   bool
	scale int
	frac  uint32
	es    uint8
	exp   Posit
}

var componenttests = map[string]componentTest{
	"1": {
		neg:   false,
		scale: -500,
		frac:  0,
		es:    2,
		exp: Posit{
			num: 0b00000000000000000000000000000000,
			es:  2,
		},
	},
	"2": {
		neg:   false,
		scale: 1,
		frac:  0,
		es:    2,
		exp: Posit{
			num: 0b01001000000000000000000000000000,
			es:  2,
		},
	},
	"3": {
		neg:   false,
		scale: 0,
		frac:  0,
		es:    2,
		exp: Posit{
			num: 0b01000000000000000000000000000000,
			es:  2,
		},
	},
	"4": {
		neg:   false,
		scale: 0,
		frac:  0b10101010100101010010101001010010,
		es:    2,
		exp: Posit{
			num: 0b01000101010101001010100101010010,
			es:  2,
		},
	},
}

func TestFromComponents(t *testing.T) {
	for name, test := range componenttests {
		t.Run(name, func(t *testing.T) {
			res := NewPosit32FromComponents(test.neg, test.scale, test.frac, test.es)
			t.Logf("exp:%#032b", test.exp.num)
			t.Logf("res:%#032b", res.num)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			switch test.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res := NewPosit32ES2FromComponents(test.neg, test.scale, test.frac)
				t.Logf("res32:%#032b", res.num)
				if res != exp {
					t.Error("got the wrong number:", res.Float(), "expected:", exp.Float())
				}
			}
		})
	}
}

type intTest struct {
	int uint64
	exp Posit
}

var fromint32tests = map[string]intTest{
	"1": {
		int: 0b01101001000100101011001010011100,
		exp: Posit{
			num: 0b01111111101010100100010010101101,
			es:  2,
		},
	},
	"2:round-1": {
		int: 0b00001001100101001110000110100000,
		exp: Posit{
			num: 0b01111111011001100101001110000110,
			es:  2,
		},
	},
	"3:maskshift-1": {
		int: 0b00000000000000000000000000000110,
		exp: Posit{
			num: 0b01010100000000000000000000000000,
			es:  2,
		},
	},
}

func TestFromint32(t *testing.T) {
	for name, test := range fromint32tests {
		t.Run(name, func(t *testing.T) {
			res := NewPosit32FromInt32(int32(test.int), 2)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.exp.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = NewPosit32ES2FromInt32(int32(test.int))
				if res2 != exp {
					t.Error("got the wrong number:", res2.Float(), "expected:", exp.Float())
				}
			}
			printFromIntTest32(test, res, res2, t)
		})
	}
}

var fromuint64tests = map[string]intTest{
	"1": {
		int: 0b1110001100010111100010111110010111111101111010001010001000101110,
		exp: Posit{
			num: 0b01111111111111111011110001100011,
			es:  2,
		},
	},
}

func TestFromuint64(t *testing.T) {
	for name, test := range fromuint64tests {
		t.Run(name, func(t *testing.T) {
			res := NewPosit32FromUint64(test.int, 2)
			if res != test.exp {
				t.Error("got the wrong number:", res.Float(), "expected:", test.exp.Float())
			}
			var res2 Posit32es2
			switch test.exp.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = NewPosit32ES2FromUint64(test.int)
				if res2 != exp {
					t.Error("got the wrong number:", res.Float(), "expected:", exp.Float())
				}
			}
			printFromIntTest32(test, res, res2, t)
		})
	}
}

var toint64tests = map[string]intTest{
	"1": {
		int: 0b00000000000000000000000000000010,
		exp: Posit{
			num: 0b01000101010110001110011110001011,
			es:  2,
		},
	},
	"2:rounding-0": {
		int: 0b0000000000000000000000000000000000000000001111101110001101101000,
		exp: Posit{
			num: 0b01111110011111011100011011010001,
			es:  2,
		},
	},
	"3:rounding-1": {
		int: 0b0000000000000000000000000000000000000000001011001111110010101000,
		exp: Posit{
			num: 0b01111110010110011111100101001111,
			es:  2,
		},
	},
	"4:overflow-0": {
		int: 0b0111111111111111111111111111111111111111111111111111111111111111,
		exp: Posit{
			num: 0b01111111111111111111011000110000,
			es:  2,
		},
	},
	"5:overflow-1": {
		int: 0b1000000000000000000000000000000000000000000000000000000000000000,
		exp: Posit{
			num: 0b10000000000000000100001010000000,
			es:  2,
		},
	},
}

func TestToInt64(t *testing.T) {
	for name, test := range toint64tests {
		t.Run(name, func(t *testing.T) {
			res := test.exp.Int64()
			if res != int64(test.int) {
				t.Error("got the wrong number:", res, "expected:", int64(test.int))
			}
			var res2 int64
			switch test.exp.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = exp.Int64()
				if res2 != int64(test.int) {
					t.Error("got the wrong number:", res2, "expected:", exp)
				}
			}
			printToIntTest32(test, uint64(res), uint64(res2), true, t)
		})
	}
}

var touint64tests = map[string]intTest{
	"1:overflow-1": {
		int: 0b1111000011100000000000000000000000000000000000000000000000000000,
		exp: Posit{
			num: 0b01111111111111111011111000011100,
			es:  2,
		},
	},
}

func TestToUint64(t *testing.T) {
	for name, test := range touint64tests {
		t.Run(name, func(t *testing.T) {
			res := test.exp.Uint64()
			if res != test.int {
				t.Error("got the wrong number:", res, "expected:", test.int)
			}
			var res2 uint64
			switch test.exp.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = exp.Uint64()
				if res2 != test.int {
					t.Error("got the wrong number:", res2, "expected:", test.int)
				}
			}
			printToIntTest32(test, res, res2, false, t)
		})
	}
}

var toint32tests = map[string]intTest{
	"1": {
		int: 0b11111111111111111111111111110010,
		exp: Posit{
			num: 0b10100001110100111100011100001101,
			es:  2,
		},
	},
	"2": {
		int: 0b10011100001110101110010000000000,
		exp: Posit{
			num: 0b10000000010101110000111010111001,
			es:  2,
		},
	},
}

func TestToInt32(t *testing.T) {
	for name, test := range toint32tests {
		t.Run(name, func(t *testing.T) {
			res := test.exp.Int32()
			if res != int32(test.int) {
				t.Error("got the wrong number:", res, "expected:", int32(test.int))
			}

			var res2 int32
			switch test.exp.es {
			case 2:
				exp := NewPosit32es2FromBits(test.exp.num)
				res2 = exp.Int32()
				if res2 != int32(test.int) {
					t.Error("got the wrong number:", res2, "expected:", int32(test.int))
				}
			}
			printToIntTest32(test, uint64(res), uint64(res2), true, t)
		})
	}
}

func printBoolTest32(test boolTest, res bool, res2 bool, t *testing.T) {
	t.Logf("srca:%#032b/%f", test.a.num, test.a.Float())
	t.Logf("srcb:%#032b/%f", test.b.num, test.b.Float())
	t.Logf("exp:%t", test.exp)
	t.Logf("res:%t", res)
	if test.a.es == 2 {
		t.Logf("res_2:%t", res2)
	}
}

func printOpTest32(test opTest, res Posit, res2 Posit32es2, useb bool, t *testing.T) {
	t.Logf("srca:%#032b/%f", test.a.num, test.a.Float())
	if useb {
		t.Logf("srcb:%#032b/%f", test.b.num, test.b.Float())
	}
	t.Logf("exp:%#032b/%f", test.exp.num, test.exp.Float())
	t.Logf("res:%#032b/%f", res.num, res.Float())
	if test.a.es == 2 {
		t.Logf("res_2:%#032b/%f", res2.num, res2.Float())
	}
}

func printFromIntTest32(test intTest, res Posit, res2 Posit32es2, t *testing.T) {
	t.Logf("src:%d", test.int)
	t.Logf("exp:%#032b/%f", test.exp.num, test.exp.Float())
	t.Logf("res:%#032b/%f", res.num, res.Float())
	if test.exp.es == 2 {
		t.Logf("res_2:%#032b/%f", res2.num, res2.Float())
	}
}

func printToIntTest32(test intTest, res uint64, res2 uint64, signed bool, t *testing.T) {
	t.Logf("src:%#032b/%f", test.exp.num, test.exp.Float())
	if signed {
		t.Logf("exp:%d", int64(test.int))
		t.Logf("res:%d", int64(res))
	} else {
		t.Logf("exp:%d", test.int)
		t.Logf("res:%d", res)
	}
	if test.exp.es == 2 {
		t.Logf("res_2:%d", res2)
	}
}
