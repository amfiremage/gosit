package main

import (
	"fmt"
	"image/color"
	"math"

	posit "gitlab.com/jaicewizard/gosit"
	goplot "gonum.org/v1/plot"
	goplotter "gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

func expPlotGraph() {
	plot := goplot.New()
	plotter := goplotter.NewFunction(func(x float64) float64 {
		return math.Exp(x)
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(6)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return posit.NewPosit32FromFloat(x, 2).ExpSameES().Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 0, A: 255}
	plotter.Width = vg.Points(3)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 (thinner to make float64 visible)", thumbnailer{color.RGBA{R: 0, G: 255, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return posit.NewPosit16ES1FromFloat(x).Exp().Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 (thinner to make posit32 visible)", thumbnailer{color.RGBA{R: 0, G: 0, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 10
	plot.Y.Min = 1
	plot.Y.Max = 23000

	fmt.Println(plot.Save(400, 400, "exp_plot.svg"))
}

func expPlotSmall32Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := px.ExpSameES()
		o := math.Exp(x)
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := px.ExpSameES()
		o := math.Exp(x)
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Exp(float64(x32)))
		o := math.Exp(x)
		return math.Abs(o-float64(o32)) * 100 / o
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(0.05)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 5
	plot.Y.Min = 0
	plot.Y.Max = 0.00003

	fmt.Println(plot.Save(400, 400, "exp_small_error_32_plot.svg"))
}

func expPlotSmall16Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := px.Exp()
		o := math.Exp(x)
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := px.Exp()
		o := math.Exp(x)
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 5
	plot.Y.Min = 0
	plot.Y.Max = 0.5

	fmt.Println(plot.Save(400, 400, "exp_small_error_16_plot.svg"))
}

func expPlotBig32Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := px.ExpSameES()
		o := math.Exp(x)
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64 (off the chats high, think 10^6)", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := px.ExpSameES()
		o := math.Exp(x)
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Exp(float64(x32)))
		o := math.Exp(x)
		return math.Abs(o-float64(o32)) * 100 / o
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(0.05)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 0
	plot.Y.Max = 0.0005

	fmt.Println(plot.Save(400, 400, "exp_big_error_32_plot.svg"))
}

func expPlotBig16Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := px.Exp()
		o := math.Exp(x)
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64 (off the charts)", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := px.Exp()
		o := math.Exp(x)
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 0
	plot.Y.Max = 150

	fmt.Println(plot.Save(400, 400, "exp_big_error_16_plot.svg"))
}
