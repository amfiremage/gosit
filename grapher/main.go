package main

import (
	"image/color"

	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
)

const samples = 10000

type thumbnailer struct {
	color.Color
}

// Thumbnail fulfills the plot.Thumbnailer interface.
func (t thumbnailer) Thumbnail(c *draw.Canvas) {
	pts := []vg.Point{
		{X: c.Min.X, Y: c.Min.Y},
		{X: c.Min.X, Y: c.Max.Y},
		{X: c.Max.X, Y: c.Max.Y},
		{X: c.Max.X, Y: c.Min.Y},
	}
	poly := c.ClipPolygonY(pts)
	c.FillPolygon(t.Color, poly)

	pts = append(pts, vg.Point{X: c.Min.X, Y: c.Min.Y})
	outline := c.ClipLinesY(pts)
	c.StrokeLines(draw.LineStyle{
		Color: color.Black,
		Width: vg.Points(1),
	}, outline...)
}
func main() {
	expPlotGraph()
	expPlotSmall32Error()
	expPlotBig32Error()
	expPlotSmall16Error()
	expPlotBig16Error()

	logPlotGraph()
	logPlotSmall32Error()
	logPlotBig32Error()
	logPlotSmall16Error()
	logPlotBig16Error()

	powPlotGraph()
	powPlotSmall32Error()
	powPlotBig32Error()
	powPlotSmall16Error()
	powPlotBig16Error()
}
