package main

import (
	"fmt"
	"image/color"
	"math"

	Posit "gitlab.com/jaicewizard/gosit"
	goplot "gonum.org/v1/plot"
	goplotter "gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

func logPlotGraph() {
	plot := goplot.New()
	plotter := goplotter.NewFunction(func(x float64) float64 {
		return math.Log2(x)
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(6)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return Posit.NewPosit32FromFloat(x, 2).Log2SameES().Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 0, A: 255}
	plotter.Width = vg.Points(2)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 (thinner to make float64 visible)", thumbnailer{color.RGBA{R: 0, G: 255, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return Posit.NewPosit16ES1FromFloat(x).Log2().Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 (thinner to make posit32 visible)", thumbnailer{color.RGBA{R: 0, G: 0, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0 + math.Nextafter(0, 1)
	plot.X.Max = 10
	plot.Y.Min = -9
	plot.Y.Max = 5

	fmt.Println(plot.Save(400, 400, "log_plot.svg"))
}

func logPlotSmall32Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit32FromFloat(x, 2)
		po := px.Log2SameES()
		o := math.Log2(x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64 (tiny line at the bottom)", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit32FromFloat(x, 2)
		po := px.Log2SameES()
		o := math.Log2(x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs((o - po.Float()) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Log2(float64(x32)))
		o := math.Log2(x)
		if o == 0.0 {
			if o32 == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(float64(o32), 0) {
			return 0.0
		}
		return math.Abs((o - float64(o32)) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(0.05)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 10
	plot.Y.Min = 0
	plot.Y.Max = 0.00001

	fmt.Println(plot.Save(400, 400, "log_small_error_32_plot.svg"))
}

func logPlotSmall16Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit16ES1FromFloat(x)
		po := px.Log2()
		o := math.Log2(x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit16ES1FromFloat(x)
		po := px.Log2()
		o := math.Log2(x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 5
	plot.Y.Min = 0
	plot.Y.Max = 0.5

	fmt.Println(plot.Save(400, 400, "log_small_error_16_plot.svg"))
}

func logPlotBig32Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit32FromFloat(x, 2)
		po := px.Log2SameES()
		o := math.Log2(x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit32FromFloat(x, 2)
		po := px.Log2SameES()
		o := math.Log2(x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs((o - po.Float()) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Log2(float64(x32)))
		o := math.Log2(x)
		if o == 0.0 {
			if o32 == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(float64(o32), 0) {
			return 0.0
		}
		return math.Abs((o - float64(o32)) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(0.05)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 0
	plot.Y.Max = 0.00001

	fmt.Println(plot.Save(400, 400, "log_big_error_32_plot.svg"))
}

func logPlotBig16Error() {
	plot := goplot.New()

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit16ES1FromFloat(x)
		po := px.Log2()
		o := math.Log2(x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.5)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := Posit.NewPosit16ES1FromFloat(x)
		po := px.Log2()
		o := math.Log2(x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(0.2)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 0
	plot.Y.Max = 0.05

	fmt.Println(plot.Save(400, 400, "log_big_error_16_plot.svg"))
}
