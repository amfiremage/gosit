package main

import (
	"fmt"
	"image/color"
	"math"

	posit "gitlab.com/jaicewizard/gosit"
	goplot "gonum.org/v1/plot"
	goplotter "gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

var (
	base32 = posit.NewPosit32FromFloat(20, 2)
	base16 = posit.NewPosit16ES1FromFloat(20)
)

func powPlotGraph() {
	plot := goplot.New()
	plot.Title.Text = "pow(20, x)"

	plotter := goplotter.NewFunction(func(x float64) float64 {
		return math.Pow(20, x)
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(6)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return base32.PowSameES(posit.NewPosit32FromFloat(x, 2)).Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 0, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 (thinner to make float64 visible)", thumbnailer{color.RGBA{R: 0, G: 255, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		return base16.Pow(posit.NewPosit16ES1FromFloat(x)).Float()
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 (thinner to make posit32 visible)", thumbnailer{color.RGBA{R: 0, G: 0, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 10
	plot.Y.Min = -9
	plot.Y.Max = 5

	fmt.Println(plot.Save(400, 400, "pow(20)_plot.svg"))
}

func powPlotSmall32Error() {
	plot := goplot.New()
	plot.Title.Text = "pow(20, x)"

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := base32.PowSameES(px)
		o := math.Pow(20, x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64 (tiny line at the bottom)", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := base32.PowSameES(px)
		o := math.Pow(20, x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs((o - po.Float()) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Pow(20, float64(x32)))
		o := math.Pow(20, x)
		if o == 0.0 {
			if o32 == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(float64(o32), 0) {
			return 0.0
		}
		return math.Abs((o - float64(o32)) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(0.05)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 10
	plot.Y.Min = 0
	plot.Y.Max = 0.0005

	fmt.Println(plot.Save(400, 400, "pow(20)_small_error_32_plot.svg"))
}

func powPlotSmall16Error() {
	plot := goplot.New()
	plot.Title.Text = "pow(20, x)"

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := base16.Pow(px)
		o := math.Pow(20, x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 255, A: 255}
	plotter.Width = vg.Points(0.05)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64", thumbnailer{color.RGBA{R: 255, G: 0, B: 255, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := base16.Pow(px)
		o := math.Pow(20, x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(0.1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 0
	plot.X.Max = 5
	plot.Y.Min = 0
	plot.Y.Max = 3

	fmt.Println(plot.Save(400, 400, "pow(20)_small_error_16_plot.svg"))
}

func powPlotBig32Error() {
	plot := goplot.New()
	plot.Title.Text = "pow(20, x)"

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := base32.PowSameES(px)
		o := math.Pow(20, x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(5)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 absolute error compared to float64 (off the charts)", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit32FromFloat(x, 2)
		po := base32.PowSameES(px)
		o := math.Pow(20, x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs((o - po.Float()) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 0, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(1)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit32 relative error compared to float64 ", thumbnailer{color.RGBA{R: 0, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		x32 := float32(x)
		o32 := float32(math.Pow(20, float64(x32)))
		o := math.Pow(20, x)
		if o == 0.0 {
			if o32 == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(float64(o32), 0) {
			return 0.0
		}
		if math.IsInf(o, 0) || math.IsInf(float64(o32), 0) {
			return 20
		}
		return math.Abs((o - float64(o32)) * 100 / o)
	})
	plotter.Color = color.RGBA{R: 255, G: 255, B: 0, A: 255}
	plotter.Samples = samples
	plotter.Width = vg.Points(1)
	plot.Add(plotter)
	plot.Legend.Add("float32 relative error compared to float64", thumbnailer{color.RGBA{R: 255, G: 255, B: 0, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 00
	plot.Y.Max = 20

	fmt.Println(plot.Save(400, 400, "pow(20)_big_error_32_plot.svg"))
}

func powPlotBig16Error() {
	plot := goplot.New()
	plot.Title.Text = "pow(20, x)"

	plotter := goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := base16.Pow(px)
		o := math.Pow(20, x)
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o - po.Float())
	})
	plotter.Color = color.RGBA{R: 255, G: 0, B: 0, A: 255}
	plotter.Width = vg.Points(0.5)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 absolute error compared to float64 (off the charts)", thumbnailer{color.RGBA{R: 255, G: 0, B: 0, A: 255}})

	plotter = goplotter.NewFunction(func(x float64) float64 {
		px := posit.NewPosit16ES1FromFloat(x)
		po := base16.Pow(px)
		o := math.Pow(20, x)
		if o == 0.0 {
			if po.Float() == 0.0 {
				return 0.0
			} else {
				return 1.0
			}
		}
		if math.IsInf(o, 0) && math.IsInf(po.Float(), 0) {
			return 0.0
		}
		return math.Abs(o-po.Float()) * 100 / o
	})
	plotter.Color = color.RGBA{R: 0, G: 255, B: 255, A: 255}
	plotter.Width = vg.Points(2)
	plotter.Samples = samples
	plot.Add(plotter)
	plot.Legend.Add("posit16 relative error compared to float64", thumbnailer{color.RGBA{R: 0, G: 255, B: 255, A: 255}})

	plot.Legend.Top = true
	plot.Legend.XOffs = -vg.Centimeter
	plot.Legend.YOffs = -vg.Centimeter

	plot.X.Min = 20
	plot.X.Max = 30
	plot.Y.Min = 0
	plot.Y.Max = 100

	fmt.Println(plot.Save(400, 400, "pow(20)_big_error_16_plot.svg"))
}
