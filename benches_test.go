package posit

import (
	"math/rand"
	"testing"

	"github.com/cjdelisle/goposit"
)

type opBenchCase struct {
	a    Posit
	b    Posit
	ag   goposit.Posit32
	bg   goposit.Posit32
	ac32 Posit32es2
	bc32 Posit32es2
	ac16 Posit16es1
	bc16 Posit16es1
}

var slowBenchcases = [...]opBenchCase{
	{
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b0_00001_10_11011101_00000000_00000000),
		bg: goposit.NewPosit32().SetBits(0b0_00001_10_11011101_00000000_00000000),
		ac32: Posit32es2{
			num: 0b0_00001_10_11011101_00000000_00000000,
		},
		bc32: Posit32es2{
			num: 0b0_00001_10_11011101_00000000_00000000,
		},
		ac16: Posit16es1{
			num: 0b0_00001_10_11011101,
		},
		bc16: Posit16es1{
			num: 0b0_00001_10_11011101,
		},
	},
	{
		a: Posit{
			num: 0b0_00001_10_11011101_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b0_00001_10_11011101_00000000_00000000),
		bg: goposit.NewPosit32().SetBits(0b0_00001_10_11111101_00000000_00000000),
		ac32: Posit32es2{
			num: 0b0_00001_10_11011101_00000000_00000000,
		},
		bc32: Posit32es2{
			num: 0b0_00001_10_11111101_00000000_00000000,
		},
		ac16: Posit16es1{
			num: 0b0_00001_10_11011101,
		},
		bc16: Posit16es1{
			num: 0b0_00001_10_11111101,
		},
	},
	{
		a: Posit{
			num: 0b1_11110_10_00100000_00000000_00000000,
			es:  2,
		},
		b: Posit{
			num: 0b0_00001_10_11111101_00000000_00000000,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b1_11110_10_00100000_00000000_00000000),
		bg: goposit.NewPosit32().SetBits(0b0_00001_10_11111101_00000000_00000000),
		ac32: Posit32es2{
			num: 0b1_11110_10_00100000_00000000_00000000,
		},
		bc32: Posit32es2{
			num: 0b0_00001_10_11111101_00000000_00000000,
		},
		ac16: Posit16es1{
			num: 0b1_11110_10_00100000,
		},
		bc16: Posit16es1{
			num: 0b0_00001_10_11011101,
		},
	},
	{
		a: Posit{
			num: 0b01000101100011011010110001101011,
			es:  2,
		},
		b: Posit{
			num: 0b10000100111000010000001001010100,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b01000101100011011010110001101011),
		bg: goposit.NewPosit32().SetBits(0b10000100111000010000001001010100),
		ac32: Posit32es2{
			num: 0b01000101100011011010110001101011,
		},
		bc32: Posit32es2{
			num: 0b10000100111000010000001001010100,
		},
		ac16: Posit16es1{
			num: 0b0100010110001101,
		},
		bc16: Posit16es1{
			num: 0b1000010011100001,
		},
	},
	{
		a: Posit{
			num: 0b11001010100000010111011101000100,
			es:  2,
		},
		b: Posit{
			num: 0b11100010010101101111000001110010,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b11001010100000010111011101000100),
		bg: goposit.NewPosit32().SetBits(0b11100010010101101111000001110010),
		ac32: Posit32es2{
			num: 0b11001010100000010111011101000100,
		},
		bc32: Posit32es2{
			num: 0b11100010010101101111000001110010,
		},
		ac16: Posit16es1{
			num: 0b1100101010000001,
		},
		bc16: Posit16es1{
			num: 0b1110001001010110,
		},
	},
	{
		a: Posit{
			num: 0b10110001010101010011110101100011,
			es:  2,
		},
		b: Posit{
			num: 0b01000000010010000100010011001001,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b10110001010101010011110101100011),
		bg: goposit.NewPosit32().SetBits(0b01000000010010000100010011001001),
		ac32: Posit32es2{
			num: 0b10110001010101010011110101100011,
		},
		bc32: Posit32es2{
			num: 0b01000000010010000100010011001001,
		},
		ac16: Posit16es1{
			num: 0b1011000101010101,
		},
		bc16: Posit16es1{
			num: 0b0100000001001000,
		},
	},
	{
		a: Posit{
			num: 0b00001010100110011011111111110101,
			es:  2,
		},
		b: Posit{
			num: 0b10011000110111111000101011101110,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b00001010100110011011111111110101),
		bg: goposit.NewPosit32().SetBits(0b10011000110111111000101011101110),
		ac32: Posit32es2{
			num: 0b00001010100110011011111111110101,
		},
		bc32: Posit32es2{
			num: 0b10011000110111111000101011101110,
		},
		ac16: Posit16es1{
			num: 0b0000101010011001,
		},
		bc16: Posit16es1{
			num: 0b1001100011011111,
		},
	},
	{
		a: Posit{
			num: 0b01011111100111100110110010101001,
			es:  2,
		},
		b: Posit{
			num: 0b01010101011001000101111011111011,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b01011111100111100110110010101001),
		bg: goposit.NewPosit32().SetBits(0b01010101011001000101111011111011),
		ac32: Posit32es2{
			num: 0b01011111100111100110110010101001,
		},
		bc32: Posit32es2{
			num: 0b01010101011001000101111011111011,
		},
		ac16: Posit16es1{
			num: 0b0101111110011110,
		},
		bc16: Posit16es1{
			num: 0b0101010101100100,
		},
	},
	{
		a: Posit{
			num: 0b11001110011111001011111010111100,
			es:  2,
		},
		b: Posit{
			num: 0b01110110011100110110110110010110,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b11001110011111001011111010111100),
		bg: goposit.NewPosit32().SetBits(0b01110110011100110110110110010110),
		ac32: Posit32es2{
			num: 0b11001110011111001011111010111100,
		},
		bc32: Posit32es2{
			num: 0b01110110011100110110110110010110,
		},
		ac16: Posit16es1{
			num: 0b1100111001111100,
		},
		bc16: Posit16es1{
			num: 0b0111011001110011,
		},
	},
	{
		a: Posit{
			num: 0b01111111111101010001101111000111,
			es:  2,
		},
		b: Posit{
			num: 0b01100001110010010010111000000100,
			es:  2,
		},
		ag: goposit.NewPosit32().SetBits(0b01111111111101010001101111000111),
		bg: goposit.NewPosit32().SetBits(0b01100001110010010010111000000100),
		ac32: Posit32es2{
			num: 0b01111111111101010001101111000111,
		},
		bc32: Posit32es2{
			num: 0b01100001110010010010111000000100,
		},
		ac16: Posit16es1{
			num: 0b0111111111110101,
		},
		bc16: Posit16es1{
			num: 0b0110000111001001,
		},
	},
}

const half = uint32(0b00111000000000000000000000000000)
const four = uint32(0b01010000000000000000000000000000)

// random distribution between 0 and 0.5
func genRandomTiny(n int) []opBenchCase {
	if n >= 1000000 {
		n = 1000000
	}
	var ret = make([]opBenchCase, n)
	for n--; n >= 0; n-- {
		for {
			scale := -rand.Intn(1000) - 1
			frac := rand.Uint64()
			ret[n].a = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].ac32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].ac16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].ac32.num <= half {
				break
			}
		}

		for {
			scale := -rand.Intn(1000) - 1
			frac := rand.Uint64()
			ret[n].b = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].bc32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].bc16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].bc32.num <= half {
				break
			}
		}
	}
	return ret
}

// random distribution between 0.5 and 4
func genRandomMedium(n int) []opBenchCase {
	if n >= 1000000 {
		n = 1000000
	}
	var ret = make([]opBenchCase, n)
	for n--; n >= 0; n-- {
		frac := rand.Uint64()
		for {
			scale := rand.Intn(4)
			if scale >= 3 {
				scale = -scale
			}
			ret[n].a = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].ac32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].ac16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].ac32.num > half && ret[n].ac32.num <= four {
				break
			}
		}

		frac = rand.Uint64()
		for {
			scale := rand.Intn(4)
			if scale >= 3 {
				scale = -scale
			}
			ret[n].b = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].bc32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].bc16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].bc32.num > half && ret[n].bc32.num <= four {
				break
			}
		}
	}
	return ret
}

// random distribution bigger than 4
func genRandomBig(n int) []opBenchCase {
	if n >= 1000000 {
		n = 1000000
	}
	var ret = make([]opBenchCase, n)
	for n--; n >= 0; n-- {
		frac := rand.Uint64()
		for {
			scale := rand.Intn(100)
			if scale >= 500 {
				scale = -scale
			}
			ret[n].a = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].ac32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].ac16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].ac32.num >= four {
				break
			}
		}

		frac = rand.Uint64()
		for {
			scale := rand.Intn(100)
			if scale >= 500 {
				scale = -scale
			}
			ret[n].b = NewPosit32FromComponents(false, scale, uint32(frac>>32), 2)
			ret[n].bc32 = NewPosit32ES2FromComponents(false, scale, uint32(frac>>32))
			ret[n].bc16 = NewPosit16ES1FromComponents(false, scale, uint16(frac>>48))
			if ret[n].bc32.num >= four {
				break
			}
		}
	}
	return ret
}

func BenchmarkAddSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.AddSameES(slowBenchcases[n%len(slowBenchcases)].b)
	}
}

func BenchmarkAdd32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Add(slowBenchcases[n%len(slowBenchcases)].bc32)
	}
}

func BenchmarkAdd16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Add(slowBenchcases[n%len(slowBenchcases)].bc16)
	}
}

func BenchmarkAddSlowGoposit(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ag.Add(slowBenchcases[n%len(slowBenchcases)].bg)
	}
}

func BenchmarkMulSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.MulSameES(slowBenchcases[n%len(slowBenchcases)].b)
	}
}

func BenchmarkMul32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Mul(slowBenchcases[n%len(slowBenchcases)].bc32)
	}
}

func BenchmarkMul16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Mul(slowBenchcases[n%len(slowBenchcases)].bc16)
	}
}

func BenchmarkMulSlowGoposit(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ag.Mul(slowBenchcases[n%len(slowBenchcases)].bg)
	}
}

func BenchmarkDivSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.DivSameES(slowBenchcases[n%len(slowBenchcases)].b)
	}
}

func BenchmarkDiv32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Div(slowBenchcases[n%len(slowBenchcases)].bc32)
	}
}

func BenchmarkDiv16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Div(slowBenchcases[n%len(slowBenchcases)].bc16)
	}
}

func BenchmarkDivSlowGoposit(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ag.Div(slowBenchcases[n%len(slowBenchcases)].bg)
	}
}

func BenchmarkSqrtSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		a := slowBenchcases[n%len(slowBenchcases)].a
		if a.num&(1<<31) != 0 {
			a.num = uint32(-int32(a.num))
		}
		a.SqrtSameES()
	}
}

func BenchmarkSqrt32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		a := slowBenchcases[n%len(slowBenchcases)].ac32
		if a.num&(1<<31) != 0 {
			a.num = uint32(-int32(a.num))
		}
		a.Sqrt()
	}
}

func BenchmarkSqrt16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		a := slowBenchcases[n%len(slowBenchcases)].ac16
		if a.num&(1<<15) != 0 {
			a.num = uint16(-int16(a.num))
		}
		a.Sqrt()
	}
}

func BenchmarkCeilSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.TruncateSameES()
	}
}

func BenchmarkCeil32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Truncate()
	}
}

func BenchmarkCeil16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Truncate()
	}
}

func BenchmarkTruncSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.TruncateSameES()
	}
}

func BenchmarkTrunc32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Truncate()
	}
}

func BenchmarkTrunc16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Truncate()
	}
}

func BenchmarkRoundSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].a.RoundSameES()
	}
}

func BenchmarkRound32ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac32.Round()
	}
}

func BenchmarkRound16ESConst(b *testing.B) {
	for n := 0; n < b.N; n++ {
		slowBenchcases[n%len(slowBenchcases)].ac16.Round()
	}
}

func BenchmarkStringSlow(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_ = slowBenchcases[n%len(slowBenchcases)].a.String()
	}
}

func BenchmarkExp32Tiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.ExpSameES()
	}
}

func BenchmarkExp32Medium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.ExpSameES()
	}
}

func BenchmarkExp32Big(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.ExpSameES()
	}
}

func BenchmarkExp32ConstESTiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Exp()
	}
}

func BenchmarkExp32ConstESMedium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Exp()
	}
}

func BenchmarkExp32ConstESBig(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Exp()
	}
}

func BenchmarkExp16ConstESTiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Exp()
	}
}

func BenchmarkExp16ConstESMedium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Exp()
	}
}

func BenchmarkExp16ConstESBig(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Exp()
	}
}

func BenchmarkLog232Tiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Log2SameES()
	}
}

func BenchmarkLog232Medium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Log2SameES()
	}
}
func BenchmarkLog232Big(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Log2SameES()
	}
}

func BenchmarkLog232ConstESTiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Log2()
	}
}

func BenchmarkLog232ConstESMedium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Log2()
	}
}

func BenchmarkLog232ConstESBig(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Log2()
	}
}

func BenchmarkLog216ConstESTiny(b *testing.B) {
	benches := genRandomTiny(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Log2()
	}
}

func BenchmarkLog216ConstESMedium(b *testing.B) {
	benches := genRandomMedium(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Log2()
	}
}
func BenchmarkLog216ConstESBig(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Log2()
	}
}

// FromInt benches
func BenchmarkFromInt3232Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32FromInt32(int32(benches[n%len(benches)].a.num), 2)
	}
}
func BenchmarkFromInt3232ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32ES2FromInt32(int32(benches[n%len(benches)].a.num))
	}
}
func BenchmarkFromInt3216ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromInt32(int32(benches[n%len(benches)].a.num))
	}
}
func BenchmarkFromUint3232Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32FromUint32(uint32(benches[n%len(benches)].a.num), 2)
	}
}
func BenchmarkFromUint3232ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32ES2FromUint32(uint32(benches[n%len(benches)].a.num))
	}
}
func BenchmarkFromUint3216ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromUint32(uint32(benches[n%len(benches)].a.num))
	}
}

func BenchmarkFromInt6432Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32FromInt64(int64(benches[n%len(benches)].a.num)<<32+int64(benches[n%len(benches)].b.num), 2)
	}
}
func BenchmarkFromInt6432ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32ES2FromInt64(int64(benches[n%len(benches)].a.num)<<32 + int64(benches[n%len(benches)].b.num))
	}
}
func BenchmarkFromInt6416ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromInt64(int64(benches[n%len(benches)].a.num)<<32 + int64(benches[n%len(benches)].b.num))
	}
}
func BenchmarkFromUint6432Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32FromUint64(uint64(benches[n%len(benches)].a.num)<<32+uint64(benches[n%len(benches)].b.num), 2)
	}
}
func BenchmarkFromUint6432ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit32ES2FromUint64(uint64(benches[n%len(benches)].a.num)<<32 + uint64(benches[n%len(benches)].b.num))
	}
}
func BenchmarkFromUint6416ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromUint64(uint64(benches[n%len(benches)].a.num)<<32 + uint64(benches[n%len(benches)].b.num))
	}
}

func BenchmarkFromInt1616ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromInt16(int16(benches[n%len(benches)].a.num))
	}
}
func BenchmarkFromUint1616ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		NewPosit16ES1FromUint16(uint16(benches[n%len(benches)].a.num))
	}
}

func BenchmarkToInt6432Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Int64()
	}
}

func BenchmarkToInt6432ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Int64()
	}
}

func BenchmarkToInt6416ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Int64()
	}
}

func BenchmarkToUint6432Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Uint64()
	}
}

func BenchmarkToUint6432ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Uint64()
	}
}

func BenchmarkToUint6416ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Uint64()
	}
}

func BenchmarkToInt3232Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Int32()
	}
}

func BenchmarkToInt3232ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Int32()
	}
}

func BenchmarkToInt3216ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Int32()
	}
}

func BenchmarkToUint3232Slow(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].a.Uint32()
	}
}

func BenchmarkToUint3232ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac32.Uint32()
	}
}

func BenchmarkToUint3216ConstES(b *testing.B) {
	benches := genRandomBig(b.N)
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		benches[n%len(benches)].ac16.Uint32()
	}
}
