//go:generate go run generate/main.go posit 32ES2 16ES1
package posit

import (
	"math"
	"math/bits"
	"strconv"
	"strings"
)

type Posit struct {
	num uint32
	es  uint8
}

func Newposit32FromBits(bits uint32, es uint8) Posit {
	if es > 31 {
		panic("ES cannot be more then 31, that doesnt even make sense")
	}
	return Posit{num: bits, es: es}
}

func (a Posit) Float() float64 {
	if a.num == 0 {
		return 0.0
	}
	if a.num == 1<<31 {
		return math.Inf(1)
	}

	neg := a.num>>31 != 0
	if neg {
		a.num = uint32(-int32(a.num))
	}
	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | 1<<31
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	frac := float64(afracP1) / float64(uint32(0b1<<(31)))
	if neg {
		return -(math.Pow(2.0, float64(ascale)) * frac)
	} else {
		return math.Pow(2.0, float64(ascale)) * frac
	}
}

func NewPosit32FromFloat(f float64, es uint8) Posit {
	if math.IsNaN(f) || math.IsInf(f, 0) {
		return Newposit32FromBits(1<<31, es)
	}

	bits := math.Float64bits(math.Abs(f))
	scale := int16((bits & 0x7fffffffffffffff) >> 52)
	combinedFrac := bits & 0x000fffffffffffff
	if scale == 0 {
		if combinedFrac == 0 {
			return Posit{
				num: 0,
				es:  es,
			}
		} else {
			combinedFrac >>= 1
			scale++
		}
	}
	scale -= 1023

	combinedFrac >>= 20
	endreg := scale >> es
	endexp := uint32(scale - (endreg << es))
	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 30 {
			outPosit = 1
			if math.Signbit(f) {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit{
				num: outPosit,
				es:  es,
			}
		}
		outPosit = (0b1 << 30) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 30 {
			outPosit = 0x7fffffff
			if math.Signbit(f) {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit{
				num: outPosit,
				es:  es,
			}
		}
		outPosit = 0x7fffffff - 0x7fffffff>>(outn&0x1f)
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 31 - es - outn - 1

	combinedFrac >>= (es + outn + 2)

	outPosit |= uint32(endexp) << outFracBits
	outPosit |= uint32(combinedFrac)

	if math.Signbit(f) {
		outPosit = uint32(-int32(outPosit))
	}
	return Posit{
		num: outPosit,
		es:  es,
	}
}

//This returns a posit that best represents neg*2^scale*(1+frac) in the given ES
func NewPosit32FromComponents(neg bool, scale int, frac uint32, es uint8) Posit {
	endreg := scale >> es
	endexp := uint32(scale - (endreg << es))

	var outPosit uint32
	var outn uint8

	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 31 {
			outPosit = 0
			return Posit{num: outPosit, es: es}
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 30 {
			outPosit = 0x7fffffff
			if neg {
				outPosit = uint32(-int32(outPosit))
			}
			return Posit{num: outPosit, es: es}
		}
		outPosit = 0x7fffffff - (0x7fffffff >> (outn & 0x1f))
	}

	//Recalculate the final fraction bits so that it matches the new exponent and m
	outFracBits := 31 - es - outn - 1

	frac >>= es + outn + 2

	outPosit |= uint32(endexp) << outFracBits
	outPosit |= uint32(frac)

	if neg {
		outPosit = uint32(-int32(outPosit))
	}

	return Posit{num: outPosit, es: es}
}

const smallsString = "00010203040506070809" +
	"10111213141516171819" +
	"20212223242526272829" +
	"30313233343536373839" +
	"40414243444546474849" +
	"50515253545556575859" +
	"60616263646566676869" +
	"70717273747576777879" +
	"80818283848586878889" +
	"90919293949596979899"

// String converts the posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit) String() string {
	return a.FormatScientific()
}

// FormatScientific converts the Posit into a scientific notation string
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit) FormatScientific() string {
	switch a.num {
	case 0:
		return "0"
	case 1 << 31:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	start := i - 1
	end := i - 1

	var s string
	if aneg {
		buf[i-2] = '-'
		start--
	}

	buf[i-1] = buf[i]

	if i+1 != len(buf) {
		last_non0 := 0
		for index, c := range buf[i+1:] {
			if c != '0' {
				last_non0 = index + 2
			}
		}
		buf[i] = '.'
		end = i - 1 + last_non0
	}
	s = string(buf[start : end+1])

	power := int64(ascaleB10) + int64(len(buf[i+1:]))
	if power == 1 {
		s += "*10"
	} else if power != 0 {
		s += "*10^" + strconv.FormatInt(int64(ascaleB10)+int64(len(buf[i+1:])), 10)
	}
	return s
}

// FormatBasic converts the posit into a string in basic numerical notation.
// Although this should give a reasonably accurate representation,
// innacuracies may occur.
func (a Posit) FormatBasic() string {
	switch a.num {
	case 0:
		return "0"
	case 1 << 31:
		return "NaN"
	}
	buf, ascaleB10, i, aneg := a.getDigits()
	var s string
	if aneg {
		s = "-"
	}
	switch {
	case int32(len(buf))+int32(ascaleB10) > int32(i):
		s += string(buf[i : len(buf)+int(ascaleB10)])
		if ascaleB10 != 0 {
			s += "."
			s += string(buf[len(buf)+int(ascaleB10):])
		}
	case int32(len(buf))+int32(ascaleB10) <= int32(i):
		s += "0."
		for int32(len(buf))+int32(ascaleB10) < int32(i) {
			s += "0"
			ascaleB10++
		}

		s += string(buf[i:])
	}
	s = strings.TrimRight(s, "0")
	s = strings.TrimRight(s, ".")
	return s
}

func (a Posit) getDigits() ([64]byte, int32, int, bool) {
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	var reg int8
	r1 := uint32(2)
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}
	sftnum <<= 2
	afracP1 := uint64((sftnum << (a.es & 0x1f)) | (1 << 31))
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascaleB2 := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	afracP1 >>= r1
	afracP1 >>= uint32(a.es)

	ascaleB10 := ascaleB2 - (31 - int32(a.es) - int32(r1))
	ascaleB5 := -ascaleB10
	if ascaleB5 < 0 {
		if ascaleB5 < -32 {
			ascaleB5 += 32
			afracP1 <<= 32
		} else {
			afracP1 <<= -ascaleB5
			ascaleB5 = 0
		}
		ascaleB10 = 0
	}

	//we need to have at least 3 bits clean for a non-rounded multiplication by 5
	for ascaleB5 > 0 && afracP1>>(61) == 0 {
		afracP1 *= 5
		ascaleB5--
	}

	var buf [64]byte
	i := len(buf)

	for afracP1 >= 100 {
		is := afracP1 % 100 * 2
		afracP1 /= 100
		i -= 2
		// If a test fails, you should probably change this
		if i <= 20 {
			afracP1 = 0
			break
		}
		buf[i+1] = smallsString[is+1]
		buf[i+0] = smallsString[is+0]
		//we need to have at least 3 bits clean for a non-rounded multiplication by 5
		for ascaleB5 > 0 && afracP1>>(61) == 0 {
			afracP1 *= 5
			ascaleB5--
		}
		for ascaleB5 < 0 && afracP1>>(64-1) == 0 {
			afracP1 <<= 1
			ascaleB5++
		}
	}
	// afracP1 < 100
	is := afracP1 * 2
	i--
	buf[i] = smallsString[is+1]
	if afracP1 >= 10 {
		i--
		buf[i] = smallsString[is]
	}

	return buf, ascaleB10, i, aneg
}

func (a Posit) LeSameES(b Posit) bool {
	if a.es != b.es {
		panic("es must be the same for AddPositFast")
	}
	return a.num <= b.num
}

func (a Posit) LtSameES(b Posit) bool {
	if a.es != b.es {
		panic("es must be the same for AddPositFast")
	}
	return a.num < b.num
}

func (a Posit) EqSameES(b Posit) bool {
	if a.es != b.es {
		panic("es must be the same for AddPositFast")
	}
	return a.num == b.num
}

func (a Posit) AddSameES(b Posit) Posit {
	if a.es != b.es {
		panic("es must be the same for AddPositFast")
	}
	if a.num == 0 || b.num == 0 {
		a.num |= b.num
		return a
	}
	if a.num == 1<<31 || b.num == 1<<31 {
		a.num = 1 << 31
		return a
	}
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	xneg := aneg
	bneg := b.num>>31 != 0
	if bneg {
		xneg = !xneg
		b.num = uint32(-int32(b.num))
	}

	//This allows less branching later on.
	//This potentially leaves bneg in the wrong position, dont use bneg!!
	if a.num < b.num {
		a.num, b.num = b.num, a.num
		aneg = bneg
		// aneg, bneg = bneg, aneg
	} else if a.num == -b.num {
		a.num = 0
		return a
	}
	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	//B
	sftnum = b.num
	if b.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - a.es)
	bscale := (int32(reg) << (a.es & 0x1f)) + int32(bexp)

	//Calc

	// We are using a trick to not have to double-negate.
	// The simple way to do this would be to negate af and fb
	// on aneg and bneg respectively, and negate the result if the result should be negated.
	// Instead we take advantage that a>b, so we never have to negate a and
	// we can negate b if and only if bneg^aneg.

	combinedFrac := uint64(afracP1) << 31
	tmp := bscale - ascale + 31
	if tmp > 0 {
		bf := uint64(bfracP1) << (tmp & 0x3f)
		if xneg {
			combinedFrac -= bf
		} else {
			combinedFrac += bf
		}
	}
	if combinedFrac == 0 {
		a.num = 0
		return a
	}
	if xneg && combinedFrac<<1 != 0 {
		for (combinedFrac>>62)&1 == 0 {
			ascale--
			combinedFrac <<= 1
		}
	}
	overflow := int32(combinedFrac >> 63)
	ascale += overflow
	combinedFrac >>= overflow & 0b1 // leave the hidden bit in

	endreg := ascale >> (a.es & 0x1f)
	endexp := uint32(ascale) &^ (0xffffffff << (a.es & 0x1f))

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 30 {
			a.num = 1
			if aneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 30 {
			a.num = 0x7fffffff
			if aneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - 0xffffffff>>(outn&0x1f)
	}
	combinedShift := (a.es + outn) & 0x3f
	combinedFrac >>= combinedShift

	h := uint32(0xffffffff >> (a.es & 0x1f))
	outfrac := uint32(combinedFrac>>31) & (h >> ((1 + outn) & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-int(combinedShift)) & h

	if combinedFrac&0x7fffffff != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if aneg {
		outPosit = uint32(-int32(outPosit))
	}
	return Posit{
		num: outPosit,
		es:  a.es,
	}
}

func (a Posit) SubSameES(b Posit) Posit {
	b.num = uint32(-int32(b.num))
	return a.AddSameES(b)
}

func (a Posit) Negate() Posit {
	a.num = uint32(-int32(a.num))
	return a
}

func (a Posit) MulSameES(b Posit) Posit {
	if a.es != b.es {
		panic("es must be the same for MulSameES")
	}
	if a.num == 1<<31 || b.num == 1<<31 {
		a.num = 1 << 31
		return a
	}
	if a.num == 0 || b.num == 0 {
		a.num = 0
		return a
	}
	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	//B
	xneg := aneg
	bneg := b.num>>31 != 0
	if bneg {
		xneg = !xneg
		b.num = uint32(-int32(b.num))
	}

	sftnum = b.num
	if b.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - a.es)
	ascale += (int32(reg) << (a.es & 0x1f)) + int32(bexp)

	//Out

	af := int64(afracP1)
	bf := int64(bfracP1)
	combinedFrac := uint64(af * bf)

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 62 for number

	overflow := uint8((combinedFrac >> 63))
	ascale += int32(overflow)
	combinedFrac >>= overflow // leave the hidden bit in

	endreg := ascale >> (a.es & 0x1f)
	endexp := uint32(ascale) &^ (0xffffffff << (a.es & 0x1f))

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 30 {
			a.num = 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 30 {
			a.num = 0x7fffffff
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - 0xffffffff>>(outn&0x1f)
	}
	//Recalculate the final fraction bits so that it matches the new exponent and m

	combinedShift := (a.es + outn) & 0x3f
	combinedFrac >>= combinedShift

	h := uint32(0xffffffff >> (a.es & 0x1f))
	outfrac := uint32(combinedFrac>>31) & (h >> ((1 + outn) & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-int(combinedShift)) & h

	if combinedFrac&0x7fffffff != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1
	if xneg {
		outPosit = uint32(-int32(outPosit))
	}
	return Posit{
		num: outPosit,
		es:  a.es,
	}
}

func (a Posit) DivSameES(b Posit) Posit {
	if a.es != b.es {
		panic("es must be the same for DivSameES")
	}
	if b.num == 1<<31 {
		return b
	}
	if a.num == 0 || a.num == 1<<31 {
		return a
	}
	if b.num == 0 {
		a.num = 1 << 31
		return a
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	//B
	xneg := aneg
	bneg := b.num>>31 != 0
	if bneg {
		xneg = !xneg
		b.num = uint32(-int32(b.num))
	}

	sftnum = b.num
	if b.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		reg = 0
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	bfracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	bexp := sftnum >> (32 - a.es)
	ascale -= (int32(reg) << (a.es & 0x1f)) + int32(bexp)

	//Calc

	af := uint64(afracP1)
	bf := uint64(bfracP1)
	combinedFrac := uint64(af<<30) / bf
	rem := uint64(af<<30) % bf

	// combinedFrac looks like:
	// 1 bit for overflow 1 for hidden bit 62 for number

	overflow := (combinedFrac >> 30) == 0
	if overflow {
		ascale -= 1
		combinedFrac <<= 1 // remove the hidden bit
	}

	endreg := ascale >> (a.es & 0x1f)
	endexp := uint32(ascale) &^ (0xffffffff << (a.es & 0x1f))

	var outPosit uint32
	var outn uint8
	if endreg < 0 {
		outn = uint8(-endreg)
		if outn > 30 {
			a.num = 1
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = (0b1 << 31) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + endreg)
		if outn > 30 {
			a.num = 0x7fffffff
			if xneg {
				a.num = uint32(-int32(a.num))
			}
			return a
		}
		outPosit = 0xffffffff - 0xffffffff>>(outn&0x1f)
	}

	combinedShift := (a.es + outn - 1) & 0x3f

	h := uint32(0xffffffff >> (a.es & 0x1f))
	outPosit |= (uint32(combinedFrac) >> (combinedShift & 0x3f)) & (h >> ((1 + outn) & 0x1f))
	outPosit |= bits.RotateLeft32(endexp, 30-int(combinedShift)) & h

	mask := uint64(1<<(combinedShift&0x3f)) - 1
	if combinedFrac&mask != 0 || rem != 0 || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if xneg {
		outPosit = uint32(-int32(outPosit))
	}
	return Posit{
		num: outPosit,
		es:  a.es,
	}
}

func (a Posit) SqrtSameES() Posit {
	// Because we know that we will never max out the outn, we can make some special
	// optimisations with this one.
	if a.num == 0 {
		return a
	}
	if a.num>>31 != 0 {
		a.num = 1 << 31
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f))
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)

	//calc
	n := uint64(afracP1) << 31
	// if we only shift by 31, the first iteration will cancel out the |(0b1 << 31)
	// otherwise do shift the or.
	if aexp&1 != 0 {
		n = (n << 1) | (0b1 << 63)
		n -= 1 << 62
	}

	//because we have the | (0b1 << 31) we always know the first iteration!

	if reg&1 != 0 {
		aexp += 1 << a.es
	}
	reg >>= 1
	aexp >>= 1

	//out
	var outPosit uint32
	var outn uint8
	if reg < 0 {
		outn = uint8(-reg)
		outPosit = (0b1 << 30) >> (outn & 0x1f)
	} else {
		outn = uint8(1 + reg)
		outPosit = 0x7fffffff - 0x7fffffff>>(outn&0x1f)
	}

	//more calc
	//https://stackoverflow.com/a/31120562

	combinedFrac := uint64(1 << (62 - ((outn + a.es) << 1)))
	one := combinedFrac >> 2
	n >>= ((outn + a.es) << 1) & 0x3f

	//There are some tricky performance characteristics of modifying this code
	//Allignment changes the benchmark for this loop a lot (jumping to aligned addeesses ...)
	//Dont be allarmed if suddenly this code performs worse with a small change
	for one != 0 {
		a1 := combinedFrac + one
		if n >= a1 {
			n -= a1
			combinedFrac += one << 1
		}
		combinedFrac >>= 1
		one >>= 2
	}
	g := uint8(2+a.es+outn) & 0x1f
	outFracBits := 30 - a.es - outn
	outPosit |= uint32(aexp) << outFracBits

	outfrac := uint32(combinedFrac>>1) << g
	outfrac >>= g
	outPosit |= outfrac
	outPosit += uint32(combinedFrac) & 1
	return Posit{
		num: outPosit,
		es:  a.es,
	}
}

func (a Posit) CeilSameES() Posit {
	//A
	if a.num == 1<<31 || a.num == 0 {
		return a
	}
	decnum := a.num
	aneg := decnum>>31 != 0
	if aneg {
		decnum = uint32(-int32(decnum))
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<30) == 0 {
		if aneg {
			a.num = 0b00000000000000000000000000000000
		} else {
			a.num = 0b01000000000000000000000000000000
		}
		return a
	}
	var an uint8
	var reg uint8
	an = 1
	aexp := (decnum << 3)
	one := uint32(1 << (32 - 3))
	for decnum&one != 0 {
		an++
		aexp <<= 1
		one >>= 1
	}
	aexp >>= 32 - a.es

	reg = an - 1
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	//calc
	tmp := int8(30-a.es-an) - int8(ascale)
	if tmp > 0 {
		leftover := decnum &^ (0xffffffff << (tmp & 0x1f))
		decnum &= (0xffffffff << (tmp & 0x1f))
		if leftover != 0 != aneg {
			decnum += 1 << (tmp & 0x1f)
		}
		if aneg {
			a.num = uint32(-int32(decnum))
		} else {
			a.num = decnum
		}
	}
	return a
}

func (a Posit) TruncateSameES() Posit {
	//A
	if a.num == 1<<31 {
		return a
	}
	decnum := a.num
	aneg := decnum>>31 != 0
	if aneg {
		decnum = uint32(-int32(decnum))
	}

	// handle everything between 0 and 1
	// This simplifies the code for decoding the posit
	if decnum&(1<<30) == 0 {
		a.num = 0
		return a
	}
	var an uint8
	var reg uint8
	an = 1
	aexp := (decnum << 3)
	one := uint32(1 << (32 - 3))
	for decnum&one != 0 {
		an++
		aexp <<= 1
		one >>= 1
	}
	aexp >>= 32 - a.es

	reg = an - 1
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	//calc
	tmp := int8(30-a.es-an) - int8(ascale)
	if tmp > 0 {
		if aneg {
			a.num = uint32(-int32(decnum & (0xffffffff << (tmp & 0x1f))))
		} else {
			a.num = a.num & (0xffffffff << (tmp & 0x1f))
		}
	}
	return a
}

func (a Posit) RoundSameES() Posit {
	if a.num == 1<<31 {
		return a
	}
	//A
	decnum := a.num
	aneg := decnum>>31 != 0

	// This is so that negative numbers are rounded "up" (which is down post-negation)
	// For example, this makes sure we round -1.5 to -2 instead of -1.
	if aneg {
		decnum = uint32(-int32(decnum))
	}

	var reg uint8
	var an = uint8(1)
	if decnum&(1<<30) == 0 {
		if decnum&(1<<29) == 0 {
			//the second time we know for sure an>1 aka a<0.5
			a.num = 0
			return a
		}

		aexp := decnum << 3
		aexp >>= 32 - a.es

		if aexp != (1<<a.es)-1 || decnum<<(3+a.es) == 0 {
			a.num = 0
			return a
		} else {
			if aneg {
				a.num = 0b11000000000000000000000000000000
				return a
			} else {
				a.num = 0b01000000000000000000000000000000
				return a
			}
		}
	}
	aexp := decnum
	for aexp&(1<<29) != 0 {
		an++
		aexp <<= 1
	}
	aexp <<= 3
	aexp >>= 32 - a.es

	reg = an - 1
	ascale := (uint32(reg) << (a.es & 0x1f)) + aexp
	//calc
	// we need to make sure to tie-break to nearest even, not uneven.
	// See comment above about ` + "`" + `negoffset` + "`" + `
	tmp := a.es + an + uint8(ascale)
	if an+a.es < 31 && ((decnum|((0x1<<30)>>((an+a.es)&0x0f)))>>((29-tmp)&0x1f)&3 == 3 || (decnum<<((2+tmp)&0x1f)) > 0x80000000) {
		//slowpath
		afracP1 := decnum | (0b1 << ((31 - (a.es + an + 1)) & 0x1f))
		afracP1 &= 0xffffffff >> ((a.es + an + 1) & 0x1f)
		afracP1 += (0b1 << 30) >> tmp
		if afracP1&((0b1<<31)>>((a.es+an)&0x1f)) != 0 {
			//overflow
			ascale++
			tmp++

			endreg := ascale >> (a.es & 0x1f)
			endexp := uint32(ascale) &^ (0xffffffff << (a.es & 0x1f))

			outn := uint8(1 + endreg)
			a.num = 0x7fffffff - 0x7fffffff>>(outn&0x1f)
			//Recalculate the final fraction bits so that it matches the new exponent and m
			outFracBits := 30 - a.es - outn
			afracP1 = (afracP1 >> 1) & ((0xffffffff >> 2) >> ((a.es + outn) & 0x1f))
			a.num |= endexp << (outFracBits & 0x1f)
		} else {
			outFracBits := 30 - a.es - an
			a.num = decnum & (0xffffffff << (outFracBits & 0x1f))

			afracP1 &= (0xffffffff >> 2) >> ((a.es + an) & 0x1f)
		}
		if 30-int32(tmp) >= 0 {
			afracP1 = afracP1 & (0xffffffff << ((30 - tmp) & 0x1f))
		}
		a.num |= afracP1
		if aneg {
			a.num = uint32(-int32(a.num))
		}
		return a
	} else {
		tmp := int8(30-a.es-an) - int8(ascale)
		if tmp >= 0 {
			if aneg {
				a.num = uint32(-int32(decnum & (0xffffffff << (tmp & 0x1f))))
			} else {
				a.num = a.num & (0xffffffff << (tmp & 0x1f))
			}
		}
		return a
	}
}

//e^(1/2^i)<<31
var eroot = [...]uint64{
	5837465777, //0
	3540601969, //1
	2757423586, //2
	2433417774, //3
	2285984444, //4
	2215652097, //5
	2181301595, //6
	2164326571, //7
	2155888661, //8
	2151682051, //9
	2149581824, //10
	2148532480, //11
	2148008000, //12
	2147745808, //13
	2147614724, //14
	2147549185, //15
	2147516416, //16
	2147500032, //17
	2147491840, //18
	2147487744, //19
	2147485696, //20
	2147484672, //21
	2147484160, //22
	2147483904, //23
	2147483776, //24
	2147483712, //25
	2147483680, //26
	2147483664, //27
	2147483656, //28
	2147483652, //29
	2147483650, //30
	2147483649, //31
	2147483648, //32
	// going futher would be useless as this is all 1s
	// the last value is probably already useless
}

//e^(1/2^i)<<32
var eroote = [...]uint64{
	11674931555, //0
	7081203938,  //1
	5514847172,  //2
	4866835547,  //3
	4571968888,  //4
	4431304193,  //5
	4362603189,  //6
	4328653142,  //7
	4311777323,  //8
	4303364101,  //9
	4299163649,  //10
	4297064960,  //11
	4296016000,  //12
	4295491616,  //13
	4295229448,  //14
	4295098370,  //15
	4295032833,  //16
	4295000064,  //17
	4294983680,  //18
	4294975488,  //19
	4294971392,  //20
	4294969344,  //21
	4294968320,  //22
	4294967808,  //23
	4294967552,  //24
	4294967424,  //25
	4294967360,  //26
	4294967328,  //27
	4294967312,  //28
	4294967304,  //29
	4294967300,  //30
	4294967298,  //31
	4294967297,  //32
	// going futher would be useless as this is all 1s
	// the last value is probably already useless
}

const erootee0 uint64 = 12535862302449813504

//e^(1/2^i)<<62
var erootee = [...]uint64{
	12535862302449813504, //0
	7603384832371617792,  //1
	5921522061444257792,  //2
	5225724877810906112,  //3
	4909114212884919296,  //4
	4758076646968243200,  //5
	4684309505938875392,  //6
	4647855920155531264,  //7
	4629735647166716928,  //8
	4620702019504571392,  //9
	4616191817794016256,  //10
	4613938368086376448,  //11
	4612812055784369152,  //12
	4612249002741945344,  //13
	4611967501994208256,  //14
	4611826758063248384,  //15
	4611756387708439552,  //16
	4611721202933694464,  //17
	4611703610646986752,  //18
	4611694814528798720,  //19
	4611690416475996160,  //20
	4611688217451167744,  //21
	4611687117939146752,  //22
	4611686568183234560,  //23
	4611686293305303040,  //24
	4611686155866343424,  //25
	4611686087146864640,  //26
	4611686052787126272,  //27
	4611686035607257088,  //28
	4611686027017322496,  //29
	4611686022722355200,  //30
	4611686020574871552,  //31
	4611686019501129728,  //32
	4611686018964258816,  //33
	4611686018695823360,  //34
	4611686018561605632,  //35
	4611686018494496768,  //36
	4611686018460942336,  //37
	4611686018444165120,  //38
	4611686018435776512,  //39
	4611686018431582208,  //40
	4611686018429485056,  //41
	4611686018428436480,  //42
	4611686018427912192,  //43
	4611686018427650048,  //44
	4611686018427518976,  //45
	4611686018427453440,  //46
	4611686018427420672,  //47
	4611686018427404288,  //48
	4611686018427396096,  //49
	4611686018427392000,  //50
	4611686018427389952,  //51
	4611686018427388928,  //52
	// going futher would be useless as this is all 1s
	// the last valua is probably already useless
}

//e^(1/2^i)<<63
var erootee2 = [...]uint64{
	12535862302449813504,     //0
	7603384832371617792 << 1, //1
	5921522061444257792 << 1, //2
	5225724877810906112 << 1, //3
	4909114212884919296 << 1, //4
	4758076646968243200 << 1, //5
	4684309505938875392 << 1, //6
	4647855920155531264 << 1, //7
	4629735647166716928 << 1, //8
	4620702019504571392 << 1, //9
	4616191817794016256 << 1, //10
	4613938368086376448 << 1, //11
	4612812055784369152 << 1, //12
	4612249002741945344 << 1, //13
	4611967501994208256 << 1, //14
	4611826758063248384 << 1, //15
	4611756387708439552 << 1, //16
	4611721202933694464 << 1, //17
	4611703610646986752 << 1, //18
	4611694814528798720 << 1, //19
	4611690416475996160 << 1, //20
	4611688217451167744 << 1, //21
	4611687117939146752 << 1, //22
	4611686568183234560 << 1, //23
	4611686293305303040 << 1, //24
	4611686155866343424 << 1, //25
	4611686087146864640 << 1, //26
	4611686052787126272 << 1, //27
	4611686035607257088 << 1, //28
	4611686027017322496 << 1, //29
	4611686022722355200 << 1, //30
	4611686020574871552 << 1, //31
	4611686019501129728 << 1, //32
	4611686018964258816 << 1, //33
	4611686018695823360 << 1, //34
	4611686018561605632 << 1, //35
	4611686018494496768 << 1, //36
	4611686018460942336 << 1, //37
	4611686018444165120 << 1, //38
	4611686018435776512 << 1, //39
	4611686018431582208 << 1, //40
	4611686018429485056 << 1, //41
	4611686018428436480 << 1, //42
	4611686018427912192 << 1, //43
	4611686018427650048 << 1, //44
	4611686018427518976 << 1, //45
	4611686018427453440 << 1, //46
	4611686018427420672 << 1, //47
	4611686018427404288 << 1, //48
	4611686018427396096 << 1, //49
	4611686018427392000 << 1, //50
	4611686018427389952 << 1, //51
	4611686018427388928 << 1, //52
	// going futher would be useless as this is all 1s
	// the last valua is probably already useless
}

var expMinOverflow = [...]uint32{
	NewPosit32FromComponents(false, 4, 0b01001000000000000000000000000000, 0).num,   //0
	NewPosit32FromComponents(false, 5, 0b01001000000000000000000000000000, 1).num,   //1
	NewPosit32FromComponents(false, 6, 0b01001000000000000000000000000000, 2).num,   //2
	NewPosit32FromComponents(false, 7, 0b01001000000000000000000000000000, 3).num,   //3
	NewPosit32FromComponents(false, 8, 0b01001000000000000000000000000000, 4).num,   //4
	NewPosit32FromComponents(false, 9, 0b01001000000000000000000000000000, 5).num,   //5
	NewPosit32FromComponents(false, 10, 0b01001000000000000000000000000000, 6).num,  //6
	NewPosit32FromComponents(false, 11, 0b01001000000000000000000000000000, 7).num,  //7
	NewPosit32FromComponents(false, 12, 0b01001000000000000000000000000000, 8).num,  //8
	NewPosit32FromComponents(false, 13, 0b01001000000000000000000000000000, 9).num,  //9
	NewPosit32FromComponents(false, 14, 0b01001000000000000000000000000000, 10).num, //10
	NewPosit32FromComponents(false, 15, 0b01001000000000000000000000000000, 11).num, //11
	NewPosit32FromComponents(false, 16, 0b01001000000000000000000000000000, 12).num, //12
	NewPosit32FromComponents(false, 17, 0b01001000000000000000000000000000, 13).num, //13
	NewPosit32FromComponents(false, 18, 0b01001000000000000000000000000000, 14).num, //14
	NewPosit32FromComponents(false, 19, 0b01001000000000000000000000000000, 15).num, //15
	NewPosit32FromComponents(false, 20, 0b01001000000000000000000000000000, 16).num, //16
	NewPosit32FromComponents(false, 21, 0b01001000000000000000000000000000, 17).num, //17
	NewPosit32FromComponents(false, 22, 0b01001000000000000000000000000000, 18).num, //18
	NewPosit32FromComponents(false, 23, 0b01001000000000000000000000000000, 19).num, //19
	NewPosit32FromComponents(false, 24, 0b01001000000000000000000000000000, 20).num, //20
	NewPosit32FromComponents(false, 25, 0b01001000000000000000000000000000, 21).num, //21
	NewPosit32FromComponents(false, 26, 0b01001000000000000000000000000000, 22).num, //22
	NewPosit32FromComponents(false, 27, 0b01001000000000000000000000000000, 23).num, //23
	NewPosit32FromComponents(false, 28, 0b01001000000000000000000000000000, 24).num, //24
	NewPosit32FromComponents(false, 29, 0b01001000000000000000000000000000, 25).num, //25
	NewPosit32FromComponents(false, 30, 0b01001000000000000000000000000000, 26).num, //26
	NewPosit32FromComponents(false, 31, 0b01001000000000000000000000000000, 27).num, //27
	NewPosit32FromComponents(false, 32, 0b01001000000000000000000000000000, 28).num, //28
	NewPosit32FromComponents(false, 33, 0b01001000000000000000000000000000, 29).num, //29
	NewPosit32FromComponents(false, 34, 0b01001000000000000000000000000000, 30).num, //30
	NewPosit32FromComponents(false, 35, 0b01001000000000000000000000000000, 31).num, //31
	NewPosit32FromComponents(false, 36, 0b01001000000000000000000000000000, 32).num, //32
}

func (a Posit) ExpSameES() Posit {
	//TODO: should we have per-size tables and calculations? like a 32 bit table since we wont need more than 16 bits of accuracy anyways

	//A
	aneg := a.num&(1<<(32-1)) != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	// check for things that result in 1
	// Modifies version of normal calculations.
	// scale = -28, but its negated since to avoid negating it at runtime
	// also avoids branches because its never bigger than 30
	if a.num <= (0b1<<31)>>(28>>(a.es&0x1f)) {
		a.num = 1 << (32 - 2)
		return a
	}

	if a.num >= expMinOverflow[a.es] {
		a.num = 0xffffffff >> 1
		if aneg {
			a.num = 0
		}
		return a
	}

	var reg int8
	r1 := uint8(1)
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
			r1++
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
			r1++
		}
	}
	n := 32 - 1 - a.es - r1 - 1
	if n >= 32 {
		n = 0
	}
	afrac := a.num & ((1 << n) - 1)
	sftnum <<= 3
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << a.es) + int32(aexp)

	for afrac&1 == 0 && afrac != 0 {
		afrac >>= 1
		n--
	}

	if afrac == 0 || n > 32 {
		n = 0
	}

	var outscale uint64
	finalscale := uint64(1 << 63)
	var outfrac uint64
	var power uint64
	var powerOfTwo uint32
	var baseindex uint8

	// Instead of taking the base out of the array directly, we store the index.
	// This way we can look up `base*base` instead of calculating. This reduces errors.
	// From 29 on it always results in 1
	if ascale < 0 {
		// e^((1+a/2^n)*2^b)	=	e^(a/2^n*2^b)*e^(2^b)
		//						=	e^(a/2^n*2^b)*e^(2^b)
		//						=	(e^(1/2^n))^(a*2^b)*e^(1/2^(-b))
		// We have n = n, b = ascale a = afrac

		outfrac = erootee2[-ascale]
		baseindex = n
		power = uint64(afrac >> -uint32(ascale))

		// We have rounded off everything after the binary point in the power.
		// Here we find the biggest fraction of this, so the biggest 1/2^n.
		// Then we add this again later
		powfracBits := uint8(-ascale)

		i := uint8(0)
		if int8(baseindex+powfracBits)+1-int8(len(erootee)) >= 0 {
			i = baseindex + powfracBits + 1 - uint8(len(erootee))
		}
		if afrac<<(32-i) != 0 {
			afrac += 1 << i
		}

		// This is prefered over doing it in the main loop,
		// because we dont have to check for overflow.
		for ; i < powfracBits; i++ {
			if afrac&(1<<i) != 0 {
				finalscale, _ = bits.Mul64(finalscale, erootee2[baseindex+powfracBits-i])
				finalscale <<= 1
				finalscale += 1
			}
		}
	} else if ascale < int32(n) {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^(n-b))
		//						=	e^(2^b)*(e^(1/(2^(n-b))))^a
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = n - uint8(ascale)
		power = uint64(afrac)
		outscale = 1 << (powerOfTwo & 0x3f)
	} else {
		// e^((1+a/2^n)*2^b)	=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a/2^n*2^b)
		//						=	e^(2^b)*e^(a*2^(b-n))
		//						=	e^(2^b)*(e^(2^(b-n)))^a
		// We have n = n, b = ascale a = afrac

		outfrac = erootee0
		powerOfTwo = uint32(ascale)
		baseindex = 0
		power = uint64(afrac << (uint32(ascale) - uint32(n)))
		outscale = 1 << powerOfTwo
	}

	for powerOfTwo > 0 {
		powerOfTwo--
		outfrac, _ = bits.Mul64(outfrac, outfrac)

		if outfrac&(1<<63) != 0 {
			outscale += 1 << powerOfTwo
			outfrac >>= 1
		}
		outfrac <<= 1
	}
	if int(baseindex) < len(erootee2) {
		for ; power != 0 && baseindex != 0; power >>= 1 {
			if power&1 == 1 {
				outfrac, _ = bits.Mul64(outfrac, erootee2[baseindex])
				if outfrac&(1<<63) != 0 {
					outscale++
					outfrac >>= 1
				}
				outfrac <<= 1
			}
			baseindex--
		}
	}
	if power != 0 && baseindex == 0 {
		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, erootee0)

			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
		outscale += power
		power >>= 1
	}
	base := erootee0

	for ; power != 0; power >>= 1 {
		base, _ = bits.Mul64(base, base)
		if base&(1<<63) != 0 {
			outscale += power
			base >>= 1
		}
		base <<= 1

		if power&1 == 1 {
			outfrac, _ = bits.Mul64(outfrac, base)
			if outfrac&(1<<63) != 0 {
				outscale++
				outfrac >>= 1
			}
			outfrac <<= 1
		}
	}
	outfrac, low := bits.Mul64(outfrac, finalscale)
	outfrac <<= 1

	endreg := outscale >> (a.es & 0x1f)
	endexp := uint32(outscale) &^ (0xffffffff << (a.es & 0x1f))

	// no need to check for max, no need to check for negatives either.
	// Max is already filtered out, and the result is always above 1.

	outn := uint8(1 + endreg)
	outPosit := uint32(0xffffffff - (0xffffffff >> (outn & 0x1f)))

	combinedShift := (a.es + uint8(outn) - 1) & 0x3f
	mask := uint64((2<<32)<<(combinedShift&0x3f)) - 1
	outfrac2 := outfrac >> (32 + 1 + 32 - 32) // now the frac is the entirity of the output

	outPosit |= (uint32(outfrac2) >> (combinedShift & 0x3f)) & ((0xffffffff >> (a.es + 1)) >> (outn & 0x1f))
	outPosit |= bits.RotateLeft32(endexp, int(32-a.es-combinedShift)) & (0xffffffff >> a.es)

	if outPosit&2 == 2 || outfrac&mask != 0 || low != 0 {
		outPosit += 1
	}
	outPosit >>= 1

	p := Posit{
		num: outPosit,
		es:  a.es,
	}

	if aneg {
		p = Newposit32FromBits(0b01000000000000000000000000000000, a.es).DivSameES(p)
	}
	return p
}

func (a Posit) Log2SameES() Posit {
	// https://en.wikipedia.org/wiki/Binary_logarithm#Iterative_approximation

	if a.num == 0 || a.num&(1<<(32-1)) != 0 {
		a.num = 1 << (32 - 1)
		return a
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<(32-2)) == 0 {
		reg = -1
		for sftnum&(1<<(32-3)) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<(32-3)) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 1
	afracP1 := uint64(((sftnum << a.es) | (1 << (32 - 2))) &^ (1 << (32 - 1))) // shifted one to the right compared to normal afracP1
	afracP1 <<= (32 - 32)
	sftnum <<= 2
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << a.es) + int32(aexp)

	n := uint32(ascale)
	if ascale < 0 {
		n = uint32(-int32(n)) - 1
	}

	var outPosit uint32
	var outn uint8
	var series uint32
	seg := uint32(1 << (31 - a.es))
	outscale := int32(0)

	// This is a very specialised version of the algorithm to use less bits.
	if ascale < 0 {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// This is done to improve the accuracy. We also
			var passedFirst bool
			var zeroCounter bool = true
			for afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				seg >>= 1
				if afracP1&(1<<31) != 0 {
					afracP1 >>= 1
					series -= seg

					if zeroCounter {
						// this means the last bit is 0 again, so we shift by one.
						seg <<= 1
						series <<= 1
						outscale--
						zeroCounter = true
					} else if passedFirst {
						break
					} else {
						passedFirst = true
						outscale--
					}
				} else if zeroCounter {
					zeroCounter = false
				}
			}

			seg <<= 1
			series <<= 1

			series &^= 1 << (31 - a.es)

			endreg := int16(outscale >> int32(a.es))
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 32-2 {
					x := -1
					a.num = uint32(x)
					return a
				}
				outPosit = (0b1 << 31) >> (outn & 0x1f)
			} else {
				outn = 1
				outPosit = 0xffffffff - (0xffffffff >> outn)
			}
		} else {
			// pre flipping the first bit, trick to be able to more the second loop after the first
			series = 1 << 31
			for n > 1 {
				outscale += 1
				series = series>>1 + (uint32(n%2) << 30)
				n >>= 1
				seg >>= 1
			}
			if afracP1 == (1 << 30) {
				outscale++
			}
			series >>= a.es

			endreg := int16(outscale >> int32(a.es))
			outn = uint8(1 + endreg)
			if outn > 32-2 {
				x := 0xffffffff >> 1
				a.num = uint32(-int32(x))
				return a
			}
			outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
		}
		series >>= outn - 1
		seg >>= outn
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series -= seg
			}
			seg >>= 1
		}
		series >>= 1
	} else {
		if n == 0 {
			// Most notably this, where we calculate the amount of empty bits
			// so we dont round to 0 and have a more accurate fraction
			for afracP1&(1<<31) == 0 && afracP1 != 1<<30 {
				afracP1 *= afracP1
				afracP1 >>= 30
				outscale--
			}
			if afracP1 == 1<<30 {
				a.num = 0
				return a
			}
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}

			endreg := int16(outscale >> int32(a.es))
			if endreg != 0 {
				outn = uint8(-endreg)
				if outn > 32-2 {
					a.num = 1
					return a
				}
				outPosit = (0b1 << 31) >> (outn & 0x1f)
			} else {
				outn = 1
				outPosit = 0xffffffff - (0xffffffff >> outn)
			}
		} else {
			// Here we first shift the series and seg to the right to limmit the amount of calculations done.
			for n > 1 {
				outscale += 1
				series = series>>1 + (uint32(n%2) << 30)
				seg >>= 1
				n >>= 1
			}
			series >>= a.es

			endreg := int16(outscale >> int32(a.es))

			outn = uint8(1 + endreg)
			if outn > 32-2 {
				a.num = 0xffffffff >> 1
				return a
			}
			outPosit = 0xffffffff - (0xffffffff >> (outn & 0x1f))
		}
		series >>= outn
		seg >>= outn + 1
		for afracP1 != (1<<30) && seg != 0 {
			afracP1 *= afracP1
			afracP1 >>= 30
			if afracP1&(1<<31) != 0 {
				afracP1 >>= 1
				series += seg
			}
			seg >>= 1
		}
	}

	endexp := uint32(outscale) &^ (0xffffffff << uint32(a.es))

	outfrac := uint32(series) & ((0xffffffff >> 1) >> ((outn + a.es) & 0x1f))
	outPosit |= outfrac
	outPosit |= bits.RotateLeft32(endexp, 31-int(a.es+outn)) & (0xffffffff >> 2)

	if afracP1 != (1<<30) || outPosit&2 == 2 {
		outPosit += 1
	}

	outPosit >>= 1

	if ascale < 0 {
		outPosit = uint32(-int32(outPosit))
	}
	return Posit{
		num: outPosit,
		es:  a.es,
	}
}

func (a Posit) LogeSameES() Posit {
	log2 := a.Log2SameES()
	// Devisor is log2(e)
	return log2.DivSameES(NewPosit32FromComponents(false, 0, 0b01110001010101000111011001100000, a.es))
}

func (a Posit) LogbSameES(b Posit) Posit {
	log2 := a.Log2SameES()
	return log2.DivSameES(b.Log2SameES())
}

//TODO: maybe using log2 and pow2 is faster/more accurate?
func (b Posit) PowSameES(p Posit) Posit {
	ln := b.LogeSameES()
	e := ln.MulSameES(p)
	return e.ExpSameES()
}

func NewPosit32FromInt32(x int32, es uint8) Posit {
	const inbits = 32
	const outbits = 32
	const bitoffset = inbits - outbits
	var a Posit
	a.es = es
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint32(x)
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= (31 - outscale) & 0x1f

	endreg := outscale >> (a.es & 0x1f)
	endexp := uint32(outscale) &^ (0xffffffff << (a.es & 0x1f))

	if endreg > 29 {
		a.num = 0x7fffffff
		return a
	}
	a.num = 0xffffffff - (0xffffffff>>1)>>endreg

	combinedShift := int8(a.es + endreg)

	// endexp will never stick out more than a.es bits.
	h := uint32(0xffffffff >> (a.es & 0x1f))
	a.num |= bits.RotateLeft32(endexp, inbits-2-int(combinedShift)) & h
	a.num |= (outfrac & 0x7fffffff) >> ((combinedShift + 1 + bitoffset) & 0x3f)

	if outfrac<<(outbits-1-(combinedShift&0x3f)) != 0 || outfrac&((1<<(1+bitoffset))<<(combinedShift)&0x3f) != 0 {
		a.num += 1
	}
	a.num >>= 1

	if aneg {
		a.num = uint32(-int32(a.num))
	}
	return a
}

func NewPosit32FromUint32(x uint32, es uint8) Posit {
	const inbits = 32
	const outbits = 32
	const bitoffset = inbits - outbits
	var a Posit
	a.es = es
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := x
	outscale := uint8(bits.Len32(outfrac >> 1))
	outfrac <<= (31 - outscale) & 0x1f

	endreg := outscale >> (a.es & 0x1f)
	endexp := uint32(outscale) &^ (0xffffffff << (a.es & 0x1f))

	if endreg > 29 {
		a.num = 0x7fffffff
		return a
	}
	a.num = 0xffffffff - (0xffffffff>>1)>>endreg

	combinedShift := int8(a.es + endreg)

	// endexp will never stick out more than a.es bits.
	h := uint32(0xffffffff >> (a.es & 0x1f))
	a.num |= bits.RotateLeft32(endexp, inbits-2-int(combinedShift)) & h
	a.num |= (outfrac & 0x7fffffff) >> ((combinedShift + 1 + bitoffset) & 0x3f)

	if outfrac<<(outbits-1-(combinedShift&0x3f)) != 0 || outfrac&((1<<(1+bitoffset))<<(combinedShift)&0x3f) != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func NewPosit32FromInt64(x int64, es uint8) Posit {
	const inbits = 64
	const outbits = 32
	const bitoffset = inbits - outbits
	var a Posit
	a.es = es
	var aneg bool
	if x == 0 {
		a.num = 0
		return a
	} else if x < 0 {
		aneg = true
		x = -x
	}

	outfrac := uint64(x)
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 63 - outscale

	endreg := outscale >> (a.es & 0x1f)
	endexp := uint32(outscale) &^ (0xffffffff << (a.es & 0x1f))

	if endreg > 29 {
		a.num = 0x7fffffff
		return a
	}
	a.num = 0xffffffff - (0xffffffff>>1)>>endreg

	combinedShift := int8(a.es + endreg)

	h := uint32(0xffffffff >> (a.es & 0x1f))
	a.num |= bits.RotateLeft32(endexp, inbits-2-(int(combinedShift))) & h
	a.num |= uint32((outfrac & 0x7fffffffffffffff) >> ((combinedShift + 1 + bitoffset) & 0x3f))

	if outfrac<<(outbits-1-(combinedShift&0x3f)) != 0 || outfrac&((1<<(1+bitoffset))<<(combinedShift)&0x3f) != 0 {
		a.num += 1
	}
	a.num >>= 1
	if aneg {
		a.num = uint32(-int32(a.num))
	}
	return a
}

func NewPosit32FromUint64(x uint64, es uint8) Posit {
	const inbits = 64
	const outbits = 32
	const bitoffset = inbits - outbits
	var a Posit
	a.es = es
	if x == 0 {
		a.num = 0
		return a
	}

	outfrac := x
	outscale := uint8(bits.Len64(outfrac >> 1))
	outfrac <<= 63 - outscale

	endreg := outscale >> (a.es & 0x1f)
	endexp := uint32(outscale) &^ (0xffffffff << (a.es & 0x1f))

	if endreg > 29 {
		a.num = 0x7fffffff
		return a
	}
	a.num = 0xffffffff - (0xffffffff>>1)>>endreg

	combinedShift := int8(a.es + endreg)

	h := uint32(0xffffffff >> (a.es & 0x1f))
	a.num |= bits.RotateLeft32(endexp, inbits-2-(int(combinedShift))) & h
	a.num |= uint32((outfrac & 0x7fffffffffffffff) >> ((combinedShift + 1 + bitoffset) & 0x3f))

	if outfrac<<(outbits-1-(combinedShift&0x3f)) != 0 || outfrac&((1<<(1+bitoffset))<<(combinedShift)&0x3f) != 0 {
		a.num += 1
	}
	a.num >>= 1

	return a
}

func (a Posit) Int64() int64 {
	// Because we know that we will never max out the outn, we can make some special
	// optimisations with this one.
	if a.num == 0 || a.num == 1<<31 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	ascale -= 31
	if ascale < 0 {
		sft := ascale + 1
		sft = -sft
		if sft >= 32 {
			return 0
		}
		ret := afracP1 >> sft
		if ret&1 != 0 && (afracP1&((1<<sft)-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return -int64(ret)
		}
		return int64(ret)
	} else if ascale >= 32 {
		if aneg {
			return math.MinInt64
		}
		return math.MaxInt64
	}
	ret := uint64(afracP1) << int64(ascale)
	if aneg {
		return -int64(ret)
	}
	return int64(ret)
}

func (a Posit) Uint64() uint64 {
	// Because we know that we will never max out the outn, we can make some special
	// optimisations with this one.
	if a.num == 0 || a.num == 1<<31 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	ascale -= 31
	if ascale < 0 {
		ret := uint64(afracP1) >> int64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<int64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint64(ret)
	} else if ascale >= 33 {
		return math.MaxUint64
	}
	ret := uint64(afracP1) << int64(ascale)
	return ret
}

func (a Posit) Int32() int32 {
	// Because we know that we will never max out the outn, we can make some special
	// optimisations with this one.
	if a.num == 0 || a.num == 1<<31 {
		return 0
	}

	//A
	aneg := a.num>>31 != 0
	if aneg {
		a.num = uint32(-int32(a.num))
	}

	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	ascale -= 31
	if ascale < 0 {
		ret := afracP1 >> int64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<int64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		if aneg {
			return int32(-ret)
		}
		return int32(ret)
	} else {
		if aneg {
			return math.MinInt32
		}
		return math.MaxInt32
	}
}

func (a Posit) Uint32() uint32 {
	// Because we know that we will never max out the outn, we can make some special
	// optimisations with this one.
	if a.num == 0 || a.num == 1<<31 || a.num>>31 != 0 {
		return 0
	}

	//A
	var reg int8
	sftnum := a.num
	if a.num&(1<<30) == 0 {
		reg = -1
		for sftnum&(1<<29) == 0 {
			reg--
			sftnum <<= 1
		}
	} else {
		for sftnum&(1<<29) != 0 {
			reg++
			sftnum <<= 1
		}
	}
	sftnum <<= 2
	afracP1 := (sftnum << (a.es & 0x1f)) | (1 << 31)
	sftnum <<= 1
	aexp := sftnum >> (32 - a.es)
	ascale := (int32(reg) << (a.es & 0x1f)) + int32(aexp)

	ascale -= 31
	if ascale < 0 {
		ret := afracP1 >> int64(-ascale-1)
		if ret&1 != 0 && (afracP1&((1<<int64(-ascale-1))-1) != 0 || ret&2 == 2) {
			ret += 1
		}
		ret >>= 1
		return uint32(ret)
	} else if ascale >= 1 {
		return math.MaxUint32
	}
	return uint32(afracP1)
}
